// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var jshint = require('gulp-jshint');
var compass = require('gulp-compass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');
var watch = require('gulp-watch');
var htmlmin = require('gulp-htmlmin');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');

gulp.task('htmlmin', function() {
  return gulp.src('index.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('live'));
});

gulp.task('images', function () {
    return gulp.src('img/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('live/img'));
});

// Lint Task
gulp.task('lint', function() {
    return gulp.src('js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('compass', function() {
  gulp.src('sass/*.scss')
    .pipe(compass({
      config_file: 'config.rb',
      css: 'stylesheets',
      sass: 'sass'
    }))
    .pipe(gulp.dest('stylesheets'));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src('js/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('scripts'))
        .pipe(uglify())
        .pipe(gulp.dest('live/scripts'));
});

// Concatenate & Minify JS
gulp.task('depends', function() {
    return gulp.src('dist/*.js')
        .pipe(concat('depends.js'))
        .pipe(gulp.dest('scripts'))
        .pipe(uglify())    
        .pipe(gulp.dest('live/scripts'));
});

gulp.task('minify-css', function() {
  return gulp.src('stylesheets/*.css')
    .pipe(minifyCSS({keepBreaks:true}))
    .pipe(gulp.dest('live/stylesheets'));
});

gulp.task('data', function() {
  return gulp.src('*.json')
    .pipe(gulp.dest('live'));
});

gulp.task('fonts', function() {
  return gulp.src('fonts/*')
    .pipe(gulp.dest('live/fonts'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('js/*.js', ['lint', 'scripts']);
    gulp.watch('dist/*.js', ['depends']);
    gulp.watch('img/*', ['images']);
    gulp.watch('sass/*.scss', ['compass']);
    gulp.watch('stylesheets/*.css', ['minify-css']);
    gulp.watch('*.html', ['htmlmin']);
    gulp.watch('*.json', ['data']);
});

// Default Task
gulp.task('default', ['data', 'fonts', 'images', 'lint', 'depends', 'scripts', 'compass', 'minify-css', 'htmlmin', 'watch']);
var data = [{"DATE":"10/28/2014","OPP":"vsORL","OUTCOME":"W","PtF":101,"PA":84,"MIN":36,"PTS":26,"REB":17,"AST":2,"FGM":10,"FGA":22,"FGP":0.455,"m3p":0,"a3p":0,"P3P":0,"FTM":6,"FTA":9,"FTP":0.667,"BLK":9,"STL":3,"PF":1,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"11/1/2014","OPP":"vsDAL","OUTCOME":"L","PtF":104,"PA":109,"MIN":43,"PTS":31,"REB":15,"AST":2,"FGM":9,"FGA":21,"FGP":0.429,"m3p":0,"a3p":0,"P3P":0,"FTM":13,"FTA":16,"FTP":0.813,"BLK":3,"STL":2,"PF":2,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"11/3/2014","OPP":"@ MEM","OUTCOME":"L","PtF":81,"PA":93,"MIN":32,"PTS":14,"REB":8,"AST":2,"FGM":6,"FGA":12,"FGP":0.5,"m3p":0,"a3p":0,"P3P":0,"FTM":2,"FTA":2,"FTP":1,"BLK":1,"STL":1,"PF":3,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"11/4/2014","OPP":"vsCHA","OUTCOME":"W","PtF":100,"PA":91,"MIN":37,"PTS":24,"REB":13,"AST":2,"FGM":10,"FGA":17,"FGP":0.588,"m3p":0,"a3p":0,"P3P":0,"FTM":4,"FTA":4,"FTP":1,"BLK":3,"STL":1,"PF":0,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"11/8/2014","OPP":"@ SA","OUTCOME":"W","PtF":100,"PA":99,"MIN":37,"PTS":27,"REB":11,"AST":1,"FGM":10,"FGA":17,"FGP":0.588,"m3p":0,"a3p":0,"P3P":0,"FTM":7,"FTA":8,"FTP":0.875,"BLK":6,"STL":4,"PF":2,"TO":4,"player":"Davis","first":"Anthony"},{"DATE":"11/10/2014","OPP":"@ CLE","OUTCOME":"L","PtF":111,"PA":118,"MIN":40,"PTS":27,"REB":14,"AST":4,"FGM":12,"FGA":21,"FGP":0.571,"m3p":0,"a3p":2,"P3P":0,"FTM":3,"FTA":4,"FTP":0.75,"BLK":3,"STL":4,"PF":1,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"11/12/2014","OPP":"vsLAL","OUTCOME":"W","PtF":109,"PA":102,"MIN":37,"PTS":25,"REB":12,"AST":1,"FGM":12,"FGA":16,"FGP":0.75,"m3p":0,"a3p":0,"P3P":0,"FTM":1,"FTA":4,"FTP":0.25,"BLK":6,"STL":1,"PF":1,"TO":3,"player":"Davis","first":"Anthony"},{"DATE":"11/14/2014","OPP":"vsMIN","OUTCOME":"W","PtF":139,"PA":91,"MIN":27,"PTS":22,"REB":4,"AST":3,"FGM":9,"FGA":13,"FGP":0.692,"m3p":0,"a3p":0,"P3P":0,"FTM":4,"FTA":5,"FTP":0.8,"BLK":2,"STL":2,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"11/17/2014","OPP":"@ POR","OUTCOME":"L","PtF":93,"PA":102,"MIN":38,"PTS":31,"REB":11,"AST":3,"FGM":14,"FGA":20,"FGP":0.7,"m3p":0,"a3p":2,"P3P":0,"FTM":3,"FTA":4,"FTP":0.75,"BLK":3,"STL":3,"PF":3,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"11/18/2014","OPP":"@ SAC","OUTCOME":"W","PtF":106,"PA":100,"MIN":41,"PTS":28,"REB":9,"AST":0,"FGM":11,"FGA":19,"FGP":0.579,"m3p":0,"a3p":0,"P3P":0,"FTM":6,"FTA":8,"FTP":0.75,"BLK":3,"STL":2,"PF":5,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"11/21/2014","OPP":"@ DEN","OUTCOME":"L","PtF":97,"PA":117,"MIN":32,"PTS":18,"REB":9,"AST":2,"FGM":7,"FGA":17,"FGP":0.412,"m3p":0,"a3p":1,"P3P":0,"FTM":4,"FTA":4,"FTP":1,"BLK":2,"STL":1,"PF":3,"TO":4,"player":"Davis","first":"Anthony"},{"DATE":"11/22/2014","OPP":"@ UTAH","OUTCOME":"W","PtF":106,"PA":94,"MIN":40,"PTS":43,"REB":14,"AST":0,"FGM":16,"FGA":23,"FGP":0.696,"m3p":0,"a3p":0,"P3P":0,"FTM":11,"FTA":12,"FTP":0.917,"BLK":1,"STL":2,"PF":1,"TO":2,"player":"Davis","first":"Anthony"},{"DATE":"11/25/2014","OPP":"vsSAC","OUTCOME":"L","PtF":89,"PA":99,"MIN":38,"PTS":14,"REB":9,"AST":2,"FGM":4,"FGA":12,"FGP":0.333,"m3p":0,"a3p":0,"P3P":0,"FTM":6,"FTA":8,"FTP":0.75,"BLK":2,"STL":1,"PF":2,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"11/28/2014","OPP":"@ ATL","OUTCOME":"L","PtF":91,"PA":100,"MIN":31,"PTS":14,"REB":11,"AST":1,"FGM":5,"FGA":14,"FGP":0.357,"m3p":0,"a3p":0,"P3P":0,"FTM":4,"FTA":5,"FTP":0.8,"BLK":1,"STL":3,"PF":3,"TO":2,"player":"Davis","first":"Anthony"},{"DATE":"11/29/2014","OPP":"@ WSH","OUTCOME":"L","PtF":80,"PA":83,"MIN":40,"PTS":30,"REB":13,"AST":0,"FGM":11,"FGA":18,"FGP":0.611,"m3p":0,"a3p":1,"P3P":0,"FTM":8,"FTA":9,"FTP":0.889,"BLK":0,"STL":1,"PF":2,"TO":4,"player":"Davis","first":"Anthony"},{"DATE":"12/2/2014","OPP":"vsOKC","OUTCOME":"W","PtF":112,"PA":104,"MIN":39,"PTS":25,"REB":10,"AST":4,"FGM":8,"FGA":15,"FGP":0.533,"m3p":0,"a3p":0,"P3P":0,"FTM":9,"FTA":16,"FTP":0.563,"BLK":4,"STL":6,"PF":2,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"12/4/2014","OPP":"@ GS","OUTCOME":"L","PtF":85,"PA":112,"MIN":38,"PTS":30,"REB":15,"AST":3,"FGM":14,"FGA":19,"FGP":0.737,"m3p":0,"a3p":0,"P3P":0,"FTM":2,"FTA":4,"FTP":0.5,"BLK":3,"STL":2,"PF":2,"TO":2,"player":"Davis","first":"Anthony"},{"DATE":"12/6/2014","OPP":"@ LAC","OUTCOME":"L","PtF":100,"PA":120,"MIN":33,"PTS":26,"REB":3,"AST":0,"FGM":9,"FGA":15,"FGP":0.6,"m3p":0,"a3p":0,"P3P":0,"FTM":8,"FTA":9,"FTP":0.889,"BLK":1,"STL":0,"PF":3,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"12/7/2014","OPP":"@ LAL","OUTCOME":"W","PtF":104,"PA":87,"MIN":32,"PTS":23,"REB":6,"AST":0,"FGM":11,"FGA":15,"FGP":0.733,"m3p":0,"a3p":0,"P3P":0,"FTM":1,"FTA":2,"FTP":0.5,"BLK":2,"STL":1,"PF":3,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"12/9/2014","OPP":"vsNY","OUTCOME":"W","PtF":104,"PA":93,"MIN":33,"PTS":18,"REB":8,"AST":1,"FGM":6,"FGA":14,"FGP":0.429,"m3p":0,"a3p":0,"P3P":0,"FTM":6,"FTA":9,"FTP":0.667,"BLK":3,"STL":1,"PF":3,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"12/10/2014","OPP":"@ DAL","OUTCOME":"L","PtF":107,"PA":112,"MIN":40,"PTS":31,"REB":11,"AST":2,"FGM":11,"FGA":20,"FGP":0.55,"m3p":0,"a3p":0,"P3P":0,"FTM":9,"FTA":9,"FTP":1,"BLK":2,"STL":0,"PF":3,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"12/12/2014","OPP":"vsCLE","OUTCOME":"W","PtF":119,"PA":114,"MIN":7,"PTS":8,"REB":1,"AST":0,"FGM":4,"FGA":4,"FGP":1,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":2,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"12/14/2014","OPP":"vsGS","OUTCOME":"L","PtF":122,"PA":128,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"12/16/2014","OPP":"vsUTAH","OUTCOME":"W","PtF":119,"PA":111,"MIN":35,"PTS":31,"REB":9,"AST":1,"FGM":12,"FGA":15,"FGP":0.8,"m3p":0,"a3p":0,"P3P":0,"FTM":7,"FTA":7,"FTP":1,"BLK":3,"STL":1,"PF":2,"TO":2,"player":"Davis","first":"Anthony"},{"DATE":"12/18/2014","OPP":"@ HOU","OUTCOME":"W","PtF":99,"PA":90,"MIN":41,"PTS":30,"REB":14,"AST":3,"FGM":10,"FGA":17,"FGP":0.588,"m3p":0,"a3p":0,"P3P":0,"FTM":10,"FTA":12,"FTP":0.833,"BLK":5,"STL":2,"PF":0,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"12/20/2014","OPP":"vsPOR","OUTCOME":"L","PtF":88,"PA":114,"MIN":28,"PTS":7,"REB":6,"AST":0,"FGM":3,"FGA":14,"FGP":0.214,"m3p":0,"a3p":0,"P3P":0,"FTM":1,"FTA":1,"FTP":1,"BLK":5,"STL":1,"PF":1,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"12/21/2014","OPP":"@ OKC","OUTCOME":"W","PtF":101,"PA":99,"MIN":37,"PTS":38,"REB":12,"AST":2,"FGM":16,"FGA":22,"FGP":0.727,"m3p":0,"a3p":1,"P3P":0,"FTM":6,"FTA":7,"FTP":0.857,"BLK":3,"STL":1,"PF":3,"TO":3,"player":"Davis","first":"Anthony"},{"DATE":"12/23/2014","OPP":"@ IND","OUTCOME":"L","PtF":84,"PA":96,"MIN":37,"PTS":21,"REB":9,"AST":0,"FGM":8,"FGA":18,"FGP":0.444,"m3p":0,"a3p":0,"P3P":0,"FTM":5,"FTA":6,"FTP":0.833,"BLK":1,"STL":0,"PF":2,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"12/26/2014","OPP":"vsSA","OUTCOME":"W","PtF":97,"PA":90,"MIN":38,"PTS":22,"REB":12,"AST":3,"FGM":8,"FGA":19,"FGP":0.421,"m3p":0,"a3p":0,"P3P":0,"FTM":6,"FTA":6,"FTP":1,"BLK":5,"STL":2,"PF":1,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"12/27/2014","OPP":"@ CHI","OUTCOME":"L","PtF":100,"PA":107,"MIN":41,"PTS":29,"REB":11,"AST":2,"FGM":13,"FGA":24,"FGP":0.542,"m3p":0,"a3p":0,"P3P":0,"FTM":3,"FTA":3,"FTP":1,"BLK":6,"STL":0,"PF":2,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"12/30/2014","OPP":"vsPHX","OUTCOME":"W","PtF":110,"PA":106,"MIN":42,"PTS":19,"REB":18,"AST":2,"FGM":7,"FGA":15,"FGP":0.467,"m3p":0,"a3p":0,"P3P":0,"FTM":5,"FTA":5,"FTP":1,"BLK":1,"STL":0,"PF":2,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"12/31/2014","OPP":"@ SA","OUTCOME":"L","PtF":93,"PA":95,"MIN":40,"PTS":21,"REB":12,"AST":0,"FGM":8,"FGA":12,"FGP":0.667,"m3p":0,"a3p":0,"P3P":0,"FTM":5,"FTA":6,"FTP":0.833,"BLK":1,"STL":4,"PF":4,"TO":2,"player":"Davis","first":"Anthony"},{"DATE":"1/2/2015","OPP":"vsHOU","OUTCOME":"W","PtF":111,"PA":83,"MIN":31,"PTS":7,"REB":8,"AST":3,"FGM":2,"FGA":7,"FGP":0.286,"m3p":0,"a3p":0,"P3P":0,"FTM":3,"FTA":3,"FTP":1,"BLK":3,"STL":2,"PF":1,"TO":3,"player":"Davis","first":"Anthony"},{"DATE":"1/5/2015","OPP":"vsWSH","OUTCOME":"L","PtF":85,"PA":92,"MIN":39,"PTS":21,"REB":10,"AST":2,"FGM":9,"FGA":12,"FGP":0.75,"m3p":0,"a3p":0,"P3P":0,"FTM":3,"FTA":4,"FTP":0.75,"BLK":1,"STL":0,"PF":3,"TO":3,"player":"Davis","first":"Anthony"},{"DATE":"1/7/2015","OPP":"@ CHA","OUTCOME":"L","PtF":94,"PA":98,"MIN":42,"PTS":32,"REB":12,"AST":1,"FGM":15,"FGA":24,"FGP":0.625,"m3p":0,"a3p":0,"P3P":0,"FTM":2,"FTA":2,"FTP":1,"BLK":4,"STL":0,"PF":3,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"1/9/2015","OPP":"vsMEM","OUTCOME":"W","PtF":106,"PA":95,"MIN":37,"PTS":20,"REB":10,"AST":2,"FGM":7,"FGA":16,"FGP":0.438,"m3p":0,"a3p":0,"P3P":0,"FTM":6,"FTA":6,"FTP":1,"BLK":3,"STL":0,"PF":3,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"1/12/2015","OPP":"@ BOS","OUTCOME":"L","PtF":100,"PA":108,"MIN":40,"PTS":34,"REB":9,"AST":4,"FGM":10,"FGA":23,"FGP":0.435,"m3p":0,"a3p":0,"P3P":0,"FTM":14,"FTA":15,"FTP":0.933,"BLK":3,"STL":0,"PF":5,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"1/14/2015","OPP":"@ DET","OUTCOME":"W","PtF":105,"PA":94,"MIN":35,"PTS":27,"REB":10,"AST":1,"FGM":11,"FGA":18,"FGP":0.611,"m3p":0,"a3p":1,"P3P":0,"FTM":5,"FTA":5,"FTP":1,"BLK":4,"STL":1,"PF":2,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"1/16/2015","OPP":"@ PHI","OUTCOME":"L","PtF":81,"PA":96,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"1/18/2015","OPP":"@ TOR","OUTCOME":"W","PtF":95,"PA":93,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"1/19/2015","OPP":"@ NY","OUTCOME":"L","PtF":92,"PA":99,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"1/21/2015","OPP":"vsLAL","OUTCOME":"W","PtF":96,"PA":80,"MIN":37,"PTS":29,"REB":8,"AST":1,"FGM":11,"FGA":18,"FGP":0.611,"m3p":0,"a3p":0,"P3P":0,"FTM":7,"FTA":8,"FTP":0.875,"BLK":4,"STL":3,"PF":2,"TO":3,"player":"Davis","first":"Anthony"},{"DATE":"1/23/2015","OPP":"@ MIN","OUTCOME":"W","PtF":92,"PA":84,"MIN":42,"PTS":21,"REB":12,"AST":0,"FGM":9,"FGA":23,"FGP":0.391,"m3p":0,"a3p":0,"P3P":0,"FTM":3,"FTA":4,"FTP":0.75,"BLK":1,"STL":2,"PF":2,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"1/25/2015","OPP":"vsDAL","OUTCOME":"W","PtF":109,"PA":106,"MIN":39,"PTS":28,"REB":10,"AST":3,"FGM":8,"FGA":18,"FGP":0.444,"m3p":0,"a3p":0,"P3P":0,"FTM":12,"FTA":14,"FTP":0.857,"BLK":1,"STL":5,"PF":3,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"1/26/2015","OPP":"vsPHI","OUTCOME":"W","PtF":99,"PA":74,"MIN":31,"PTS":32,"REB":10,"AST":3,"FGM":12,"FGA":19,"FGP":0.632,"m3p":0,"a3p":0,"P3P":0,"FTM":8,"FTA":8,"FTP":1,"BLK":4,"STL":3,"PF":3,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"1/28/2015","OPP":"vsDEN","OUTCOME":"L","PtF":85,"PA":93,"MIN":37,"PTS":24,"REB":12,"AST":2,"FGM":8,"FGA":15,"FGP":0.533,"m3p":0,"a3p":0,"P3P":0,"FTM":8,"FTA":10,"FTP":0.8,"BLK":2,"STL":0,"PF":2,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"1/30/2015","OPP":"vsLAC","OUTCOME":"W","PtF":108,"PA":103,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"2/2/2015","OPP":"vsATL","OUTCOME":"W","PtF":115,"PA":100,"MIN":34,"PTS":29,"REB":13,"AST":3,"FGM":12,"FGA":23,"FGP":0.522,"m3p":0,"a3p":0,"P3P":0,"FTM":5,"FTA":7,"FTP":0.714,"BLK":2,"STL":1,"PF":2,"TO":2,"player":"Davis","first":"Anthony"},{"DATE":"2/4/2015","OPP":"vsOKC","OUTCOME":"L","PtF":91,"PA":102,"MIN":39,"PTS":23,"REB":9,"AST":2,"FGM":9,"FGA":21,"FGP":0.429,"m3p":0,"a3p":0,"P3P":0,"FTM":5,"FTA":6,"FTP":0.833,"BLK":1,"STL":1,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"2/6/2015","OPP":"@ OKC","OUTCOME":"W","PtF":116,"PA":113,"MIN":44,"PTS":41,"REB":10,"AST":3,"FGM":15,"FGA":23,"FGP":0.652,"m3p":1,"a3p":1,"P3P":1,"FTM":10,"FTA":10,"FTP":1,"BLK":2,"STL":0,"PF":2,"TO":3,"player":"Davis","first":"Anthony"},{"DATE":"2/7/2015","OPP":"vsCHI","OUTCOME":"L","PtF":72,"PA":107,"MIN":13,"PTS":8,"REB":5,"AST":1,"FGM":4,"FGA":11,"FGP":0.364,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":1,"STL":1,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"2/9/2015","OPP":"vsUTAH","OUTCOME":"L","PtF":96,"PA":100,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"2/11/2015","OPP":"vsIND","OUTCOME":"L","PtF":93,"PA":106,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"2/20/2015","OPP":"@ ORL","OUTCOME":"L","PtF":84,"PA":95,"MIN":38,"PTS":13,"REB":11,"AST":1,"FGM":5,"FGA":15,"FGP":0.333,"m3p":0,"a3p":0,"P3P":0,"FTM":3,"FTA":4,"FTP":0.75,"BLK":2,"STL":0,"PF":1,"TO":4,"player":"Davis","first":"Anthony"},{"DATE":"2/21/2015","OPP":"@ MIA","OUTCOME":"W","PtF":105,"PA":91,"MIN":9,"PTS":6,"REB":6,"AST":0,"FGM":2,"FGA":8,"FGP":0.25,"m3p":0,"a3p":0,"P3P":0,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":0,"PF":0,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"2/23/2015","OPP":"vsTOR","OUTCOME":"W","PtF":100,"PA":97,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"2/25/2015","OPP":"vsBKN","OUTCOME":"W","PtF":102,"PA":96,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"2/27/2015","OPP":"vsMIA","OUTCOME":"W","PtF":104,"PA":102,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"3/1/2015","OPP":"@ DEN","OUTCOME":"W","PtF":99,"PA":92,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"3/2/2015","OPP":"@ DAL","OUTCOME":"L","PtF":93,"PA":102,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"3/4/2015","OPP":"vsDET","OUTCOME":"W","PtF":88,"PA":85,"MIN":42,"PTS":39,"REB":13,"AST":2,"FGM":17,"FGA":30,"FGP":0.567,"m3p":0,"a3p":0,"P3P":0,"FTM":5,"FTA":7,"FTP":0.714,"BLK":8,"STL":3,"PF":1,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"3/6/2015","OPP":"vsBOS","OUTCOME":"L","PtF":98,"PA":104,"MIN":42,"PTS":29,"REB":14,"AST":2,"FGM":11,"FGA":21,"FGP":0.524,"m3p":0,"a3p":0,"P3P":0,"FTM":7,"FTA":8,"FTP":0.875,"BLK":3,"STL":1,"PF":2,"TO":3,"player":"Davis","first":"Anthony"},{"DATE":"3/7/2015","OPP":"vsMEM","OUTCOME":"W","PtF":95,"PA":89,"MIN":40,"PTS":23,"REB":10,"AST":1,"FGM":11,"FGA":21,"FGP":0.524,"m3p":0,"a3p":0,"P3P":0,"FTM":1,"FTA":1,"FTP":1,"BLK":5,"STL":0,"PF":4,"TO":2,"player":"Davis","first":"Anthony"},{"DATE":"3/9/2015","OPP":"@ MIL","OUTCOME":"W","PtF":114,"PA":103,"MIN":41,"PTS":43,"REB":10,"AST":6,"FGM":17,"FGA":23,"FGP":0.739,"m3p":0,"a3p":0,"P3P":0,"FTM":9,"FTA":11,"FTP":0.818,"BLK":2,"STL":1,"PF":3,"TO":4,"player":"Davis","first":"Anthony"},{"DATE":"3/10/2015","OPP":"@ BKN","OUTCOME":"W","PtF":111,"PA":91,"MIN":28,"PTS":15,"REB":6,"AST":5,"FGM":5,"FGA":16,"FGP":0.313,"m3p":0,"a3p":0,"P3P":0,"FTM":5,"FTA":6,"FTP":0.833,"BLK":3,"STL":0,"PF":2,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"3/15/2015","OPP":"vsDEN","OUTCOME":"L","PtF":111,"PA":118,"MIN":50,"PTS":36,"REB":14,"AST":7,"FGM":16,"FGA":28,"FGP":0.571,"m3p":0,"a3p":1,"P3P":0,"FTM":4,"FTA":6,"FTP":0.667,"BLK":9,"STL":1,"PF":1,"TO":2,"player":"Davis","first":"Anthony"},{"DATE":"3/17/2015","OPP":"vsMIL","OUTCOME":"W","PtF":85,"PA":84,"MIN":42,"PTS":20,"REB":12,"AST":4,"FGM":6,"FGA":18,"FGP":0.333,"m3p":0,"a3p":0,"P3P":0,"FTM":8,"FTA":9,"FTP":0.889,"BLK":3,"STL":1,"PF":4,"TO":2,"player":"Davis","first":"Anthony"},{"DATE":"3/19/2015","OPP":"@ PHX","OUTCOME":"L","PtF":72,"PA":74,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"3/20/2015","OPP":"@ GS","OUTCOME":"L","PtF":96,"PA":112,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"3/22/2015","OPP":"@ LAC","OUTCOME":"L","PtF":100,"PA":107,"MIN":39,"PTS":26,"REB":12,"AST":5,"FGM":11,"FGA":13,"FGP":0.846,"m3p":0,"a3p":0,"P3P":0,"FTM":4,"FTA":5,"FTP":0.8,"BLK":2,"STL":2,"PF":4,"TO":2,"player":"Davis","first":"Anthony"},{"DATE":"3/25/2015","OPP":"vsHOU","OUTCOME":"L","PtF":93,"PA":95,"MIN":44,"PTS":24,"REB":14,"AST":1,"FGM":9,"FGA":18,"FGP":0.5,"m3p":0,"a3p":0,"P3P":0,"FTM":6,"FTA":14,"FTP":0.429,"BLK":3,"STL":3,"PF":1,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"3/27/2015","OPP":"vsSAC","OUTCOME":"W","PtF":102,"PA":88,"MIN":41,"PTS":24,"REB":9,"AST":2,"FGM":11,"FGA":26,"FGP":0.423,"m3p":0,"a3p":0,"P3P":0,"FTM":2,"FTA":3,"FTP":0.667,"BLK":6,"STL":1,"PF":3,"TO":2,"player":"Davis","first":"Anthony"},{"DATE":"3/29/2015","OPP":"vsMIN","OUTCOME":"W","PtF":110,"PA":88,"MIN":37,"PTS":28,"REB":9,"AST":5,"FGM":10,"FGA":21,"FGP":0.476,"m3p":0,"a3p":0,"P3P":0,"FTM":8,"FTA":10,"FTP":0.8,"BLK":1,"STL":2,"PF":3,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"4/1/2015","OPP":"@ LAL","OUTCOME":"W","PtF":113,"PA":92,"MIN":35,"PTS":20,"REB":7,"AST":6,"FGM":9,"FGA":15,"FGP":0.6,"m3p":0,"a3p":0,"P3P":0,"FTM":2,"FTA":2,"FTP":1,"BLK":4,"STL":1,"PF":1,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"4/3/2015","OPP":"@ SAC","OUTCOME":"W","PtF":101,"PA":95,"MIN":35,"PTS":20,"REB":10,"AST":3,"FGM":6,"FGA":17,"FGP":0.353,"m3p":0,"a3p":0,"P3P":0,"FTM":8,"FTA":10,"FTP":0.8,"BLK":2,"STL":0,"PF":3,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"4/4/2015","OPP":"@ POR","OUTCOME":"L","PtF":90,"PA":99,"MIN":37,"PTS":19,"REB":9,"AST":2,"FGM":8,"FGA":20,"FGP":0.4,"m3p":0,"a3p":0,"P3P":0,"FTM":3,"FTA":3,"FTP":1,"BLK":4,"STL":3,"PF":2,"TO":2,"player":"Davis","first":"Anthony"},{"DATE":"4/7/2015","OPP":"vsGS","OUTCOME":"W","PtF":103,"PA":100,"MIN":40,"PTS":29,"REB":10,"AST":2,"FGM":10,"FGA":21,"FGP":0.476,"m3p":0,"a3p":2,"P3P":0,"FTM":9,"FTA":12,"FTP":0.75,"BLK":4,"STL":2,"PF":1,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"4/8/2015","OPP":"@ MEM","OUTCOME":"L","PtF":74,"PA":110,"MIN":28,"PTS":12,"REB":5,"AST":1,"FGM":5,"FGA":9,"FGP":0.556,"m3p":0,"a3p":0,"P3P":0,"FTM":2,"FTA":2,"FTP":1,"BLK":2,"STL":1,"PF":2,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"4/10/2015","OPP":"vsPHX","OUTCOME":"W","PtF":90,"PA":75,"MIN":23,"PTS":19,"REB":9,"AST":4,"FGM":7,"FGA":12,"FGP":0.583,"m3p":0,"a3p":0,"P3P":0,"FTM":5,"FTA":11,"FTP":0.455,"BLK":1,"STL":1,"PF":0,"TO":0,"player":"Davis","first":"Anthony"},{"DATE":"4/12/2015","OPP":"@ HOU","OUTCOME":"L","PtF":114,"PA":121,"MIN":40,"PTS":27,"REB":7,"AST":4,"FGM":10,"FGA":16,"FGP":0.625,"m3p":0,"a3p":0,"P3P":0,"FTM":7,"FTA":10,"FTP":0.7,"BLK":1,"STL":2,"PF":4,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"4/13/2015","OPP":"@ MIN","OUTCOME":"W","PtF":100,"PA":88,"MIN":36,"PTS":24,"REB":11,"AST":5,"FGM":8,"FGA":14,"FGP":0.571,"m3p":0,"a3p":0,"P3P":0,"FTM":8,"FTA":8,"FTP":1,"BLK":6,"STL":2,"PF":3,"TO":1,"player":"Davis","first":"Anthony"},{"DATE":"4/15/2015","OPP":"vsSAS","OUTCOME":"W","PtF":108,"PA":103,"MIN":43,"PTS":31,"REB":13,"AST":2,"FGM":12,"FGA":26,"FGP":0.462,"m3p":0,"a3p":0,"P3P":0,"FTM":7,"FTA":9,"FTP":0.778,"BLK":3,"STL":2,"PF":1,"TO":6,"player":"Davis","first":"Anthony"},{"DATE":"10/29/2014","OPP":"@ SAC","OUTCOME":"W","PtF":95,"PA":77,"MIN":37,"PTS":24,"REB":10,"AST":5,"FGM":7,"FGA":17,"FGP":0.412,"m3p":2,"a3p":9,"P3P":0.222,"FTM":8,"FTA":9,"FTP":0.889,"BLK":0,"STL":6,"PF":5,"TO":4,"player":"Curry","first":"Stephen"},{"DATE":"11/1/2014","OPP":"vsLAL","OUTCOME":"W","PtF":127,"PA":104,"MIN":34,"PTS":31,"REB":5,"AST":10,"FGM":10,"FGA":19,"FGP":0.526,"m3p":3,"a3p":8,"P3P":0.375,"FTM":8,"FTA":8,"FTP":1,"BLK":0,"STL":3,"PF":3,"TO":2,"player":"Curry","first":"Stephen"},{"DATE":"11/2/2014","OPP":"@ POR","OUTCOME":"W","PtF":95,"PA":90,"MIN":31,"PTS":21,"REB":5,"AST":6,"FGM":6,"FGA":18,"FGP":0.333,"m3p":1,"a3p":5,"P3P":0.2,"FTM":8,"FTA":8,"FTP":1,"BLK":0,"STL":2,"PF":1,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"11/5/2014","OPP":"vsLAC","OUTCOME":"W","PtF":121,"PA":104,"MIN":34,"PTS":28,"REB":6,"AST":7,"FGM":9,"FGA":18,"FGP":0.5,"m3p":4,"a3p":8,"P3P":0.5,"FTM":6,"FTA":6,"FTP":1,"BLK":0,"STL":1,"PF":4,"TO":5,"player":"Curry","first":"Stephen"},{"DATE":"11/8/2014","OPP":"@ HOU","OUTCOME":"W","PtF":98,"PA":87,"MIN":40,"PTS":34,"REB":9,"AST":5,"FGM":13,"FGA":19,"FGP":0.684,"m3p":6,"a3p":9,"P3P":0.667,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":4,"PF":2,"TO":5,"player":"Curry","first":"Stephen"},{"DATE":"11/9/2014","OPP":"@ PHX","OUTCOME":"L","PtF":95,"PA":107,"MIN":34,"PTS":28,"REB":2,"AST":10,"FGM":10,"FGA":20,"FGP":0.5,"m3p":4,"a3p":10,"P3P":0.4,"FTM":4,"FTA":4,"FTP":1,"BLK":0,"STL":5,"PF":5,"TO":10,"player":"Curry","first":"Stephen"},{"DATE":"11/11/2014","OPP":"vsSA","OUTCOME":"L","PtF":100,"PA":113,"MIN":36,"PTS":16,"REB":6,"AST":5,"FGM":7,"FGA":18,"FGP":0.389,"m3p":0,"a3p":7,"P3P":0,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":0,"PF":2,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"11/13/2014","OPP":"vsBKN","OUTCOME":"W","PtF":107,"PA":99,"MIN":32,"PTS":17,"REB":3,"AST":5,"FGM":6,"FGA":12,"FGP":0.5,"m3p":3,"a3p":7,"P3P":0.429,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":0,"PF":5,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"11/15/2014","OPP":"vsCHA","OUTCOME":"W","PtF":112,"PA":87,"MIN":28,"PTS":19,"REB":5,"AST":9,"FGM":8,"FGA":15,"FGP":0.533,"m3p":3,"a3p":6,"P3P":0.5,"FTM":0,"FTA":1,"FTP":0,"BLK":0,"STL":1,"PF":3,"TO":1,"player":"Curry","first":"Stephen"},{"DATE":"11/16/2014","OPP":"@ LAL","OUTCOME":"W","PtF":136,"PA":115,"MIN":30,"PTS":30,"REB":4,"AST":15,"FGM":10,"FGA":19,"FGP":0.526,"m3p":5,"a3p":9,"P3P":0.556,"FTM":5,"FTA":5,"FTP":1,"BLK":0,"STL":1,"PF":0,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"11/21/2014","OPP":"vsUTAH","OUTCOME":"W","PtF":101,"PA":88,"MIN":25,"PTS":8,"REB":5,"AST":10,"FGM":3,"FGA":9,"FGP":0.333,"m3p":2,"a3p":6,"P3P":0.333,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":2,"PF":2,"TO":4,"player":"Curry","first":"Stephen"},{"DATE":"11/23/2014","OPP":"@ OKC","OUTCOME":"W","PtF":91,"PA":86,"MIN":38,"PTS":15,"REB":6,"AST":6,"FGM":5,"FGA":15,"FGP":0.333,"m3p":2,"a3p":6,"P3P":0.333,"FTM":3,"FTA":4,"FTP":0.75,"BLK":0,"STL":1,"PF":0,"TO":1,"player":"Curry","first":"Stephen"},{"DATE":"11/25/2014","OPP":"@ MIA","OUTCOME":"W","PtF":114,"PA":97,"MIN":37,"PTS":40,"REB":6,"AST":7,"FGM":12,"FGA":19,"FGP":0.632,"m3p":8,"a3p":11,"P3P":0.727,"FTM":8,"FTA":9,"FTP":0.889,"BLK":1,"STL":3,"PF":2,"TO":2,"player":"Curry","first":"Stephen"},{"DATE":"11/26/2014","OPP":"@ ORL","OUTCOME":"W","PtF":111,"PA":96,"MIN":24,"PTS":28,"REB":5,"AST":8,"FGM":9,"FGA":13,"FGP":0.692,"m3p":6,"a3p":8,"P3P":0.75,"FTM":4,"FTA":5,"FTP":0.8,"BLK":0,"STL":0,"PF":1,"TO":1,"player":"Curry","first":"Stephen"},{"DATE":"11/28/2014","OPP":"@ CHA","OUTCOME":"W","PtF":106,"PA":101,"MIN":32,"PTS":26,"REB":4,"AST":6,"FGM":9,"FGA":20,"FGP":0.45,"m3p":1,"a3p":10,"P3P":0.1,"FTM":7,"FTA":7,"FTP":1,"BLK":2,"STL":1,"PF":4,"TO":4,"player":"Curry","first":"Stephen"},{"DATE":"11/30/2014","OPP":"@ DET","OUTCOME":"W","PtF":104,"PA":93,"MIN":28,"PTS":16,"REB":3,"AST":10,"FGM":5,"FGA":9,"FGP":0.556,"m3p":1,"a3p":3,"P3P":0.333,"FTM":5,"FTA":6,"FTP":0.833,"BLK":0,"STL":1,"PF":2,"TO":1,"player":"Curry","first":"Stephen"},{"DATE":"12/2/2014","OPP":"vsORL","OUTCOME":"W","PtF":98,"PA":97,"MIN":36,"PTS":22,"REB":4,"AST":5,"FGM":8,"FGA":17,"FGP":0.471,"m3p":3,"a3p":9,"P3P":0.333,"FTM":3,"FTA":3,"FTP":1,"BLK":0,"STL":0,"PF":2,"TO":4,"player":"Curry","first":"Stephen"},{"DATE":"12/4/2014","OPP":"vsNO","OUTCOME":"W","PtF":112,"PA":85,"MIN":31,"PTS":19,"REB":3,"AST":11,"FGM":8,"FGA":17,"FGP":0.471,"m3p":3,"a3p":6,"P3P":0.5,"FTM":0,"FTA":0,"FTP":0,"BLK":1,"STL":4,"PF":0,"TO":2,"player":"Curry","first":"Stephen"},{"DATE":"12/6/2014","OPP":"@ CHI","OUTCOME":"W","PtF":112,"PA":102,"MIN":36,"PTS":19,"REB":6,"AST":7,"FGM":5,"FGA":14,"FGP":0.357,"m3p":1,"a3p":5,"P3P":0.2,"FTM":8,"FTA":9,"FTP":0.889,"BLK":0,"STL":1,"PF":2,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"12/8/2014","OPP":"@ MIN","OUTCOME":"W","PtF":102,"PA":86,"MIN":31,"PTS":21,"REB":4,"AST":7,"FGM":10,"FGA":20,"FGP":0.5,"m3p":1,"a3p":7,"P3P":0.143,"FTM":0,"FTA":0,"FTP":0,"BLK":1,"STL":3,"PF":3,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"12/10/2014","OPP":"vsHOU","OUTCOME":"W","PtF":105,"PA":93,"MIN":37,"PTS":20,"REB":7,"AST":7,"FGM":8,"FGA":15,"FGP":0.533,"m3p":3,"a3p":5,"P3P":0.6,"FTM":1,"FTA":1,"FTP":1,"BLK":0,"STL":0,"PF":3,"TO":4,"player":"Curry","first":"Stephen"},{"DATE":"12/13/2014","OPP":"@ DAL","OUTCOME":"W","PtF":105,"PA":98,"MIN":37,"PTS":29,"REB":3,"AST":8,"FGM":9,"FGA":22,"FGP":0.409,"m3p":3,"a3p":11,"P3P":0.273,"FTM":8,"FTA":9,"FTP":0.889,"BLK":0,"STL":2,"PF":1,"TO":1,"player":"Curry","first":"Stephen"},{"DATE":"12/14/2014","OPP":"@ NO","OUTCOME":"W","PtF":128,"PA":122,"MIN":40,"PTS":34,"REB":7,"AST":7,"FGM":11,"FGA":21,"FGP":0.524,"m3p":6,"a3p":10,"P3P":0.6,"FTM":6,"FTA":6,"FTP":1,"BLK":0,"STL":1,"PF":1,"TO":6,"player":"Curry","first":"Stephen"},{"DATE":"12/16/2014","OPP":"@ MEM","OUTCOME":"L","PtF":98,"PA":105,"MIN":39,"PTS":19,"REB":7,"AST":6,"FGM":9,"FGA":25,"FGP":0.36,"m3p":1,"a3p":10,"P3P":0.1,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":4,"PF":2,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"12/18/2014","OPP":"vsOKC","OUTCOME":"W","PtF":114,"PA":109,"MIN":42,"PTS":34,"REB":7,"AST":9,"FGM":14,"FGA":24,"FGP":0.583,"m3p":5,"a3p":12,"P3P":0.417,"FTM":1,"FTA":1,"FTP":1,"BLK":1,"STL":4,"PF":0,"TO":4,"player":"Curry","first":"Stephen"},{"DATE":"12/22/2014","OPP":"vsSAC","OUTCOME":"W","PtF":128,"PA":108,"MIN":26,"PTS":12,"REB":2,"AST":11,"FGM":5,"FGA":12,"FGP":0.417,"m3p":1,"a3p":5,"P3P":0.2,"FTM":1,"FTA":1,"FTP":1,"BLK":0,"STL":3,"PF":1,"TO":1,"player":"Curry","first":"Stephen"},{"DATE":"12/23/2014","OPP":"@ LAL","OUTCOME":"L","PtF":105,"PA":115,"MIN":30,"PTS":22,"REB":2,"AST":6,"FGM":10,"FGA":14,"FGP":0.714,"m3p":2,"a3p":5,"P3P":0.4,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":3,"PF":0,"TO":7,"player":"Curry","first":"Stephen"},{"DATE":"12/25/2014","OPP":"@ LAC","OUTCOME":"L","PtF":86,"PA":100,"MIN":36,"PTS":14,"REB":9,"AST":7,"FGM":5,"FGA":12,"FGP":0.417,"m3p":1,"a3p":5,"P3P":0.2,"FTM":3,"FTA":4,"FTP":0.75,"BLK":0,"STL":2,"PF":3,"TO":5,"player":"Curry","first":"Stephen"},{"DATE":"12/27/2014","OPP":"vsMIN","OUTCOME":"W","PtF":110,"PA":97,"MIN":31,"PTS":25,"REB":3,"AST":6,"FGM":9,"FGA":18,"FGP":0.5,"m3p":4,"a3p":9,"P3P":0.444,"FTM":3,"FTA":4,"FTP":0.75,"BLK":0,"STL":3,"PF":3,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"12/30/2014","OPP":"vsPHI","OUTCOME":"W","PtF":126,"PA":86,"MIN":27,"PTS":13,"REB":3,"AST":9,"FGM":4,"FGA":7,"FGP":0.571,"m3p":3,"a3p":6,"P3P":0.5,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":3,"PF":3,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"1/2/2015","OPP":"vsTOR","OUTCOME":"W","PtF":126,"PA":105,"MIN":34,"PTS":32,"REB":5,"AST":12,"FGM":10,"FGA":18,"FGP":0.556,"m3p":5,"a3p":11,"P3P":0.455,"FTM":7,"FTA":7,"FTP":1,"BLK":0,"STL":1,"PF":3,"TO":0,"player":"Curry","first":"Stephen"},{"DATE":"1/5/2015","OPP":"vsOKC","OUTCOME":"W","PtF":117,"PA":91,"MIN":31,"PTS":19,"REB":9,"AST":6,"FGM":7,"FGA":14,"FGP":0.5,"m3p":1,"a3p":4,"P3P":0.25,"FTM":4,"FTA":4,"FTP":1,"BLK":0,"STL":4,"PF":2,"TO":2,"player":"Curry","first":"Stephen"},{"DATE":"1/7/2015","OPP":"vsIND","OUTCOME":"W","PtF":117,"PA":102,"MIN":35,"PTS":21,"REB":3,"AST":15,"FGM":8,"FGA":17,"FGP":0.471,"m3p":4,"a3p":8,"P3P":0.5,"FTM":1,"FTA":1,"FTP":1,"BLK":0,"STL":4,"PF":5,"TO":5,"player":"Curry","first":"Stephen"},{"DATE":"1/9/2015","OPP":"vsCLE","OUTCOME":"W","PtF":112,"PA":94,"MIN":35,"PTS":23,"REB":1,"AST":10,"FGM":8,"FGA":15,"FGP":0.533,"m3p":3,"a3p":8,"P3P":0.375,"FTM":4,"FTA":4,"FTP":1,"BLK":0,"STL":2,"PF":1,"TO":1,"player":"Curry","first":"Stephen"},{"DATE":"1/13/2015","OPP":"@ UTAH","OUTCOME":"W","PtF":116,"PA":105,"MIN":29,"PTS":27,"REB":2,"AST":11,"FGM":10,"FGA":16,"FGP":0.625,"m3p":4,"a3p":9,"P3P":0.444,"FTM":3,"FTA":3,"FTP":1,"BLK":0,"STL":0,"PF":2,"TO":2,"player":"Curry","first":"Stephen"},{"DATE":"1/14/2015","OPP":"vsMIA","OUTCOME":"W","PtF":104,"PA":89,"MIN":32,"PTS":32,"REB":5,"AST":3,"FGM":11,"FGA":19,"FGP":0.579,"m3p":7,"a3p":10,"P3P":0.7,"FTM":3,"FTA":5,"FTP":0.6,"BLK":0,"STL":2,"PF":1,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"1/16/2015","OPP":"@ OKC","OUTCOME":"L","PtF":115,"PA":127,"MIN":31,"PTS":19,"REB":3,"AST":6,"FGM":6,"FGA":13,"FGP":0.462,"m3p":2,"a3p":6,"P3P":0.333,"FTM":5,"FTA":5,"FTP":1,"BLK":0,"STL":2,"PF":2,"TO":6,"player":"Curry","first":"Stephen"},{"DATE":"1/17/2015","OPP":"@ HOU","OUTCOME":"W","PtF":131,"PA":106,"MIN":33,"PTS":27,"REB":7,"AST":11,"FGM":9,"FGA":18,"FGP":0.5,"m3p":2,"a3p":8,"P3P":0.25,"FTM":7,"FTA":8,"FTP":0.875,"BLK":1,"STL":3,"PF":0,"TO":2,"player":"Curry","first":"Stephen"},{"DATE":"1/19/2015","OPP":"vsDEN","OUTCOME":"W","PtF":122,"PA":79,"MIN":25,"PTS":20,"REB":1,"AST":8,"FGM":7,"FGA":13,"FGP":0.538,"m3p":2,"a3p":5,"P3P":0.4,"FTM":4,"FTA":4,"FTP":1,"BLK":2,"STL":1,"PF":2,"TO":1,"player":"Curry","first":"Stephen"},{"DATE":"1/21/2015","OPP":"vsHOU","OUTCOME":"W","PtF":126,"PA":113,"MIN":34,"PTS":22,"REB":3,"AST":10,"FGM":7,"FGA":12,"FGP":0.583,"m3p":3,"a3p":5,"P3P":0.6,"FTM":5,"FTA":6,"FTP":0.833,"BLK":0,"STL":1,"PF":3,"TO":6,"player":"Curry","first":"Stephen"},{"DATE":"1/23/2015","OPP":"vsSAC","OUTCOME":"W","PtF":126,"PA":101,"MIN":29,"PTS":10,"REB":4,"AST":11,"FGM":3,"FGA":11,"FGP":0.273,"m3p":2,"a3p":7,"P3P":0.286,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":2,"PF":0,"TO":6,"player":"Curry","first":"Stephen"},{"DATE":"1/25/2015","OPP":"vsBOS","OUTCOME":"W","PtF":114,"PA":111,"MIN":33,"PTS":22,"REB":4,"AST":11,"FGM":6,"FGA":16,"FGP":0.375,"m3p":1,"a3p":5,"P3P":0.2,"FTM":9,"FTA":9,"FTP":1,"BLK":0,"STL":2,"PF":1,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"1/27/2015","OPP":"vsCHI","OUTCOME":"L","PtF":111,"PA":113,"MIN":43,"PTS":21,"REB":4,"AST":9,"FGM":9,"FGA":23,"FGP":0.391,"m3p":2,"a3p":9,"P3P":0.222,"FTM":1,"FTA":1,"FTP":1,"BLK":0,"STL":3,"PF":3,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"1/30/2015","OPP":"@ UTAH","OUTCOME":"L","PtF":100,"PA":110,"MIN":34,"PTS":32,"REB":7,"AST":6,"FGM":10,"FGA":22,"FGP":0.455,"m3p":5,"a3p":11,"P3P":0.455,"FTM":7,"FTA":9,"FTP":0.778,"BLK":0,"STL":3,"PF":1,"TO":2,"player":"Curry","first":"Stephen"},{"DATE":"1/31/2015","OPP":"vsPHX","OUTCOME":"W","PtF":106,"PA":87,"MIN":37,"PTS":25,"REB":4,"AST":7,"FGM":9,"FGA":23,"FGP":0.391,"m3p":4,"a3p":9,"P3P":0.444,"FTM":3,"FTA":3,"FTP":1,"BLK":1,"STL":2,"PF":2,"TO":1,"player":"Curry","first":"Stephen"},{"DATE":"2/3/2015","OPP":"@ SAC","OUTCOME":"W","PtF":121,"PA":96,"MIN":29,"PTS":23,"REB":5,"AST":9,"FGM":6,"FGA":15,"FGP":0.4,"m3p":2,"a3p":6,"P3P":0.333,"FTM":9,"FTA":10,"FTP":0.9,"BLK":0,"STL":3,"PF":2,"TO":2,"player":"Curry","first":"Stephen"},{"DATE":"2/4/2015","OPP":"vsDAL","OUTCOME":"W","PtF":128,"PA":114,"MIN":37,"PTS":51,"REB":4,"AST":4,"FGM":16,"FGA":26,"FGP":0.615,"m3p":10,"a3p":16,"P3P":0.625,"FTM":9,"FTA":11,"FTP":0.818,"BLK":0,"STL":1,"PF":3,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"2/6/2015","OPP":"@ ATL","OUTCOME":"L","PtF":116,"PA":124,"MIN":36,"PTS":26,"REB":2,"AST":9,"FGM":8,"FGA":19,"FGP":0.421,"m3p":4,"a3p":9,"P3P":0.444,"FTM":6,"FTA":7,"FTP":0.857,"BLK":2,"STL":2,"PF":3,"TO":1,"player":"Curry","first":"Stephen"},{"DATE":"2/7/2015","OPP":"@ NY","OUTCOME":"W","PtF":106,"PA":92,"MIN":31,"PTS":22,"REB":6,"AST":0,"FGM":8,"FGA":18,"FGP":0.444,"m3p":5,"a3p":9,"P3P":0.556,"FTM":1,"FTA":3,"FTP":0.333,"BLK":0,"STL":2,"PF":0,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"2/9/2015","OPP":"@ PHI","OUTCOME":"W","PtF":89,"PA":84,"MIN":32,"PTS":20,"REB":5,"AST":6,"FGM":7,"FGA":20,"FGP":0.35,"m3p":3,"a3p":12,"P3P":0.25,"FTM":3,"FTA":4,"FTP":0.75,"BLK":0,"STL":3,"PF":2,"TO":1,"player":"Curry","first":"Stephen"},{"DATE":"2/11/2015","OPP":"@ MIN","OUTCOME":"W","PtF":94,"PA":91,"MIN":36,"PTS":25,"REB":5,"AST":8,"FGM":9,"FGA":23,"FGP":0.391,"m3p":2,"a3p":10,"P3P":0.2,"FTM":5,"FTA":5,"FTP":1,"BLK":0,"STL":3,"PF":1,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"2/20/2015","OPP":"vsSA","OUTCOME":"W","PtF":110,"PA":99,"MIN":32,"PTS":25,"REB":4,"AST":11,"FGM":8,"FGA":17,"FGP":0.471,"m3p":4,"a3p":8,"P3P":0.5,"FTM":5,"FTA":5,"FTP":1,"BLK":0,"STL":4,"PF":2,"TO":2,"player":"Curry","first":"Stephen"},{"DATE":"2/22/2015","OPP":"@ IND","OUTCOME":"L","PtF":98,"PA":104,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Curry","first":"Stephen"},{"DATE":"2/24/2015","OPP":"@ WSH","OUTCOME":"W","PtF":114,"PA":107,"MIN":34,"PTS":32,"REB":0,"AST":8,"FGM":11,"FGA":18,"FGP":0.611,"m3p":5,"a3p":9,"P3P":0.556,"FTM":5,"FTA":5,"FTP":1,"BLK":0,"STL":2,"PF":1,"TO":0,"player":"Curry","first":"Stephen"},{"DATE":"2/26/2015","OPP":"@ CLE","OUTCOME":"L","PtF":99,"PA":110,"MIN":34,"PTS":18,"REB":3,"AST":6,"FGM":5,"FGA":17,"FGP":0.294,"m3p":3,"a3p":9,"P3P":0.333,"FTM":5,"FTA":5,"FTP":1,"BLK":0,"STL":1,"PF":3,"TO":4,"player":"Curry","first":"Stephen"},{"DATE":"2/27/2015","OPP":"@ TOR","OUTCOME":"W","PtF":113,"PA":89,"MIN":25,"PTS":22,"REB":6,"AST":5,"FGM":8,"FGA":13,"FGP":0.615,"m3p":3,"a3p":4,"P3P":0.75,"FTM":3,"FTA":3,"FTP":1,"BLK":0,"STL":2,"PF":0,"TO":2,"player":"Curry","first":"Stephen"},{"DATE":"3/1/2015","OPP":"@ BOS","OUTCOME":"W","PtF":106,"PA":101,"MIN":38,"PTS":37,"REB":4,"AST":5,"FGM":14,"FGA":22,"FGP":0.636,"m3p":5,"a3p":8,"P3P":0.625,"FTM":4,"FTA":4,"FTP":1,"BLK":0,"STL":1,"PF":1,"TO":5,"player":"Curry","first":"Stephen"},{"DATE":"3/2/2015","OPP":"@ BKN","OUTCOME":"L","PtF":108,"PA":110,"MIN":29,"PTS":26,"REB":2,"AST":7,"FGM":8,"FGA":16,"FGP":0.5,"m3p":6,"a3p":12,"P3P":0.5,"FTM":4,"FTA":4,"FTP":1,"BLK":0,"STL":1,"PF":4,"TO":4,"player":"Curry","first":"Stephen"},{"DATE":"3/4/2015","OPP":"vsMIL","OUTCOME":"W","PtF":102,"PA":93,"MIN":37,"PTS":19,"REB":1,"AST":11,"FGM":6,"FGA":16,"FGP":0.375,"m3p":6,"a3p":13,"P3P":0.462,"FTM":1,"FTA":2,"FTP":0.5,"BLK":2,"STL":2,"PF":3,"TO":5,"player":"Curry","first":"Stephen"},{"DATE":"3/6/2015","OPP":"vsDAL","OUTCOME":"W","PtF":104,"PA":89,"MIN":31,"PTS":22,"REB":2,"AST":7,"FGM":6,"FGA":11,"FGP":0.545,"m3p":5,"a3p":8,"P3P":0.625,"FTM":5,"FTA":6,"FTP":0.833,"BLK":1,"STL":2,"PF":2,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"3/8/2015","OPP":"vsLAC","OUTCOME":"W","PtF":106,"PA":98,"MIN":32,"PTS":12,"REB":1,"AST":4,"FGM":3,"FGA":9,"FGP":0.333,"m3p":2,"a3p":2,"P3P":1,"FTM":4,"FTA":4,"FTP":1,"BLK":0,"STL":3,"PF":1,"TO":4,"player":"Curry","first":"Stephen"},{"DATE":"3/9/2015","OPP":"@ PHX","OUTCOME":"W","PtF":98,"PA":80,"MIN":36,"PTS":36,"REB":6,"AST":5,"FGM":14,"FGA":24,"FGP":0.583,"m3p":7,"a3p":13,"P3P":0.538,"FTM":1,"FTA":2,"FTP":0.5,"BLK":0,"STL":4,"PF":3,"TO":4,"player":"Curry","first":"Stephen"},{"DATE":"3/11/2015","OPP":"vsDET","OUTCOME":"W","PtF":105,"PA":98,"MIN":34,"PTS":9,"REB":5,"AST":11,"FGM":4,"FGA":15,"FGP":0.267,"m3p":1,"a3p":7,"P3P":0.143,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":1,"PF":1,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"3/13/2015","OPP":"@ DEN","OUTCOME":"L","PtF":103,"PA":114,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Curry","first":"Stephen"},{"DATE":"3/14/2015","OPP":"vsNY","OUTCOME":"W","PtF":125,"PA":94,"MIN":27,"PTS":25,"REB":3,"AST":11,"FGM":8,"FGA":15,"FGP":0.533,"m3p":6,"a3p":10,"P3P":0.6,"FTM":3,"FTA":3,"FTP":1,"BLK":0,"STL":3,"PF":2,"TO":1,"player":"Curry","first":"Stephen"},{"DATE":"3/16/2015","OPP":"vsLAL","OUTCOME":"W","PtF":108,"PA":105,"MIN":33,"PTS":19,"REB":4,"AST":9,"FGM":5,"FGA":14,"FGP":0.357,"m3p":2,"a3p":6,"P3P":0.333,"FTM":7,"FTA":7,"FTP":1,"BLK":0,"STL":3,"PF":2,"TO":6,"player":"Curry","first":"Stephen"},{"DATE":"3/18/2015","OPP":"vsATL","OUTCOME":"W","PtF":114,"PA":95,"MIN":30,"PTS":16,"REB":4,"AST":12,"FGM":4,"FGA":11,"FGP":0.364,"m3p":2,"a3p":6,"P3P":0.333,"FTM":6,"FTA":6,"FTP":1,"BLK":0,"STL":0,"PF":1,"TO":4,"player":"Curry","first":"Stephen"},{"DATE":"3/20/2015","OPP":"vsNO","OUTCOME":"W","PtF":112,"PA":96,"MIN":27,"PTS":16,"REB":2,"AST":11,"FGM":4,"FGA":17,"FGP":0.235,"m3p":2,"a3p":10,"P3P":0.2,"FTM":6,"FTA":6,"FTP":1,"BLK":0,"STL":0,"PF":0,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"3/21/2015","OPP":"vsUTAH","OUTCOME":"W","PtF":106,"PA":91,"MIN":32,"PTS":24,"REB":3,"AST":3,"FGM":8,"FGA":18,"FGP":0.444,"m3p":3,"a3p":6,"P3P":0.5,"FTM":5,"FTA":5,"FTP":1,"BLK":0,"STL":3,"PF":0,"TO":0,"player":"Curry","first":"Stephen"},{"DATE":"3/23/2015","OPP":"vsWSH","OUTCOME":"W","PtF":107,"PA":76,"MIN":28,"PTS":24,"REB":5,"AST":6,"FGM":8,"FGA":14,"FGP":0.571,"m3p":5,"a3p":8,"P3P":0.625,"FTM":3,"FTA":3,"FTP":1,"BLK":0,"STL":2,"PF":1,"TO":2,"player":"Curry","first":"Stephen"},{"DATE":"3/24/2015","OPP":"@ POR","OUTCOME":"W","PtF":122,"PA":108,"MIN":35,"PTS":33,"REB":2,"AST":10,"FGM":13,"FGA":22,"FGP":0.591,"m3p":5,"a3p":9,"P3P":0.556,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":1,"PF":2,"TO":2,"player":"Curry","first":"Stephen"},{"DATE":"3/27/2015","OPP":"@ MEM","OUTCOME":"W","PtF":107,"PA":84,"MIN":34,"PTS":38,"REB":3,"AST":10,"FGM":12,"FGA":22,"FGP":0.545,"m3p":8,"a3p":12,"P3P":0.667,"FTM":6,"FTA":6,"FTP":1,"BLK":1,"STL":3,"PF":3,"TO":4,"player":"Curry","first":"Stephen"},{"DATE":"3/28/2015","OPP":"@ MIL","OUTCOME":"W","PtF":108,"PA":95,"MIN":30,"PTS":25,"REB":4,"AST":6,"FGM":8,"FGA":13,"FGP":0.615,"m3p":6,"a3p":9,"P3P":0.667,"FTM":3,"FTA":3,"FTP":1,"BLK":0,"STL":1,"PF":4,"TO":6,"player":"Curry","first":"Stephen"},{"DATE":"3/31/2015","OPP":"@ LAC","OUTCOME":"W","PtF":110,"PA":106,"MIN":36,"PTS":27,"REB":1,"AST":4,"FGM":8,"FGA":15,"FGP":0.533,"m3p":4,"a3p":6,"P3P":0.667,"FTM":7,"FTA":7,"FTP":1,"BLK":0,"STL":1,"PF":3,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"4/2/2015","OPP":"vsPHX","OUTCOME":"W","PtF":107,"PA":106,"MIN":34,"PTS":28,"REB":8,"AST":5,"FGM":10,"FGA":22,"FGP":0.455,"m3p":6,"a3p":11,"P3P":0.545,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":0,"PF":3,"TO":6,"player":"Curry","first":"Stephen"},{"DATE":"4/4/2015","OPP":"@ DAL","OUTCOME":"W","PtF":123,"PA":110,"MIN":27,"PTS":11,"REB":4,"AST":3,"FGM":4,"FGA":12,"FGP":0.333,"m3p":1,"a3p":4,"P3P":0.25,"FTM":2,"FTA":3,"FTP":0.667,"BLK":0,"STL":1,"PF":1,"TO":1,"player":"Curry","first":"Stephen"},{"DATE":"4/5/2015","OPP":"@ SA","OUTCOME":"L","PtF":92,"PA":107,"MIN":30,"PTS":24,"REB":4,"AST":6,"FGM":9,"FGA":17,"FGP":0.529,"m3p":5,"a3p":10,"P3P":0.5,"FTM":1,"FTA":2,"FTP":0.5,"BLK":0,"STL":1,"PF":2,"TO":4,"player":"Curry","first":"Stephen"},{"DATE":"4/7/2015","OPP":"@ NO","OUTCOME":"L","PtF":100,"PA":103,"MIN":35,"PTS":25,"REB":6,"AST":9,"FGM":9,"FGA":18,"FGP":0.5,"m3p":5,"a3p":8,"P3P":0.625,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":1,"PF":3,"TO":2,"player":"Curry","first":"Stephen"},{"DATE":"4/9/2015","OPP":"vsPOR","OUTCOME":"W","PtF":116,"PA":105,"MIN":35,"PTS":45,"REB":2,"AST":10,"FGM":17,"FGA":23,"FGP":0.739,"m3p":8,"a3p":13,"P3P":0.615,"FTM":3,"FTA":3,"FTP":1,"BLK":0,"STL":0,"PF":2,"TO":4,"player":"Curry","first":"Stephen"},{"DATE":"4/11/2015","OPP":"vsMIN","OUTCOME":"W","PtF":110,"PA":101,"MIN":35,"PTS":34,"REB":4,"AST":7,"FGM":11,"FGA":21,"FGP":0.524,"m3p":5,"a3p":11,"P3P":0.455,"FTM":7,"FTA":8,"FTP":0.875,"BLK":0,"STL":4,"PF":1,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"4/13/2015","OPP":"vsMEM","OUTCOME":"W","PtF":111,"PA":107,"MIN":29,"PTS":15,"REB":4,"AST":8,"FGM":6,"FGA":10,"FGP":0.6,"m3p":3,"a3p":6,"P3P":0.5,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":1,"PF":2,"TO":3,"player":"Curry","first":"Stephen"},{"DATE":"4/13/2015","OPP":"vsDEN","OUTCOME":"W","PtF":133,"PA":126,"MIN":19,"PTS":10,"REB":4,"AST":7,"FGM":4,"FGA":10,"FGP":0.4,"m3p":2,"a3p":4,"P3P":0.5,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":5,"PF":1,"TO":2,"player":"Curry","first":"Stephen"},{"DATE":"10/28/2014","OPP":"@ LAL","OUTCOME":"W","PtF":108,"PA":90,"MIN":31,"PTS":32,"REB":1,"AST":6,"FGM":7,"FGA":17,"FGP":0.412,"m3p":3,"a3p":6,"P3P":0.5,"FTM":15,"FTA":16,"FTP":0.938,"BLK":0,"STL":1,"PF":1,"TO":0,"player":"Harden","first":"James"},{"DATE":"10/29/2014","OPP":"@ UTAH","OUTCOME":"W","PtF":104,"PA":93,"MIN":38,"PTS":18,"REB":7,"AST":10,"FGM":6,"FGA":18,"FGP":0.333,"m3p":1,"a3p":5,"P3P":0.2,"FTM":5,"FTA":6,"FTP":0.833,"BLK":1,"STL":0,"PF":5,"TO":6,"player":"Harden","first":"James"},{"DATE":"11/1/2014","OPP":"vsBOS","OUTCOME":"W","PtF":104,"PA":90,"MIN":31,"PTS":26,"REB":8,"AST":6,"FGM":5,"FGA":14,"FGP":0.357,"m3p":2,"a3p":6,"P3P":0.333,"FTM":14,"FTA":14,"FTP":1,"BLK":1,"STL":4,"PF":1,"TO":3,"player":"Harden","first":"James"},{"DATE":"11/3/2014","OPP":"@ PHI","OUTCOME":"W","PtF":104,"PA":93,"MIN":38,"PTS":35,"REB":9,"AST":5,"FGM":7,"FGA":18,"FGP":0.389,"m3p":4,"a3p":8,"P3P":0.5,"FTM":17,"FTA":18,"FTP":0.944,"BLK":1,"STL":1,"PF":2,"TO":4,"player":"Harden","first":"James"},{"DATE":"11/4/2014","OPP":"@ MIA","OUTCOME":"W","PtF":108,"PA":91,"MIN":40,"PTS":25,"REB":9,"AST":10,"FGM":6,"FGA":13,"FGP":0.462,"m3p":3,"a3p":7,"P3P":0.429,"FTM":10,"FTA":10,"FTP":1,"BLK":2,"STL":1,"PF":2,"TO":5,"player":"Harden","first":"James"},{"DATE":"11/6/2014","OPP":"vsSA","OUTCOME":"W","PtF":98,"PA":81,"MIN":33,"PTS":20,"REB":6,"AST":6,"FGM":8,"FGA":15,"FGP":0.533,"m3p":1,"a3p":4,"P3P":0.25,"FTM":3,"FTA":6,"FTP":0.5,"BLK":3,"STL":1,"PF":1,"TO":8,"player":"Harden","first":"James"},{"DATE":"11/8/2014","OPP":"vsGS","OUTCOME":"L","PtF":87,"PA":98,"MIN":43,"PTS":22,"REB":7,"AST":7,"FGM":8,"FGA":24,"FGP":0.333,"m3p":1,"a3p":11,"P3P":0.091,"FTM":5,"FTA":6,"FTP":0.833,"BLK":0,"STL":2,"PF":0,"TO":5,"player":"Harden","first":"James"},{"DATE":"11/12/2014","OPP":"@ MIN","OUTCOME":"W","PtF":113,"PA":101,"MIN":40,"PTS":23,"REB":4,"AST":10,"FGM":8,"FGA":23,"FGP":0.348,"m3p":2,"a3p":8,"P3P":0.25,"FTM":5,"FTA":5,"FTP":1,"BLK":0,"STL":3,"PF":3,"TO":1,"player":"Harden","first":"James"},{"DATE":"11/14/2014","OPP":"vsPHI","OUTCOME":"W","PtF":88,"PA":87,"MIN":41,"PTS":35,"REB":6,"AST":7,"FGM":10,"FGA":24,"FGP":0.417,"m3p":1,"a3p":5,"P3P":0.2,"FTM":14,"FTA":16,"FTP":0.875,"BLK":1,"STL":3,"PF":5,"TO":5,"player":"Harden","first":"James"},{"DATE":"11/16/2014","OPP":"@ OKC","OUTCOME":"W","PtF":69,"PA":65,"MIN":37,"PTS":19,"REB":9,"AST":5,"FGM":5,"FGA":17,"FGP":0.294,"m3p":2,"a3p":9,"P3P":0.222,"FTM":7,"FTA":8,"FTP":0.875,"BLK":1,"STL":1,"PF":3,"TO":4,"player":"Harden","first":"James"},{"DATE":"11/17/2014","OPP":"@ MEM","OUTCOME":"L","PtF":93,"PA":119,"MIN":34,"PTS":6,"REB":5,"AST":4,"FGM":1,"FGA":8,"FGP":0.125,"m3p":0,"a3p":4,"P3P":0,"FTM":4,"FTA":5,"FTP":0.8,"BLK":1,"STL":2,"PF":4,"TO":3,"player":"Harden","first":"James"},{"DATE":"11/19/2014","OPP":"vsLAL","OUTCOME":"L","PtF":92,"PA":98,"MIN":38,"PTS":24,"REB":4,"AST":7,"FGM":7,"FGA":11,"FGP":0.636,"m3p":2,"a3p":3,"P3P":0.667,"FTM":8,"FTA":10,"FTP":0.8,"BLK":1,"STL":1,"PF":4,"TO":6,"player":"Harden","first":"James"},{"DATE":"11/22/2014","OPP":"vsDAL","OUTCOME":"W","PtF":95,"PA":92,"MIN":37,"PTS":32,"REB":8,"AST":4,"FGM":8,"FGA":18,"FGP":0.444,"m3p":4,"a3p":9,"P3P":0.444,"FTM":12,"FTA":13,"FTP":0.923,"BLK":0,"STL":2,"PF":3,"TO":4,"player":"Harden","first":"James"},{"DATE":"11/24/2014","OPP":"vsNY","OUTCOME":"W","PtF":91,"PA":86,"MIN":40,"PTS":36,"REB":6,"AST":6,"FGM":11,"FGA":22,"FGP":0.5,"m3p":7,"a3p":12,"P3P":0.583,"FTM":7,"FTA":7,"FTP":1,"BLK":1,"STL":3,"PF":2,"TO":5,"player":"Harden","first":"James"},{"DATE":"11/26/2014","OPP":"vsSAC","OUTCOME":"W","PtF":102,"PA":89,"MIN":37,"PTS":26,"REB":7,"AST":8,"FGM":10,"FGA":31,"FGP":0.323,"m3p":2,"a3p":9,"P3P":0.222,"FTM":4,"FTA":6,"FTP":0.667,"BLK":3,"STL":2,"PF":3,"TO":1,"player":"Harden","first":"James"},{"DATE":"11/28/2014","OPP":"vsLAC","OUTCOME":"L","PtF":85,"PA":102,"MIN":38,"PTS":16,"REB":4,"AST":6,"FGM":5,"FGA":12,"FGP":0.417,"m3p":0,"a3p":4,"P3P":0,"FTM":6,"FTA":6,"FTP":1,"BLK":2,"STL":2,"PF":3,"TO":5,"player":"Harden","first":"James"},{"DATE":"11/29/2014","OPP":"@ MIL","OUTCOME":"W","PtF":117,"PA":103,"MIN":38,"PTS":34,"REB":6,"AST":7,"FGM":9,"FGA":14,"FGP":0.643,"m3p":4,"a3p":5,"P3P":0.8,"FTM":12,"FTA":13,"FTP":0.923,"BLK":1,"STL":4,"PF":4,"TO":5,"player":"Harden","first":"James"},{"DATE":"12/3/2014","OPP":"vsMEM","OUTCOME":"W","PtF":105,"PA":96,"MIN":28,"PTS":21,"REB":4,"AST":4,"FGM":6,"FGA":11,"FGP":0.545,"m3p":2,"a3p":5,"P3P":0.4,"FTM":7,"FTA":8,"FTP":0.875,"BLK":1,"STL":3,"PF":3,"TO":3,"player":"Harden","first":"James"},{"DATE":"12/5/2014","OPP":"@ MIN","OUTCOME":"W","PtF":114,"PA":112,"MIN":37,"PTS":38,"REB":4,"AST":6,"FGM":11,"FGA":17,"FGP":0.647,"m3p":3,"a3p":5,"P3P":0.6,"FTM":13,"FTA":14,"FTP":0.929,"BLK":0,"STL":0,"PF":6,"TO":8,"player":"Harden","first":"James"},{"DATE":"12/6/2014","OPP":"vsPHX","OUTCOME":"W","PtF":100,"PA":95,"MIN":39,"PTS":15,"REB":12,"AST":7,"FGM":5,"FGA":21,"FGP":0.238,"m3p":1,"a3p":9,"P3P":0.111,"FTM":4,"FTA":5,"FTP":0.8,"BLK":3,"STL":2,"PF":1,"TO":6,"player":"Harden","first":"James"},{"DATE":"12/10/2014","OPP":"@ GS","OUTCOME":"L","PtF":93,"PA":105,"MIN":40,"PTS":34,"REB":7,"AST":4,"FGM":14,"FGA":27,"FGP":0.519,"m3p":3,"a3p":8,"P3P":0.375,"FTM":3,"FTA":4,"FTP":0.75,"BLK":0,"STL":4,"PF":3,"TO":5,"player":"Harden","first":"James"},{"DATE":"12/11/2014","OPP":"@ SAC","OUTCOME":"W","PtF":113,"PA":109,"MIN":48,"PTS":44,"REB":4,"AST":8,"FGM":14,"FGA":32,"FGP":0.438,"m3p":3,"a3p":10,"P3P":0.3,"FTM":13,"FTA":15,"FTP":0.867,"BLK":1,"STL":3,"PF":2,"TO":2,"player":"Harden","first":"James"},{"DATE":"12/13/2014","OPP":"vsDEN","OUTCOME":"W","PtF":108,"PA":96,"MIN":38,"PTS":24,"REB":10,"AST":10,"FGM":7,"FGA":20,"FGP":0.35,"m3p":2,"a3p":5,"P3P":0.4,"FTM":8,"FTA":9,"FTP":0.889,"BLK":2,"STL":1,"PF":3,"TO":3,"player":"Harden","first":"James"},{"DATE":"12/17/2014","OPP":"@ DEN","OUTCOME":"W","PtF":115,"PA":111,"MIN":45,"PTS":41,"REB":4,"AST":10,"FGM":11,"FGA":21,"FGP":0.524,"m3p":1,"a3p":7,"P3P":0.143,"FTM":18,"FTA":21,"FTP":0.857,"BLK":0,"STL":2,"PF":2,"TO":4,"player":"Harden","first":"James"},{"DATE":"12/18/2014","OPP":"vsNO","OUTCOME":"L","PtF":90,"PA":99,"MIN":34,"PTS":21,"REB":7,"AST":5,"FGM":8,"FGA":23,"FGP":0.348,"m3p":2,"a3p":7,"P3P":0.286,"FTM":3,"FTA":5,"FTP":0.6,"BLK":0,"STL":0,"PF":4,"TO":4,"player":"Harden","first":"James"},{"DATE":"12/20/2014","OPP":"vsATL","OUTCOME":"L","PtF":97,"PA":104,"MIN":39,"PTS":18,"REB":6,"AST":14,"FGM":5,"FGA":16,"FGP":0.313,"m3p":2,"a3p":7,"P3P":0.286,"FTM":6,"FTA":6,"FTP":1,"BLK":1,"STL":1,"PF":5,"TO":3,"player":"Harden","first":"James"},{"DATE":"12/22/2014","OPP":"vsPOR","OUTCOME":"W","PtF":110,"PA":95,"MIN":32,"PTS":44,"REB":3,"AST":7,"FGM":14,"FGA":26,"FGP":0.538,"m3p":4,"a3p":9,"P3P":0.444,"FTM":12,"FTA":13,"FTP":0.923,"BLK":0,"STL":5,"PF":2,"TO":1,"player":"Harden","first":"James"},{"DATE":"12/26/2014","OPP":"@ MEM","OUTCOME":"W","PtF":117,"PA":111,"MIN":41,"PTS":32,"REB":8,"AST":10,"FGM":10,"FGA":17,"FGP":0.588,"m3p":2,"a3p":4,"P3P":0.5,"FTM":10,"FTA":11,"FTP":0.909,"BLK":3,"STL":0,"PF":1,"TO":2,"player":"Harden","first":"James"},{"DATE":"12/28/2014","OPP":"@ SA","OUTCOME":"L","PtF":106,"PA":110,"MIN":36,"PTS":28,"REB":2,"AST":5,"FGM":10,"FGA":18,"FGP":0.556,"m3p":3,"a3p":6,"P3P":0.5,"FTM":5,"FTA":6,"FTP":0.833,"BLK":0,"STL":2,"PF":5,"TO":9,"player":"Harden","first":"James"},{"DATE":"12/29/2014","OPP":"vsWSH","OUTCOME":"L","PtF":103,"PA":104,"MIN":36,"PTS":33,"REB":6,"AST":4,"FGM":12,"FGA":23,"FGP":0.522,"m3p":3,"a3p":8,"P3P":0.375,"FTM":6,"FTA":7,"FTP":0.857,"BLK":0,"STL":1,"PF":3,"TO":2,"player":"Harden","first":"James"},{"DATE":"12/31/2014","OPP":"vsCHA","OUTCOME":"W","PtF":102,"PA":83,"MIN":33,"PTS":36,"REB":7,"AST":6,"FGM":12,"FGA":19,"FGP":0.632,"m3p":8,"a3p":11,"P3P":0.727,"FTM":4,"FTA":4,"FTP":1,"BLK":1,"STL":2,"PF":1,"TO":1,"player":"Harden","first":"James"},{"DATE":"1/2/2015","OPP":"@ NO","OUTCOME":"L","PtF":83,"PA":111,"MIN":25,"PTS":11,"REB":2,"AST":3,"FGM":5,"FGA":13,"FGP":0.385,"m3p":1,"a3p":6,"P3P":0.167,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":2,"PF":3,"TO":5,"player":"Harden","first":"James"},{"DATE":"1/3/2015","OPP":"vsMIA","OUTCOME":"W","PtF":115,"PA":79,"MIN":28,"PTS":28,"REB":5,"AST":2,"FGM":9,"FGA":17,"FGP":0.529,"m3p":3,"a3p":6,"P3P":0.5,"FTM":7,"FTA":7,"FTP":1,"BLK":0,"STL":1,"PF":1,"TO":3,"player":"Harden","first":"James"},{"DATE":"1/5/2015","OPP":"@ CHI","OUTCOME":"L","PtF":105,"PA":114,"MIN":38,"PTS":20,"REB":2,"AST":4,"FGM":7,"FGA":22,"FGP":0.318,"m3p":3,"a3p":8,"P3P":0.375,"FTM":3,"FTA":3,"FTP":1,"BLK":2,"STL":1,"PF":4,"TO":2,"player":"Harden","first":"James"},{"DATE":"1/7/2015","OPP":"@ CLE","OUTCOME":"W","PtF":105,"PA":93,"MIN":33,"PTS":21,"REB":7,"AST":9,"FGM":6,"FGA":18,"FGP":0.333,"m3p":2,"a3p":7,"P3P":0.286,"FTM":7,"FTA":7,"FTP":1,"BLK":0,"STL":1,"PF":0,"TO":7,"player":"Harden","first":"James"},{"DATE":"1/8/2015","OPP":"@ NY","OUTCOME":"W","PtF":120,"PA":96,"MIN":31,"PTS":25,"REB":4,"AST":9,"FGM":8,"FGA":17,"FGP":0.471,"m3p":3,"a3p":7,"P3P":0.429,"FTM":6,"FTA":6,"FTP":1,"BLK":0,"STL":3,"PF":2,"TO":3,"player":"Harden","first":"James"},{"DATE":"1/10/2015","OPP":"vsUTAH","OUTCOME":"W","PtF":97,"PA":82,"MIN":34,"PTS":30,"REB":3,"AST":5,"FGM":8,"FGA":14,"FGP":0.571,"m3p":4,"a3p":8,"P3P":0.5,"FTM":10,"FTA":11,"FTP":0.909,"BLK":1,"STL":1,"PF":3,"TO":6,"player":"Harden","first":"James"},{"DATE":"1/12/2015","OPP":"@ BKN","OUTCOME":"W","PtF":113,"PA":99,"MIN":30,"PTS":30,"REB":3,"AST":6,"FGM":9,"FGA":13,"FGP":0.692,"m3p":4,"a3p":7,"P3P":0.571,"FTM":8,"FTA":8,"FTP":1,"BLK":0,"STL":2,"PF":1,"TO":2,"player":"Harden","first":"James"},{"DATE":"1/14/2015","OPP":"@ ORL","OUTCOME":"L","PtF":113,"PA":120,"MIN":38,"PTS":26,"REB":4,"AST":10,"FGM":10,"FGA":16,"FGP":0.625,"m3p":1,"a3p":5,"P3P":0.2,"FTM":5,"FTA":6,"FTP":0.833,"BLK":1,"STL":5,"PF":4,"TO":7,"player":"Harden","first":"James"},{"DATE":"1/15/2015","OPP":"vsOKC","OUTCOME":"W","PtF":112,"PA":101,"MIN":37,"PTS":31,"REB":9,"AST":10,"FGM":9,"FGA":15,"FGP":0.6,"m3p":6,"a3p":8,"P3P":0.75,"FTM":7,"FTA":9,"FTP":0.778,"BLK":0,"STL":1,"PF":3,"TO":2,"player":"Harden","first":"James"},{"DATE":"1/17/2015","OPP":"vsGS","OUTCOME":"L","PtF":106,"PA":131,"MIN":30,"PTS":12,"REB":4,"AST":4,"FGM":4,"FGA":15,"FGP":0.267,"m3p":0,"a3p":4,"P3P":0,"FTM":4,"FTA":7,"FTP":0.571,"BLK":0,"STL":1,"PF":5,"TO":3,"player":"Harden","first":"James"},{"DATE":"1/19/2015","OPP":"vsIND","OUTCOME":"W","PtF":110,"PA":98,"MIN":40,"PTS":45,"REB":1,"AST":7,"FGM":12,"FGA":18,"FGP":0.667,"m3p":7,"a3p":12,"P3P":0.583,"FTM":14,"FTA":15,"FTP":0.933,"BLK":1,"STL":4,"PF":0,"TO":5,"player":"Harden","first":"James"},{"DATE":"1/21/2015","OPP":"@ GS","OUTCOME":"L","PtF":113,"PA":126,"MIN":32,"PTS":33,"REB":4,"AST":6,"FGM":8,"FGA":18,"FGP":0.444,"m3p":3,"a3p":6,"P3P":0.5,"FTM":14,"FTA":16,"FTP":0.875,"BLK":1,"STL":2,"PF":2,"TO":2,"player":"Harden","first":"James"},{"DATE":"1/23/2015","OPP":"@ PHX","OUTCOME":"W","PtF":113,"PA":111,"MIN":40,"PTS":33,"REB":6,"AST":10,"FGM":8,"FGA":18,"FGP":0.444,"m3p":3,"a3p":6,"P3P":0.5,"FTM":14,"FTA":17,"FTP":0.824,"BLK":1,"STL":3,"PF":4,"TO":3,"player":"Harden","first":"James"},{"DATE":"1/25/2015","OPP":"@ LAL","OUTCOME":"W","PtF":99,"PA":87,"MIN":35,"PTS":37,"REB":8,"AST":5,"FGM":12,"FGA":20,"FGP":0.6,"m3p":4,"a3p":11,"P3P":0.364,"FTM":9,"FTA":10,"FTP":0.9,"BLK":0,"STL":2,"PF":0,"TO":6,"player":"Harden","first":"James"},{"DATE":"1/28/2015","OPP":"vsDAL","OUTCOME":"W","PtF":99,"PA":94,"MIN":38,"PTS":17,"REB":5,"AST":8,"FGM":7,"FGA":14,"FGP":0.5,"m3p":1,"a3p":3,"P3P":0.333,"FTM":2,"FTA":3,"FTP":0.667,"BLK":0,"STL":4,"PF":5,"TO":4,"player":"Harden","first":"James"},{"DATE":"1/30/2015","OPP":"@ BOS","OUTCOME":"W","PtF":93,"PA":87,"MIN":39,"PTS":14,"REB":6,"AST":7,"FGM":4,"FGA":21,"FGP":0.19,"m3p":2,"a3p":7,"P3P":0.286,"FTM":4,"FTA":5,"FTP":0.8,"BLK":0,"STL":2,"PF":2,"TO":5,"player":"Harden","first":"James"},{"DATE":"1/31/2015","OPP":"@ DET","OUTCOME":"L","PtF":101,"PA":114,"MIN":36,"PTS":26,"REB":7,"AST":9,"FGM":10,"FGA":17,"FGP":0.588,"m3p":2,"a3p":4,"P3P":0.5,"FTM":4,"FTA":5,"FTP":0.8,"BLK":1,"STL":2,"PF":1,"TO":4,"player":"Harden","first":"James"},{"DATE":"2/4/2015","OPP":"vsCHI","OUTCOME":"W","PtF":101,"PA":90,"MIN":36,"PTS":27,"REB":3,"AST":4,"FGM":9,"FGA":20,"FGP":0.45,"m3p":3,"a3p":5,"P3P":0.6,"FTM":6,"FTA":6,"FTP":1,"BLK":2,"STL":1,"PF":2,"TO":2,"player":"Harden","first":"James"},{"DATE":"2/6/2015","OPP":"vsMIL","OUTCOME":"W","PtF":117,"PA":111,"MIN":40,"PTS":33,"REB":3,"AST":5,"FGM":9,"FGA":14,"FGP":0.643,"m3p":3,"a3p":6,"P3P":0.5,"FTM":12,"FTA":17,"FTP":0.706,"BLK":0,"STL":1,"PF":1,"TO":3,"player":"Harden","first":"James"},{"DATE":"2/8/2015","OPP":"vsPOR","OUTCOME":"L","PtF":98,"PA":109,"MIN":40,"PTS":45,"REB":9,"AST":8,"FGM":11,"FGA":25,"FGP":0.44,"m3p":4,"a3p":10,"P3P":0.4,"FTM":19,"FTA":23,"FTP":0.826,"BLK":0,"STL":2,"PF":2,"TO":4,"player":"Harden","first":"James"},{"DATE":"2/10/2015","OPP":"@ PHX","OUTCOME":"W","PtF":127,"PA":118,"MIN":43,"PTS":40,"REB":12,"AST":9,"FGM":13,"FGA":23,"FGP":0.565,"m3p":3,"a3p":7,"P3P":0.429,"FTM":11,"FTA":14,"FTP":0.786,"BLK":1,"STL":3,"PF":1,"TO":5,"player":"Harden","first":"James"},{"DATE":"2/11/2015","OPP":"@ LAC","OUTCOME":"L","PtF":95,"PA":110,"MIN":36,"PTS":9,"REB":5,"AST":6,"FGM":3,"FGA":12,"FGP":0.25,"m3p":0,"a3p":7,"P3P":0,"FTM":3,"FTA":5,"FTP":0.6,"BLK":0,"STL":3,"PF":4,"TO":5,"player":"Harden","first":"James"},{"DATE":"2/20/2015","OPP":"@ DAL","OUTCOME":"L","PtF":100,"PA":111,"MIN":37,"PTS":26,"REB":7,"AST":5,"FGM":6,"FGA":14,"FGP":0.429,"m3p":3,"a3p":5,"P3P":0.6,"FTM":11,"FTA":13,"FTP":0.846,"BLK":0,"STL":2,"PF":5,"TO":6,"player":"Harden","first":"James"},{"DATE":"2/21/2015","OPP":"vsTOR","OUTCOME":"W","PtF":98,"PA":76,"MIN":29,"PTS":20,"REB":5,"AST":7,"FGM":5,"FGA":12,"FGP":0.417,"m3p":2,"a3p":6,"P3P":0.333,"FTM":8,"FTA":10,"FTP":0.8,"BLK":0,"STL":2,"PF":2,"TO":5,"player":"Harden","first":"James"},{"DATE":"2/23/2015","OPP":"vsMIN","OUTCOME":"W","PtF":113,"PA":102,"MIN":39,"PTS":31,"REB":11,"AST":10,"FGM":7,"FGA":20,"FGP":0.35,"m3p":5,"a3p":11,"P3P":0.455,"FTM":12,"FTA":15,"FTP":0.8,"BLK":4,"STL":1,"PF":3,"TO":3,"player":"Harden","first":"James"},{"DATE":"2/25/2015","OPP":"vsLAC","OUTCOME":"W","PtF":110,"PA":105,"MIN":36,"PTS":21,"REB":4,"AST":10,"FGM":4,"FGA":13,"FGP":0.308,"m3p":3,"a3p":9,"P3P":0.333,"FTM":10,"FTA":12,"FTP":0.833,"BLK":0,"STL":0,"PF":1,"TO":3,"player":"Harden","first":"James"},{"DATE":"2/27/2015","OPP":"vsBKN","OUTCOME":"W","PtF":102,"PA":98,"MIN":38,"PTS":15,"REB":5,"AST":12,"FGM":4,"FGA":15,"FGP":0.267,"m3p":0,"a3p":4,"P3P":0,"FTM":7,"FTA":7,"FTP":1,"BLK":0,"STL":1,"PF":3,"TO":4,"player":"Harden","first":"James"},{"DATE":"3/1/2015","OPP":"vsCLE","OUTCOME":"W","PtF":105,"PA":103,"MIN":42,"PTS":33,"REB":8,"AST":5,"FGM":8,"FGA":18,"FGP":0.444,"m3p":2,"a3p":6,"P3P":0.333,"FTM":15,"FTA":18,"FTP":0.833,"BLK":2,"STL":3,"PF":3,"TO":5,"player":"Harden","first":"James"},{"DATE":"3/3/2015","OPP":"@ ATL","OUTCOME":"L","PtF":96,"PA":104,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Harden","first":"James"},{"DATE":"3/4/2015","OPP":"vsMEM","OUTCOME":"L","PtF":100,"PA":102,"MIN":41,"PTS":18,"REB":4,"AST":13,"FGM":7,"FGA":16,"FGP":0.438,"m3p":2,"a3p":7,"P3P":0.286,"FTM":2,"FTA":2,"FTP":1,"BLK":2,"STL":1,"PF":1,"TO":3,"player":"Harden","first":"James"},{"DATE":"3/6/2015","OPP":"vsDET","OUTCOME":"W","PtF":103,"PA":93,"MIN":38,"PTS":38,"REB":12,"AST":12,"FGM":10,"FGA":21,"FGP":0.476,"m3p":2,"a3p":6,"P3P":0.333,"FTM":16,"FTA":18,"FTP":0.889,"BLK":0,"STL":1,"PF":1,"TO":7,"player":"Harden","first":"James"},{"DATE":"3/7/2015","OPP":"@ DEN","OUTCOME":"W","PtF":114,"PA":100,"MIN":36,"PTS":28,"REB":4,"AST":7,"FGM":8,"FGA":19,"FGP":0.421,"m3p":2,"a3p":4,"P3P":0.5,"FTM":10,"FTA":11,"FTP":0.909,"BLK":0,"STL":2,"PF":5,"TO":4,"player":"Harden","first":"James"},{"DATE":"3/11/2015","OPP":"@ POR","OUTCOME":"L","PtF":100,"PA":105,"MIN":38,"PTS":18,"REB":2,"AST":6,"FGM":7,"FGA":19,"FGP":0.368,"m3p":0,"a3p":3,"P3P":0,"FTM":4,"FTA":5,"FTP":0.8,"BLK":0,"STL":3,"PF":3,"TO":4,"player":"Harden","first":"James"},{"DATE":"3/12/2015","OPP":"@ UTAH","OUTCOME":"L","PtF":91,"PA":109,"MIN":34,"PTS":15,"REB":4,"AST":7,"FGM":3,"FGA":13,"FGP":0.231,"m3p":0,"a3p":5,"P3P":0,"FTM":9,"FTA":13,"FTP":0.692,"BLK":0,"STL":3,"PF":1,"TO":3,"player":"Harden","first":"James"},{"DATE":"3/15/2015","OPP":"@ LAC","OUTCOME":"W","PtF":100,"PA":98,"MIN":41,"PTS":34,"REB":7,"AST":7,"FGM":7,"FGA":16,"FGP":0.438,"m3p":3,"a3p":5,"P3P":0.6,"FTM":17,"FTA":18,"FTP":0.944,"BLK":0,"STL":0,"PF":3,"TO":3,"player":"Harden","first":"James"},{"DATE":"3/17/2015","OPP":"vsORL","OUTCOME":"W","PtF":107,"PA":94,"MIN":35,"PTS":17,"REB":8,"AST":4,"FGM":4,"FGA":14,"FGP":0.286,"m3p":1,"a3p":4,"P3P":0.25,"FTM":8,"FTA":11,"FTP":0.727,"BLK":0,"STL":3,"PF":1,"TO":4,"player":"Harden","first":"James"},{"DATE":"3/19/2015","OPP":"vsDEN","OUTCOME":"W","PtF":118,"PA":108,"MIN":40,"PTS":50,"REB":10,"AST":4,"FGM":12,"FGA":27,"FGP":0.444,"m3p":4,"a3p":12,"P3P":0.333,"FTM":22,"FTA":25,"FTP":0.88,"BLK":0,"STL":1,"PF":2,"TO":4,"player":"Harden","first":"James"},{"DATE":"3/21/2015","OPP":"vsPHX","OUTCOME":"L","PtF":102,"PA":117,"MIN":33,"PTS":16,"REB":2,"AST":5,"FGM":5,"FGA":19,"FGP":0.263,"m3p":1,"a3p":8,"P3P":0.125,"FTM":5,"FTA":5,"FTP":1,"BLK":2,"STL":0,"PF":3,"TO":5,"player":"Harden","first":"James"},{"DATE":"3/23/2015","OPP":"@ IND","OUTCOME":"W","PtF":110,"PA":100,"MIN":43,"PTS":44,"REB":4,"AST":7,"FGM":10,"FGA":21,"FGP":0.476,"m3p":3,"a3p":8,"P3P":0.375,"FTM":21,"FTA":22,"FTP":0.955,"BLK":2,"STL":2,"PF":4,"TO":4,"player":"Harden","first":"James"},{"DATE":"3/25/2015","OPP":"@ NO","OUTCOME":"W","PtF":95,"PA":93,"MIN":37,"PTS":25,"REB":6,"AST":10,"FGM":9,"FGA":20,"FGP":0.45,"m3p":1,"a3p":4,"P3P":0.25,"FTM":6,"FTA":9,"FTP":0.667,"BLK":1,"STL":1,"PF":3,"TO":2,"player":"Harden","first":"James"},{"DATE":"3/27/2015","OPP":"vsMIN","OUTCOME":"W","PtF":120,"PA":110,"MIN":37,"PTS":33,"REB":4,"AST":8,"FGM":9,"FGA":19,"FGP":0.474,"m3p":6,"a3p":12,"P3P":0.5,"FTM":9,"FTA":10,"FTP":0.9,"BLK":0,"STL":5,"PF":4,"TO":3,"player":"Harden","first":"James"},{"DATE":"3/29/2015","OPP":"@ WSH","OUTCOME":"W","PtF":99,"PA":91,"MIN":38,"PTS":24,"REB":3,"AST":6,"FGM":7,"FGA":20,"FGP":0.35,"m3p":1,"a3p":6,"P3P":0.167,"FTM":9,"FTA":10,"FTP":0.9,"BLK":1,"STL":2,"PF":3,"TO":3,"player":"Harden","first":"James"},{"DATE":"3/30/2015","OPP":"@ TOR","OUTCOME":"L","PtF":96,"PA":99,"MIN":38,"PTS":31,"REB":5,"AST":5,"FGM":9,"FGA":22,"FGP":0.409,"m3p":3,"a3p":8,"P3P":0.375,"FTM":10,"FTA":12,"FTP":0.833,"BLK":0,"STL":1,"PF":3,"TO":3,"player":"Harden","first":"James"},{"DATE":"4/1/2015","OPP":"vsSAC","OUTCOME":"W","PtF":115,"PA":111,"MIN":38,"PTS":51,"REB":8,"AST":6,"FGM":16,"FGA":25,"FGP":0.64,"m3p":8,"a3p":9,"P3P":0.889,"FTM":11,"FTA":13,"FTP":0.846,"BLK":0,"STL":3,"PF":1,"TO":7,"player":"Harden","first":"James"},{"DATE":"4/2/2015","OPP":"@ DAL","OUTCOME":"W","PtF":108,"PA":101,"MIN":39,"PTS":24,"REB":4,"AST":6,"FGM":6,"FGA":15,"FGP":0.4,"m3p":2,"a3p":8,"P3P":0.25,"FTM":10,"FTA":12,"FTP":0.833,"BLK":0,"STL":1,"PF":2,"TO":5,"player":"Harden","first":"James"},{"DATE":"4/5/2015","OPP":"@ OKC","OUTCOME":"W","PtF":115,"PA":112,"MIN":38,"PTS":41,"REB":6,"AST":6,"FGM":12,"FGA":22,"FGP":0.545,"m3p":6,"a3p":9,"P3P":0.667,"FTM":11,"FTA":13,"FTP":0.846,"BLK":0,"STL":3,"PF":6,"TO":3,"player":"Harden","first":"James"},{"DATE":"4/8/2015","OPP":"@ SA","OUTCOME":"L","PtF":98,"PA":110,"MIN":35,"PTS":22,"REB":4,"AST":4,"FGM":6,"FGA":15,"FGP":0.4,"m3p":2,"a3p":6,"P3P":0.333,"FTM":8,"FTA":8,"FTP":1,"BLK":0,"STL":1,"PF":1,"TO":1,"player":"Harden","first":"James"},{"DATE":"4/10/2015","OPP":"vsSA","OUTCOME":"L","PtF":103,"PA":104,"MIN":38,"PTS":16,"REB":2,"AST":10,"FGM":5,"FGA":19,"FGP":0.263,"m3p":2,"a3p":5,"P3P":0.4,"FTM":4,"FTA":6,"FTP":0.667,"BLK":0,"STL":3,"PF":2,"TO":4,"player":"Harden","first":"James"},{"DATE":"4/12/2015","OPP":"vsNO","OUTCOME":"W","PtF":121,"PA":114,"MIN":38,"PTS":30,"REB":1,"AST":7,"FGM":10,"FGA":18,"FGP":0.556,"m3p":1,"a3p":6,"P3P":0.167,"FTM":9,"FTA":9,"FTP":1,"BLK":1,"STL":1,"PF":2,"TO":4,"player":"Harden","first":"James"},{"DATE":"4/13/2015","OPP":"@ CHA","OUTCOME":"W","PtF":100,"PA":90,"MIN":37,"PTS":29,"REB":6,"AST":6,"FGM":7,"FGA":20,"FGP":0.35,"m3p":1,"a3p":8,"P3P":0.125,"FTM":14,"FTA":16,"FTP":0.875,"BLK":2,"STL":1,"PF":1,"TO":2,"player":"Harden","first":"James"},{"DATE":"4/15/2015","OPP":"vsUTAH","OUTCOME":"W","PtF":117,"PA":91,"MIN":27,"PTS":16,"REB":11,"AST":10,"FGM":3,"FGA":8,"FGP":0.375,"m3p":2,"a3p":5,"P3P":0.4,"FTM":8,"FTA":8,"FTP":1,"BLK":1,"STL":1,"PF":2,"TO":6,"player":"Harden","first":"James"},{"DATE":"10/30/2014","OPP":"vsNY","OUTCOME":"L","PtF":90,"PA":95,"MIN":43,"PTS":17,"REB":5,"AST":4,"FGM":5,"FGA":15,"FGP":0.333,"m3p":1,"a3p":5,"P3P":0.2,"FTM":6,"FTA":7,"FTP":0.857,"BLK":0,"STL":0,"PF":3,"TO":8,"player":"James","first":"Lebron"},{"DATE":"10/31/2014","OPP":"@ CHI","OUTCOME":"W","PtF":114,"PA":108,"MIN":42,"PTS":36,"REB":8,"AST":5,"FGM":14,"FGA":30,"FGP":0.467,"m3p":1,"a3p":3,"P3P":0.333,"FTM":7,"FTA":9,"FTP":0.778,"BLK":1,"STL":4,"PF":1,"TO":3,"player":"James","first":"Lebron"},{"DATE":"11/4/2014","OPP":"@ POR","OUTCOME":"L","PtF":82,"PA":101,"MIN":35,"PTS":11,"REB":7,"AST":7,"FGM":4,"FGA":12,"FGP":0.333,"m3p":2,"a3p":4,"P3P":0.5,"FTM":1,"FTA":1,"FTP":1,"BLK":0,"STL":1,"PF":2,"TO":3,"player":"James","first":"Lebron"},{"DATE":"11/5/2014","OPP":"@ UTAH","OUTCOME":"L","PtF":100,"PA":102,"MIN":42,"PTS":31,"REB":3,"AST":4,"FGM":8,"FGA":18,"FGP":0.444,"m3p":3,"a3p":5,"P3P":0.6,"FTM":12,"FTA":12,"FTP":1,"BLK":0,"STL":3,"PF":2,"TO":4,"player":"James","first":"Lebron"},{"DATE":"11/7/2014","OPP":"@ DEN","OUTCOME":"W","PtF":110,"PA":101,"MIN":40,"PTS":22,"REB":7,"AST":11,"FGM":8,"FGA":18,"FGP":0.444,"m3p":1,"a3p":5,"P3P":0.2,"FTM":5,"FTA":6,"FTP":0.833,"BLK":1,"STL":1,"PF":1,"TO":3,"player":"James","first":"Lebron"},{"DATE":"11/10/2014","OPP":"vsNO","OUTCOME":"W","PtF":118,"PA":111,"MIN":40,"PTS":32,"REB":11,"AST":9,"FGM":9,"FGA":17,"FGP":0.529,"m3p":1,"a3p":3,"P3P":0.333,"FTM":13,"FTA":17,"FTP":0.765,"BLK":1,"STL":1,"PF":1,"TO":4,"player":"James","first":"Lebron"},{"DATE":"11/14/2014","OPP":"@ BOS","OUTCOME":"W","PtF":122,"PA":121,"MIN":41,"PTS":41,"REB":4,"AST":7,"FGM":16,"FGA":27,"FGP":0.593,"m3p":3,"a3p":8,"P3P":0.375,"FTM":6,"FTA":9,"FTP":0.667,"BLK":1,"STL":0,"PF":4,"TO":3,"player":"James","first":"Lebron"},{"DATE":"11/15/2014","OPP":"vsATL","OUTCOME":"W","PtF":127,"PA":94,"MIN":29,"PTS":32,"REB":6,"AST":7,"FGM":13,"FGA":20,"FGP":0.65,"m3p":4,"a3p":5,"P3P":0.8,"FTM":2,"FTA":4,"FTP":0.5,"BLK":1,"STL":0,"PF":1,"TO":3,"player":"James","first":"Lebron"},{"DATE":"11/17/2014","OPP":"vsDEN","OUTCOME":"L","PtF":97,"PA":106,"MIN":41,"PTS":22,"REB":9,"AST":5,"FGM":8,"FGA":18,"FGP":0.444,"m3p":1,"a3p":4,"P3P":0.25,"FTM":5,"FTA":7,"FTP":0.714,"BLK":2,"STL":0,"PF":2,"TO":4,"player":"James","first":"Lebron"},{"DATE":"11/19/2014","OPP":"vsSA","OUTCOME":"L","PtF":90,"PA":92,"MIN":34,"PTS":15,"REB":6,"AST":9,"FGM":6,"FGA":17,"FGP":0.353,"m3p":2,"a3p":3,"P3P":0.667,"FTM":1,"FTA":1,"FTP":1,"BLK":1,"STL":1,"PF":3,"TO":5,"player":"James","first":"Lebron"},{"DATE":"11/21/2014","OPP":"@ WSH","OUTCOME":"L","PtF":78,"PA":91,"MIN":39,"PTS":22,"REB":4,"AST":4,"FGM":8,"FGA":21,"FGP":0.381,"m3p":2,"a3p":7,"P3P":0.286,"FTM":4,"FTA":7,"FTP":0.571,"BLK":1,"STL":1,"PF":0,"TO":3,"player":"James","first":"Lebron"},{"DATE":"11/22/2014","OPP":"vsTOR","OUTCOME":"L","PtF":93,"PA":110,"MIN":37,"PTS":15,"REB":1,"AST":10,"FGM":6,"FGA":12,"FGP":0.5,"m3p":1,"a3p":4,"P3P":0.25,"FTM":2,"FTA":3,"FTP":0.667,"BLK":1,"STL":1,"PF":2,"TO":5,"player":"James","first":"Lebron"},{"DATE":"11/24/2014","OPP":"vsORL","OUTCOME":"W","PtF":106,"PA":74,"MIN":31,"PTS":29,"REB":4,"AST":11,"FGM":9,"FGA":17,"FGP":0.529,"m3p":2,"a3p":4,"P3P":0.5,"FTM":9,"FTA":11,"FTP":0.818,"BLK":0,"STL":3,"PF":0,"TO":1,"player":"James","first":"Lebron"},{"DATE":"11/26/2014","OPP":"vsWSH","OUTCOME":"W","PtF":113,"PA":87,"MIN":36,"PTS":29,"REB":10,"AST":8,"FGM":7,"FGA":14,"FGP":0.5,"m3p":1,"a3p":5,"P3P":0.2,"FTM":14,"FTA":17,"FTP":0.824,"BLK":0,"STL":3,"PF":3,"TO":4,"player":"James","first":"Lebron"},{"DATE":"11/29/2014","OPP":"vsIND","OUTCOME":"W","PtF":109,"PA":97,"MIN":32,"PTS":19,"REB":2,"AST":7,"FGM":9,"FGA":18,"FGP":0.5,"m3p":0,"a3p":1,"P3P":0,"FTM":1,"FTA":2,"FTP":0.5,"BLK":0,"STL":1,"PF":1,"TO":3,"player":"James","first":"Lebron"},{"DATE":"12/2/2014","OPP":"vsMIL","OUTCOME":"W","PtF":111,"PA":108,"MIN":41,"PTS":26,"REB":5,"AST":10,"FGM":6,"FGA":15,"FGP":0.4,"m3p":0,"a3p":4,"P3P":0,"FTM":14,"FTA":21,"FTP":0.667,"BLK":0,"STL":0,"PF":1,"TO":7,"player":"James","first":"Lebron"},{"DATE":"12/4/2014","OPP":"@ NY","OUTCOME":"W","PtF":90,"PA":87,"MIN":40,"PTS":19,"REB":5,"AST":12,"FGM":7,"FGA":17,"FGP":0.412,"m3p":1,"a3p":6,"P3P":0.167,"FTM":4,"FTA":7,"FTP":0.571,"BLK":0,"STL":2,"PF":2,"TO":4,"player":"James","first":"Lebron"},{"DATE":"12/5/2014","OPP":"@ TOR","OUTCOME":"W","PtF":105,"PA":91,"MIN":42,"PTS":24,"REB":7,"AST":13,"FGM":9,"FGA":18,"FGP":0.5,"m3p":2,"a3p":4,"P3P":0.5,"FTM":4,"FTA":6,"FTP":0.667,"BLK":1,"STL":3,"PF":2,"TO":4,"player":"James","first":"Lebron"},{"DATE":"12/8/2014","OPP":"@ BKN","OUTCOME":"W","PtF":110,"PA":88,"MIN":34,"PTS":18,"REB":4,"AST":7,"FGM":8,"FGA":17,"FGP":0.471,"m3p":1,"a3p":3,"P3P":0.333,"FTM":1,"FTA":2,"FTP":0.5,"BLK":0,"STL":3,"PF":2,"TO":3,"player":"James","first":"Lebron"},{"DATE":"12/9/2014","OPP":"vsTOR","OUTCOME":"W","PtF":105,"PA":101,"MIN":42,"PTS":35,"REB":2,"AST":4,"FGM":12,"FGA":21,"FGP":0.571,"m3p":2,"a3p":5,"P3P":0.4,"FTM":9,"FTA":9,"FTP":1,"BLK":2,"STL":2,"PF":1,"TO":2,"player":"James","first":"Lebron"},{"DATE":"12/11/2014","OPP":"@ OKC","OUTCOME":"L","PtF":94,"PA":103,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"James","first":"Lebron"},{"DATE":"12/12/2014","OPP":"@ NO","OUTCOME":"L","PtF":114,"PA":119,"MIN":38,"PTS":41,"REB":5,"AST":5,"FGM":17,"FGA":24,"FGP":0.708,"m3p":2,"a3p":2,"P3P":1,"FTM":5,"FTA":8,"FTP":0.625,"BLK":1,"STL":0,"PF":1,"TO":2,"player":"James","first":"Lebron"},{"DATE":"12/15/2014","OPP":"vsCHA","OUTCOME":"W","PtF":97,"PA":88,"MIN":40,"PTS":27,"REB":7,"AST":14,"FGM":11,"FGA":19,"FGP":0.579,"m3p":2,"a3p":3,"P3P":0.667,"FTM":3,"FTA":4,"FTP":0.75,"BLK":3,"STL":1,"PF":2,"TO":5,"player":"James","first":"Lebron"},{"DATE":"12/17/2014","OPP":"vsATL","OUTCOME":"L","PtF":98,"PA":127,"MIN":29,"PTS":21,"REB":3,"AST":2,"FGM":8,"FGA":16,"FGP":0.5,"m3p":0,"a3p":2,"P3P":0,"FTM":5,"FTA":7,"FTP":0.714,"BLK":0,"STL":2,"PF":0,"TO":2,"player":"James","first":"Lebron"},{"DATE":"12/19/2014","OPP":"vsBKN","OUTCOME":"W","PtF":95,"PA":91,"MIN":40,"PTS":22,"REB":4,"AST":9,"FGM":7,"FGA":18,"FGP":0.389,"m3p":2,"a3p":4,"P3P":0.5,"FTM":6,"FTA":8,"FTP":0.75,"BLK":0,"STL":2,"PF":4,"TO":4,"player":"James","first":"Lebron"},{"DATE":"12/21/2014","OPP":"vsMEM","OUTCOME":"W","PtF":105,"PA":91,"MIN":38,"PTS":25,"REB":1,"AST":11,"FGM":9,"FGA":15,"FGP":0.6,"m3p":3,"a3p":5,"P3P":0.6,"FTM":4,"FTA":6,"FTP":0.667,"BLK":1,"STL":0,"PF":1,"TO":5,"player":"James","first":"Lebron"},{"DATE":"12/23/2014","OPP":"vsMIN","OUTCOME":"W","PtF":125,"PA":104,"MIN":35,"PTS":24,"REB":4,"AST":3,"FGM":8,"FGA":16,"FGP":0.5,"m3p":4,"a3p":7,"P3P":0.571,"FTM":4,"FTA":5,"FTP":0.8,"BLK":3,"STL":1,"PF":3,"TO":2,"player":"James","first":"Lebron"},{"DATE":"12/25/2014","OPP":"@ MIA","OUTCOME":"L","PtF":91,"PA":101,"MIN":40,"PTS":30,"REB":4,"AST":8,"FGM":9,"FGA":16,"FGP":0.563,"m3p":2,"a3p":8,"P3P":0.25,"FTM":10,"FTA":18,"FTP":0.556,"BLK":0,"STL":0,"PF":1,"TO":4,"player":"James","first":"Lebron"},{"DATE":"12/26/2014","OPP":"@ ORL","OUTCOME":"W","PtF":98,"PA":89,"MIN":37,"PTS":29,"REB":5,"AST":8,"FGM":10,"FGA":20,"FGP":0.5,"m3p":0,"a3p":5,"P3P":0,"FTM":9,"FTA":11,"FTP":0.818,"BLK":2,"STL":2,"PF":1,"TO":4,"player":"James","first":"Lebron"},{"DATE":"12/28/2014","OPP":"vsDET","OUTCOME":"L","PtF":80,"PA":103,"MIN":32,"PTS":17,"REB":10,"AST":7,"FGM":5,"FGA":19,"FGP":0.263,"m3p":2,"a3p":6,"P3P":0.333,"FTM":5,"FTA":5,"FTP":1,"BLK":0,"STL":1,"PF":3,"TO":7,"player":"James","first":"Lebron"},{"DATE":"12/30/2014","OPP":"@ ATL","OUTCOME":"L","PtF":101,"PA":109,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"James","first":"Lebron"},{"DATE":"12/31/2014","OPP":"vsMIL","OUTCOME":"L","PtF":80,"PA":96,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"James","first":"Lebron"},{"DATE":"1/2/2015","OPP":"@ CHA","OUTCOME":"W","PtF":91,"PA":87,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"James","first":"Lebron"},{"DATE":"1/4/2015","OPP":"vsDAL","OUTCOME":"L","PtF":90,"PA":109,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"James","first":"Lebron"},{"DATE":"1/5/2015","OPP":"@ PHI","OUTCOME":"L","PtF":92,"PA":95,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"James","first":"Lebron"},{"DATE":"1/7/2015","OPP":"vsHOU","OUTCOME":"L","PtF":93,"PA":105,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"James","first":"Lebron"},{"DATE":"1/9/2015","OPP":"@ GS","OUTCOME":"L","PtF":94,"PA":112,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"James","first":"Lebron"},{"DATE":"1/11/2015","OPP":"@ SAC","OUTCOME":"L","PtF":84,"PA":103,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"James","first":"Lebron"},{"DATE":"1/13/2015","OPP":"@ PHX","OUTCOME":"L","PtF":100,"PA":107,"MIN":37,"PTS":33,"REB":7,"AST":5,"FGM":11,"FGA":18,"FGP":0.611,"m3p":3,"a3p":8,"P3P":0.375,"FTM":8,"FTA":13,"FTP":0.615,"BLK":0,"STL":1,"PF":3,"TO":5,"player":"James","first":"Lebron"},{"DATE":"1/15/2015","OPP":"@ LAL","OUTCOME":"W","PtF":109,"PA":102,"MIN":38,"PTS":36,"REB":4,"AST":5,"FGM":12,"FGA":23,"FGP":0.522,"m3p":4,"a3p":9,"P3P":0.444,"FTM":8,"FTA":13,"FTP":0.615,"BLK":0,"STL":2,"PF":2,"TO":1,"player":"James","first":"Lebron"},{"DATE":"1/16/2015","OPP":"@ LAC","OUTCOME":"W","PtF":126,"PA":121,"MIN":42,"PTS":32,"REB":11,"AST":7,"FGM":11,"FGA":23,"FGP":0.478,"m3p":3,"a3p":8,"P3P":0.375,"FTM":7,"FTA":12,"FTP":0.583,"BLK":0,"STL":3,"PF":4,"TO":9,"player":"James","first":"Lebron"},{"DATE":"1/19/2015","OPP":"vsCHI","OUTCOME":"W","PtF":108,"PA":94,"MIN":36,"PTS":26,"REB":5,"AST":4,"FGM":12,"FGA":23,"FGP":0.522,"m3p":0,"a3p":3,"P3P":0,"FTM":2,"FTA":3,"FTP":0.667,"BLK":0,"STL":0,"PF":4,"TO":6,"player":"James","first":"Lebron"},{"DATE":"1/21/2015","OPP":"vsUTAH","OUTCOME":"W","PtF":106,"PA":92,"MIN":36,"PTS":26,"REB":7,"AST":9,"FGM":8,"FGA":16,"FGP":0.5,"m3p":0,"a3p":2,"P3P":0,"FTM":10,"FTA":14,"FTP":0.714,"BLK":0,"STL":4,"PF":0,"TO":7,"player":"James","first":"Lebron"},{"DATE":"1/23/2015","OPP":"vsCHA","OUTCOME":"W","PtF":129,"PA":90,"MIN":27,"PTS":25,"REB":6,"AST":9,"FGM":9,"FGA":15,"FGP":0.6,"m3p":2,"a3p":4,"P3P":0.5,"FTM":5,"FTA":6,"FTP":0.833,"BLK":1,"STL":4,"PF":1,"TO":2,"player":"James","first":"Lebron"},{"DATE":"1/25/2015","OPP":"vsOKC","OUTCOME":"W","PtF":108,"PA":98,"MIN":39,"PTS":34,"REB":7,"AST":5,"FGM":11,"FGA":25,"FGP":0.44,"m3p":2,"a3p":6,"P3P":0.333,"FTM":10,"FTA":13,"FTP":0.769,"BLK":2,"STL":1,"PF":1,"TO":2,"player":"James","first":"Lebron"},{"DATE":"1/27/2015","OPP":"@ DET","OUTCOME":"W","PtF":103,"PA":95,"MIN":36,"PTS":32,"REB":6,"AST":7,"FGM":11,"FGA":24,"FGP":0.458,"m3p":0,"a3p":4,"P3P":0,"FTM":10,"FTA":11,"FTP":0.909,"BLK":0,"STL":1,"PF":3,"TO":4,"player":"James","first":"Lebron"},{"DATE":"1/28/2015","OPP":"vsPOR","OUTCOME":"W","PtF":99,"PA":94,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"James","first":"Lebron"},{"DATE":"1/30/2015","OPP":"vsSAC","OUTCOME":"W","PtF":101,"PA":90,"MIN":33,"PTS":19,"REB":3,"AST":7,"FGM":7,"FGA":16,"FGP":0.438,"m3p":1,"a3p":4,"P3P":0.25,"FTM":4,"FTA":6,"FTP":0.667,"BLK":2,"STL":4,"PF":1,"TO":6,"player":"James","first":"Lebron"},{"DATE":"1/31/2015","OPP":"@ MIN","OUTCOME":"W","PtF":106,"PA":90,"MIN":40,"PTS":36,"REB":6,"AST":5,"FGM":14,"FGA":25,"FGP":0.56,"m3p":3,"a3p":8,"P3P":0.375,"FTM":5,"FTA":8,"FTP":0.625,"BLK":1,"STL":0,"PF":1,"TO":2,"player":"James","first":"Lebron"},{"DATE":"2/2/2015","OPP":"vsPHI","OUTCOME":"W","PtF":97,"PA":84,"MIN":36,"PTS":18,"REB":3,"AST":11,"FGM":6,"FGA":17,"FGP":0.353,"m3p":1,"a3p":4,"P3P":0.25,"FTM":5,"FTA":6,"FTP":0.833,"BLK":0,"STL":2,"PF":2,"TO":6,"player":"James","first":"Lebron"},{"DATE":"2/5/2015","OPP":"vsLAC","OUTCOME":"W","PtF":105,"PA":94,"MIN":28,"PTS":23,"REB":3,"AST":9,"FGM":8,"FGA":14,"FGP":0.571,"m3p":1,"a3p":2,"P3P":0.5,"FTM":6,"FTA":7,"FTP":0.857,"BLK":0,"STL":1,"PF":0,"TO":2,"player":"James","first":"Lebron"},{"DATE":"2/6/2015","OPP":"@ IND","OUTCOME":"L","PtF":99,"PA":103,"MIN":39,"PTS":25,"REB":6,"AST":5,"FGM":10,"FGA":21,"FGP":0.476,"m3p":0,"a3p":4,"P3P":0,"FTM":5,"FTA":8,"FTP":0.625,"BLK":1,"STL":2,"PF":3,"TO":7,"player":"James","first":"Lebron"},{"DATE":"2/8/2015","OPP":"vsLAL","OUTCOME":"W","PtF":120,"PA":105,"MIN":30,"PTS":22,"REB":10,"AST":8,"FGM":8,"FGA":16,"FGP":0.5,"m3p":2,"a3p":7,"P3P":0.286,"FTM":4,"FTA":5,"FTP":0.8,"BLK":0,"STL":1,"PF":2,"TO":6,"player":"James","first":"Lebron"},{"DATE":"2/11/2015","OPP":"vsMIA","OUTCOME":"W","PtF":113,"PA":93,"MIN":31,"PTS":18,"REB":9,"AST":7,"FGM":6,"FGA":16,"FGP":0.375,"m3p":0,"a3p":4,"P3P":0,"FTM":6,"FTA":7,"FTP":0.857,"BLK":0,"STL":1,"PF":2,"TO":5,"player":"James","first":"Lebron"},{"DATE":"2/12/2015","OPP":"@ CHI","OUTCOME":"L","PtF":98,"PA":113,"MIN":34,"PTS":31,"REB":5,"AST":4,"FGM":12,"FGA":26,"FGP":0.462,"m3p":2,"a3p":9,"P3P":0.222,"FTM":5,"FTA":6,"FTP":0.833,"BLK":0,"STL":0,"PF":3,"TO":8,"player":"James","first":"Lebron"},{"DATE":"2/20/2015","OPP":"@ WSH","OUTCOME":"W","PtF":127,"PA":89,"MIN":25,"PTS":28,"REB":5,"AST":6,"FGM":12,"FGA":21,"FGP":0.571,"m3p":3,"a3p":5,"P3P":0.6,"FTM":1,"FTA":3,"FTP":0.333,"BLK":0,"STL":1,"PF":3,"TO":2,"player":"James","first":"Lebron"},{"DATE":"2/22/2015","OPP":"@ NY","OUTCOME":"W","PtF":101,"PA":83,"MIN":28,"PTS":18,"REB":7,"AST":7,"FGM":7,"FGA":17,"FGP":0.412,"m3p":1,"a3p":6,"P3P":0.167,"FTM":3,"FTA":5,"FTP":0.6,"BLK":0,"STL":2,"PF":2,"TO":4,"player":"James","first":"Lebron"},{"DATE":"2/24/2015","OPP":"@ DET","OUTCOME":"W","PtF":102,"PA":93,"MIN":39,"PTS":19,"REB":7,"AST":11,"FGM":6,"FGA":14,"FGP":0.429,"m3p":1,"a3p":5,"P3P":0.2,"FTM":6,"FTA":10,"FTP":0.6,"BLK":1,"STL":3,"PF":3,"TO":9,"player":"James","first":"Lebron"},{"DATE":"2/26/2015","OPP":"vsGS","OUTCOME":"W","PtF":110,"PA":99,"MIN":36,"PTS":42,"REB":11,"AST":5,"FGM":15,"FGA":25,"FGP":0.6,"m3p":4,"a3p":9,"P3P":0.444,"FTM":8,"FTA":11,"FTP":0.727,"BLK":1,"STL":3,"PF":5,"TO":6,"player":"James","first":"Lebron"},{"DATE":"2/27/2015","OPP":"@ IND","OUTCOME":"L","PtF":86,"PA":93,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"James","first":"Lebron"},{"DATE":"3/1/2015","OPP":"@ HOU","OUTCOME":"L","PtF":103,"PA":105,"MIN":42,"PTS":37,"REB":8,"AST":4,"FGM":15,"FGA":35,"FGP":0.429,"m3p":4,"a3p":12,"P3P":0.333,"FTM":3,"FTA":11,"FTP":0.273,"BLK":3,"STL":3,"PF":4,"TO":4,"player":"James","first":"Lebron"},{"DATE":"3/3/2015","OPP":"vsBOS","OUTCOME":"W","PtF":110,"PA":79,"MIN":26,"PTS":27,"REB":3,"AST":5,"FGM":12,"FGA":23,"FGP":0.522,"m3p":0,"a3p":1,"P3P":0,"FTM":3,"FTA":3,"FTP":1,"BLK":1,"STL":3,"PF":0,"TO":1,"player":"James","first":"Lebron"},{"DATE":"3/4/2015","OPP":"@ TOR","OUTCOME":"W","PtF":120,"PA":112,"MIN":37,"PTS":29,"REB":6,"AST":14,"FGM":9,"FGA":16,"FGP":0.563,"m3p":3,"a3p":6,"P3P":0.5,"FTM":8,"FTA":13,"FTP":0.615,"BLK":0,"STL":0,"PF":2,"TO":3,"player":"James","first":"Lebron"},{"DATE":"3/6/2015","OPP":"@ ATL","OUTCOME":"L","PtF":97,"PA":106,"MIN":40,"PTS":18,"REB":6,"AST":8,"FGM":5,"FGA":13,"FGP":0.385,"m3p":1,"a3p":4,"P3P":0.25,"FTM":7,"FTA":8,"FTP":0.875,"BLK":1,"STL":2,"PF":3,"TO":9,"player":"James","first":"Lebron"},{"DATE":"3/7/2015","OPP":"vsPHX","OUTCOME":"W","PtF":89,"PA":79,"MIN":36,"PTS":17,"REB":6,"AST":8,"FGM":6,"FGA":16,"FGP":0.375,"m3p":2,"a3p":5,"P3P":0.4,"FTM":3,"FTA":6,"FTP":0.5,"BLK":0,"STL":1,"PF":3,"TO":4,"player":"James","first":"Lebron"},{"DATE":"3/10/2015","OPP":"@ DAL","OUTCOME":"W","PtF":127,"PA":94,"MIN":29,"PTS":27,"REB":7,"AST":8,"FGM":10,"FGA":14,"FGP":0.714,"m3p":4,"a3p":6,"P3P":0.667,"FTM":3,"FTA":5,"FTP":0.6,"BLK":1,"STL":1,"PF":3,"TO":2,"player":"James","first":"Lebron"},{"DATE":"3/12/2015","OPP":"@ SA","OUTCOME":"W","PtF":128,"PA":125,"MIN":44,"PTS":31,"REB":5,"AST":7,"FGM":10,"FGA":20,"FGP":0.5,"m3p":3,"a3p":6,"P3P":0.5,"FTM":8,"FTA":10,"FTP":0.8,"BLK":1,"STL":2,"PF":2,"TO":3,"player":"James","first":"Lebron"},{"DATE":"3/15/2015","OPP":"@ ORL","OUTCOME":"W","PtF":123,"PA":108,"MIN":35,"PTS":21,"REB":8,"AST":13,"FGM":9,"FGA":14,"FGP":0.643,"m3p":0,"a3p":3,"P3P":0,"FTM":3,"FTA":4,"FTP":0.75,"BLK":1,"STL":3,"PF":3,"TO":1,"player":"James","first":"Lebron"},{"DATE":"3/16/2015","OPP":"@ MIA","OUTCOME":"L","PtF":92,"PA":106,"MIN":35,"PTS":26,"REB":2,"AST":4,"FGM":8,"FGA":19,"FGP":0.421,"m3p":4,"a3p":8,"P3P":0.5,"FTM":6,"FTA":8,"FTP":0.75,"BLK":0,"STL":0,"PF":2,"TO":4,"player":"James","first":"Lebron"},{"DATE":"3/18/2015","OPP":"vsBKN","OUTCOME":"W","PtF":117,"PA":92,"MIN":35,"PTS":16,"REB":3,"AST":7,"FGM":5,"FGA":13,"FGP":0.385,"m3p":2,"a3p":4,"P3P":0.5,"FTM":4,"FTA":6,"FTP":0.667,"BLK":0,"STL":1,"PF":1,"TO":2,"player":"James","first":"Lebron"},{"DATE":"3/20/2015","OPP":"vsIND","OUTCOME":"W","PtF":95,"PA":92,"MIN":39,"PTS":29,"REB":7,"AST":5,"FGM":13,"FGA":26,"FGP":0.5,"m3p":2,"a3p":6,"P3P":0.333,"FTM":1,"FTA":2,"FTP":0.5,"BLK":0,"STL":0,"PF":0,"TO":3,"player":"James","first":"Lebron"},{"DATE":"3/22/2015","OPP":"@ MIL","OUTCOME":"W","PtF":108,"PA":90,"MIN":39,"PTS":28,"REB":10,"AST":6,"FGM":10,"FGA":17,"FGP":0.588,"m3p":1,"a3p":4,"P3P":0.25,"FTM":7,"FTA":7,"FTP":1,"BLK":1,"STL":5,"PF":1,"TO":5,"player":"James","first":"Lebron"},{"DATE":"3/25/2015","OPP":"@ MEM","OUTCOME":"W","PtF":111,"PA":89,"MIN":31,"PTS":20,"REB":8,"AST":5,"FGM":7,"FGA":15,"FGP":0.467,"m3p":3,"a3p":5,"P3P":0.6,"FTM":3,"FTA":4,"FTP":0.75,"BLK":0,"STL":0,"PF":2,"TO":1,"player":"James","first":"Lebron"},{"DATE":"3/27/2015","OPP":"@ BKN","OUTCOME":"L","PtF":98,"PA":106,"MIN":38,"PTS":24,"REB":5,"AST":9,"FGM":8,"FGA":15,"FGP":0.533,"m3p":2,"a3p":5,"P3P":0.4,"FTM":6,"FTA":8,"FTP":0.75,"BLK":1,"STL":5,"PF":4,"TO":3,"player":"James","first":"Lebron"},{"DATE":"3/29/2015","OPP":"vsPHI","OUTCOME":"W","PtF":87,"PA":86,"MIN":38,"PTS":20,"REB":11,"AST":6,"FGM":8,"FGA":24,"FGP":0.333,"m3p":1,"a3p":4,"P3P":0.25,"FTM":3,"FTA":6,"FTP":0.5,"BLK":1,"STL":0,"PF":0,"TO":5,"player":"James","first":"Lebron"},{"DATE":"4/2/2015","OPP":"vsMIA","OUTCOME":"W","PtF":114,"PA":88,"MIN":35,"PTS":23,"REB":8,"AST":7,"FGM":9,"FGA":16,"FGP":0.563,"m3p":1,"a3p":2,"P3P":0.5,"FTM":4,"FTA":5,"FTP":0.8,"BLK":3,"STL":1,"PF":3,"TO":3,"player":"James","first":"Lebron"},{"DATE":"4/5/2015","OPP":"vsCHI","OUTCOME":"W","PtF":99,"PA":94,"MIN":37,"PTS":20,"REB":10,"AST":12,"FGM":8,"FGA":17,"FGP":0.471,"m3p":1,"a3p":3,"P3P":0.333,"FTM":3,"FTA":6,"FTP":0.5,"BLK":1,"STL":3,"PF":4,"TO":3,"player":"James","first":"Lebron"},{"DATE":"4/8/2015","OPP":"@ MIL","OUTCOME":"W","PtF":104,"PA":99,"MIN":40,"PTS":21,"REB":9,"AST":8,"FGM":8,"FGA":15,"FGP":0.533,"m3p":2,"a3p":5,"P3P":0.4,"FTM":3,"FTA":4,"FTP":0.75,"BLK":1,"STL":1,"PF":2,"TO":2,"player":"James","first":"Lebron"},{"DATE":"4/10/2015","OPP":"vsBOS","OUTCOME":"L","PtF":90,"PA":99,"MIN":26,"PTS":14,"REB":3,"AST":7,"FGM":5,"FGA":14,"FGP":0.357,"m3p":0,"a3p":5,"P3P":0,"FTM":4,"FTA":9,"FTP":0.444,"BLK":0,"STL":1,"PF":1,"TO":3,"player":"James","first":"Lebron"},{"DATE":"4/12/2015","OPP":"@BOS","OUTCOME":"L","PtF":78,"PA":117,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"James","first":"Lebron"},{"DATE":"4/13/2015","OPP":"vsDET","OUTCOME":"W","PtF":109,"PA":97,"MIN":32,"PTS":21,"REB":10,"AST":11,"FGM":7,"FGA":17,"FGP":0.412,"m3p":3,"a3p":4,"P3P":0.75,"FTM":4,"FTA":6,"FTP":0.667,"BLK":1,"STL":2,"PF":0,"TO":1,"player":"James","first":"Lebron"},{"DATE":"4/15/2015","OPP":"vsUTAH","OUTCOME":"W","PtF":113,"PA":108,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"James","first":"Lebron"},{"DATE":"10/29/2014","OPP":"@ POR","OUTCOME":"L","PtF":89,"PA":106,"MIN":33,"PTS":38,"REB":3,"AST":6,"FGM":11,"FGA":26,"FGP":0.423,"m3p":1,"a3p":2,"P3P":0.5,"FTM":15,"FTA":16,"FTP":0.938,"BLK":0,"STL":3,"PF":3,"TO":2,"player":"Westbrook","first":"Russ"},{"DATE":"10/30/2014","OPP":"@ LAC","OUTCOME":"L","PtF":90,"PA":93,"MIN":9,"PTS":2,"REB":0,"AST":4,"FGM":1,"FGA":3,"FGP":0.333,"m3p":0,"a3p":1,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":1,"PF":2,"TO":3,"player":"Westbrook","first":"Russ"},{"DATE":"11/1/2014","OPP":"vsDEN","OUTCOME":"W","PtF":102,"PA":91,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Westbrook","first":"Russ"},{"DATE":"11/3/2014","OPP":"@ BKN","OUTCOME":"L","PtF":85,"PA":116,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Westbrook","first":"Russ"},{"DATE":"11/4/2014","OPP":"@ TOR","OUTCOME":"L","PtF":88,"PA":100,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Westbrook","first":"Russ"},{"DATE":"11/7/2014","OPP":"vsMEM","OUTCOME":"L","PtF":89,"PA":91,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Westbrook","first":"Russ"},{"DATE":"11/9/2014","OPP":"vsSAC","OUTCOME":"W","PtF":101,"PA":93,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Westbrook","first":"Russ"},{"DATE":"11/11/2014","OPP":"@ MIL","OUTCOME":"L","PtF":78,"PA":85,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Westbrook","first":"Russ"},{"DATE":"11/12/2014","OPP":"@ BOS","OUTCOME":"W","PtF":109,"PA":94,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Westbrook","first":"Russ"},{"DATE":"11/14/2014","OPP":"vsDET","OUTCOME":"L","PtF":89,"PA":96,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Westbrook","first":"Russ"},{"DATE":"11/16/2014","OPP":"vsHOU","OUTCOME":"L","PtF":65,"PA":69,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Westbrook","first":"Russ"},{"DATE":"11/18/2014","OPP":"@ UTAH","OUTCOME":"L","PtF":81,"PA":98,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Westbrook","first":"Russ"},{"DATE":"11/19/2014","OPP":"@ DEN","OUTCOME":"L","PtF":100,"PA":107,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Westbrook","first":"Russ"},{"DATE":"11/21/2014","OPP":"vsBKN","OUTCOME":"L","PtF":92,"PA":94,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Westbrook","first":"Russ"},{"DATE":"11/23/2014","OPP":"vsGS","OUTCOME":"L","PtF":86,"PA":91,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Westbrook","first":"Russ"},{"DATE":"11/26/2014","OPP":"vsUTAH","OUTCOME":"W","PtF":97,"PA":82,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Westbrook","first":"Russ"},{"DATE":"11/28/2014","OPP":"vsNY","OUTCOME":"W","PtF":105,"PA":78,"MIN":24,"PTS":32,"REB":7,"AST":8,"FGM":12,"FGA":17,"FGP":0.706,"m3p":3,"a3p":4,"P3P":0.75,"FTM":5,"FTA":7,"FTP":0.714,"BLK":0,"STL":1,"PF":2,"TO":3,"player":"Westbrook","first":"Russ"},{"DATE":"12/2/2014","OPP":"@ NO","OUTCOME":"L","PtF":104,"PA":112,"MIN":33,"PTS":21,"REB":6,"AST":7,"FGM":6,"FGA":20,"FGP":0.3,"m3p":1,"a3p":4,"P3P":0.25,"FTM":8,"FTA":9,"FTP":0.889,"BLK":0,"STL":2,"PF":3,"TO":7,"player":"Westbrook","first":"Russ"},{"DATE":"12/5/2014","OPP":"@ PHI","OUTCOME":"W","PtF":103,"PA":91,"MIN":31,"PTS":27,"REB":5,"AST":7,"FGM":8,"FGA":14,"FGP":0.571,"m3p":1,"a3p":3,"P3P":0.333,"FTM":10,"FTA":14,"FTP":0.714,"BLK":0,"STL":2,"PF":3,"TO":6,"player":"Westbrook","first":"Russ"},{"DATE":"12/7/2014","OPP":"@ DET","OUTCOME":"W","PtF":96,"PA":94,"MIN":35,"PTS":22,"REB":11,"AST":7,"FGM":9,"FGA":19,"FGP":0.474,"m3p":1,"a3p":3,"P3P":0.333,"FTM":3,"FTA":6,"FTP":0.5,"BLK":0,"STL":0,"PF":1,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"12/9/2014","OPP":"vsMIL","OUTCOME":"W","PtF":114,"PA":101,"MIN":32,"PTS":28,"REB":5,"AST":7,"FGM":8,"FGA":16,"FGP":0.5,"m3p":0,"a3p":2,"P3P":0,"FTM":12,"FTA":15,"FTP":0.8,"BLK":0,"STL":3,"PF":5,"TO":5,"player":"Westbrook","first":"Russ"},{"DATE":"12/11/2014","OPP":"vsCLE","OUTCOME":"W","PtF":103,"PA":94,"MIN":36,"PTS":26,"REB":7,"AST":8,"FGM":12,"FGA":24,"FGP":0.5,"m3p":0,"a3p":2,"P3P":0,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":1,"PF":0,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"12/12/2014","OPP":"@ MIN","OUTCOME":"W","PtF":111,"PA":92,"MIN":36,"PTS":34,"REB":6,"AST":6,"FGM":12,"FGA":19,"FGP":0.632,"m3p":2,"a3p":3,"P3P":0.667,"FTM":8,"FTA":9,"FTP":0.889,"BLK":0,"STL":2,"PF":3,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"12/14/2014","OPP":"vsPHX","OUTCOME":"W","PtF":112,"PA":88,"MIN":27,"PTS":28,"REB":8,"AST":8,"FGM":7,"FGA":18,"FGP":0.389,"m3p":1,"a3p":4,"P3P":0.25,"FTM":13,"FTA":13,"FTP":1,"BLK":0,"STL":2,"PF":2,"TO":3,"player":"Westbrook","first":"Russ"},{"DATE":"12/16/2014","OPP":"@ SAC","OUTCOME":"W","PtF":104,"PA":92,"MIN":32,"PTS":32,"REB":6,"AST":7,"FGM":11,"FGA":20,"FGP":0.55,"m3p":1,"a3p":3,"P3P":0.333,"FTM":9,"FTA":12,"FTP":0.75,"BLK":0,"STL":4,"PF":4,"TO":1,"player":"Westbrook","first":"Russ"},{"DATE":"12/18/2014","OPP":"@ GS","OUTCOME":"L","PtF":109,"PA":114,"MIN":40,"PTS":33,"REB":3,"AST":8,"FGM":11,"FGA":30,"FGP":0.367,"m3p":3,"a3p":10,"P3P":0.3,"FTM":8,"FTA":12,"FTP":0.667,"BLK":0,"STL":2,"PF":2,"TO":6,"player":"Westbrook","first":"Russ"},{"DATE":"12/19/2014","OPP":"@ LAL","OUTCOME":"W","PtF":104,"PA":103,"MIN":36,"PTS":31,"REB":5,"AST":10,"FGM":9,"FGA":22,"FGP":0.409,"m3p":0,"a3p":1,"P3P":0,"FTM":13,"FTA":13,"FTP":1,"BLK":0,"STL":1,"PF":2,"TO":1,"player":"Westbrook","first":"Russ"},{"DATE":"12/21/2014","OPP":"vsNO","OUTCOME":"L","PtF":99,"PA":101,"MIN":35,"PTS":29,"REB":5,"AST":8,"FGM":10,"FGA":27,"FGP":0.37,"m3p":0,"a3p":6,"P3P":0,"FTM":9,"FTA":11,"FTP":0.818,"BLK":0,"STL":3,"PF":2,"TO":6,"player":"Westbrook","first":"Russ"},{"DATE":"12/23/2014","OPP":"vsPOR","OUTCOME":"L","PtF":111,"PA":115,"MIN":39,"PTS":40,"REB":10,"AST":6,"FGM":16,"FGA":34,"FGP":0.471,"m3p":1,"a3p":5,"P3P":0.2,"FTM":7,"FTA":9,"FTP":0.778,"BLK":0,"STL":3,"PF":6,"TO":2,"player":"Westbrook","first":"Russ"},{"DATE":"12/25/2014","OPP":"@ SA","OUTCOME":"W","PtF":114,"PA":106,"MIN":35,"PTS":34,"REB":5,"AST":11,"FGM":14,"FGA":28,"FGP":0.5,"m3p":0,"a3p":1,"P3P":0,"FTM":6,"FTA":8,"FTP":0.75,"BLK":0,"STL":5,"PF":0,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"12/26/2014","OPP":"vsCHA","OUTCOME":"W","PtF":98,"PA":75,"MIN":25,"PTS":29,"REB":2,"AST":5,"FGM":11,"FGA":21,"FGP":0.524,"m3p":1,"a3p":1,"P3P":1,"FTM":6,"FTA":6,"FTP":1,"BLK":1,"STL":2,"PF":2,"TO":6,"player":"Westbrook","first":"Russ"},{"DATE":"12/28/2014","OPP":"@ DAL","OUTCOME":"L","PtF":107,"PA":112,"MIN":36,"PTS":18,"REB":9,"AST":9,"FGM":6,"FGA":23,"FGP":0.261,"m3p":0,"a3p":1,"P3P":0,"FTM":6,"FTA":9,"FTP":0.667,"BLK":0,"STL":5,"PF":5,"TO":5,"player":"Westbrook","first":"Russ"},{"DATE":"12/31/2014","OPP":"vsPHX","OUTCOME":"W","PtF":137,"PA":134,"MIN":17,"PTS":20,"REB":4,"AST":4,"FGM":5,"FGA":13,"FGP":0.385,"m3p":1,"a3p":2,"P3P":0.5,"FTM":9,"FTA":10,"FTP":0.9,"BLK":0,"STL":1,"PF":1,"TO":1,"player":"Westbrook","first":"Russ"},{"DATE":"1/2/2015","OPP":"vsWSH","OUTCOME":"W","PtF":109,"PA":102,"MIN":30,"PTS":22,"REB":3,"AST":6,"FGM":8,"FGA":23,"FGP":0.348,"m3p":0,"a3p":3,"P3P":0,"FTM":6,"FTA":7,"FTP":0.857,"BLK":0,"STL":2,"PF":5,"TO":2,"player":"Westbrook","first":"Russ"},{"DATE":"1/5/2015","OPP":"@ GS","OUTCOME":"L","PtF":91,"PA":117,"MIN":29,"PTS":22,"REB":7,"AST":5,"FGM":5,"FGA":21,"FGP":0.238,"m3p":1,"a3p":5,"P3P":0.2,"FTM":11,"FTA":14,"FTP":0.786,"BLK":0,"STL":1,"PF":2,"TO":2,"player":"Westbrook","first":"Russ"},{"DATE":"1/7/2015","OPP":"@ SAC","OUTCOME":"L","PtF":83,"PA":104,"MIN":27,"PTS":10,"REB":7,"AST":4,"FGM":3,"FGA":19,"FGP":0.158,"m3p":0,"a3p":5,"P3P":0,"FTM":4,"FTA":5,"FTP":0.8,"BLK":0,"STL":1,"PF":4,"TO":7,"player":"Westbrook","first":"Russ"},{"DATE":"1/9/2015","OPP":"vsUTAH","OUTCOME":"W","PtF":99,"PA":94,"MIN":38,"PTS":25,"REB":4,"AST":12,"FGM":9,"FGA":17,"FGP":0.529,"m3p":1,"a3p":1,"P3P":1,"FTM":6,"FTA":6,"FTP":1,"BLK":0,"STL":3,"PF":3,"TO":3,"player":"Westbrook","first":"Russ"},{"DATE":"1/15/2015","OPP":"@ HOU","OUTCOME":"L","PtF":101,"PA":112,"MIN":38,"PTS":16,"REB":8,"AST":8,"FGM":6,"FGA":13,"FGP":0.462,"m3p":1,"a3p":3,"P3P":0.333,"FTM":3,"FTA":4,"FTP":0.75,"BLK":0,"STL":5,"PF":5,"TO":8,"player":"Westbrook","first":"Russ"},{"DATE":"1/16/2015","OPP":"vsGS","OUTCOME":"W","PtF":127,"PA":115,"MIN":36,"PTS":17,"REB":15,"AST":16,"FGM":7,"FGA":19,"FGP":0.368,"m3p":1,"a3p":4,"P3P":0.25,"FTM":2,"FTA":4,"FTP":0.5,"BLK":0,"STL":4,"PF":4,"TO":3,"player":"Westbrook","first":"Russ"},{"DATE":"1/18/2015","OPP":"@ ORL","OUTCOME":"W","PtF":127,"PA":99,"MIN":24,"PTS":17,"REB":1,"AST":6,"FGM":7,"FGA":12,"FGP":0.583,"m3p":0,"a3p":3,"P3P":0,"FTM":3,"FTA":6,"FTP":0.5,"BLK":1,"STL":1,"PF":2,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"1/20/2015","OPP":"@ MIA","OUTCOME":"W","PtF":94,"PA":86,"MIN":35,"PTS":19,"REB":10,"AST":6,"FGM":8,"FGA":17,"FGP":0.471,"m3p":0,"a3p":2,"P3P":0,"FTM":3,"FTA":4,"FTP":0.75,"BLK":0,"STL":4,"PF":2,"TO":3,"player":"Westbrook","first":"Russ"},{"DATE":"1/21/2015","OPP":"@ WSH","OUTCOME":"W","PtF":105,"PA":103,"MIN":42,"PTS":32,"REB":8,"AST":8,"FGM":12,"FGA":28,"FGP":0.429,"m3p":0,"a3p":4,"P3P":0,"FTM":8,"FTA":10,"FTP":0.8,"BLK":0,"STL":1,"PF":4,"TO":3,"player":"Westbrook","first":"Russ"},{"DATE":"1/23/2015","OPP":"@ ATL","OUTCOME":"L","PtF":93,"PA":103,"MIN":36,"PTS":22,"REB":3,"AST":11,"FGM":8,"FGA":16,"FGP":0.5,"m3p":2,"a3p":4,"P3P":0.5,"FTM":4,"FTA":6,"FTP":0.667,"BLK":0,"STL":3,"PF":3,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"1/25/2015","OPP":"@ CLE","OUTCOME":"L","PtF":98,"PA":108,"MIN":40,"PTS":22,"REB":9,"AST":11,"FGM":7,"FGA":26,"FGP":0.269,"m3p":3,"a3p":6,"P3P":0.5,"FTM":5,"FTA":7,"FTP":0.714,"BLK":1,"STL":2,"PF":3,"TO":2,"player":"Westbrook","first":"Russ"},{"DATE":"1/26/2015","OPP":"vsMIN","OUTCOME":"W","PtF":92,"PA":84,"MIN":31,"PTS":18,"REB":4,"AST":5,"FGM":7,"FGA":22,"FGP":0.318,"m3p":0,"a3p":3,"P3P":0,"FTM":4,"FTA":6,"FTP":0.667,"BLK":0,"STL":1,"PF":2,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"1/28/2015","OPP":"@ NY","OUTCOME":"L","PtF":92,"PA":100,"MIN":35,"PTS":40,"REB":4,"AST":4,"FGM":13,"FGA":30,"FGP":0.433,"m3p":2,"a3p":8,"P3P":0.25,"FTM":12,"FTA":12,"FTP":1,"BLK":1,"STL":2,"PF":1,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"1/31/2015","OPP":"@ MEM","OUTCOME":"L","PtF":74,"PA":85,"MIN":32,"PTS":14,"REB":6,"AST":5,"FGM":5,"FGA":16,"FGP":0.313,"m3p":1,"a3p":3,"P3P":0.333,"FTM":3,"FTA":4,"FTP":0.75,"BLK":0,"STL":0,"PF":4,"TO":3,"player":"Westbrook","first":"Russ"},{"DATE":"2/2/2015","OPP":"vsORL","OUTCOME":"W","PtF":104,"PA":97,"MIN":38,"PTS":25,"REB":11,"AST":14,"FGM":8,"FGA":15,"FGP":0.533,"m3p":1,"a3p":3,"P3P":0.333,"FTM":8,"FTA":8,"FTP":1,"BLK":1,"STL":4,"PF":1,"TO":2,"player":"Westbrook","first":"Russ"},{"DATE":"2/4/2015","OPP":"@ NO","OUTCOME":"W","PtF":102,"PA":91,"MIN":40,"PTS":45,"REB":6,"AST":6,"FGM":18,"FGA":31,"FGP":0.581,"m3p":2,"a3p":6,"P3P":0.333,"FTM":7,"FTA":9,"FTP":0.778,"BLK":1,"STL":1,"PF":2,"TO":6,"player":"Westbrook","first":"Russ"},{"DATE":"2/6/2015","OPP":"vsNO","OUTCOME":"L","PtF":113,"PA":116,"MIN":40,"PTS":48,"REB":9,"AST":11,"FGM":15,"FGA":28,"FGP":0.536,"m3p":3,"a3p":5,"P3P":0.6,"FTM":15,"FTA":15,"FTP":1,"BLK":0,"STL":4,"PF":4,"TO":3,"player":"Westbrook","first":"Russ"},{"DATE":"2/8/2015","OPP":"vsLAC","OUTCOME":"W","PtF":131,"PA":108,"MIN":32,"PTS":19,"REB":11,"AST":5,"FGM":6,"FGA":19,"FGP":0.316,"m3p":1,"a3p":4,"P3P":0.25,"FTM":6,"FTA":6,"FTP":1,"BLK":0,"STL":1,"PF":2,"TO":5,"player":"Westbrook","first":"Russ"},{"DATE":"2/9/2015","OPP":"@ DEN","OUTCOME":"W","PtF":124,"PA":114,"MIN":32,"PTS":26,"REB":5,"AST":9,"FGM":8,"FGA":14,"FGP":0.571,"m3p":1,"a3p":2,"P3P":0.5,"FTM":9,"FTA":12,"FTP":0.75,"BLK":0,"STL":0,"PF":1,"TO":1,"player":"Westbrook","first":"Russ"},{"DATE":"2/11/2015","OPP":"vsMEM","OUTCOME":"W","PtF":105,"PA":89,"MIN":32,"PTS":24,"REB":9,"AST":9,"FGM":4,"FGA":15,"FGP":0.267,"m3p":1,"a3p":3,"P3P":0.333,"FTM":15,"FTA":15,"FTP":1,"BLK":0,"STL":1,"PF":1,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"2/19/2015","OPP":"vsDAL","OUTCOME":"W","PtF":104,"PA":89,"MIN":36,"PTS":34,"REB":5,"AST":10,"FGM":9,"FGA":17,"FGP":0.529,"m3p":2,"a3p":4,"P3P":0.5,"FTM":14,"FTA":14,"FTP":1,"BLK":0,"STL":2,"PF":3,"TO":6,"player":"Westbrook","first":"Russ"},{"DATE":"2/21/2015","OPP":"@ CHA","OUTCOME":"W","PtF":110,"PA":103,"MIN":35,"PTS":33,"REB":7,"AST":10,"FGM":12,"FGA":27,"FGP":0.444,"m3p":0,"a3p":3,"P3P":0,"FTM":9,"FTA":9,"FTP":1,"BLK":0,"STL":1,"PF":2,"TO":6,"player":"Westbrook","first":"Russ"},{"DATE":"2/22/2015","OPP":"vsDEN","OUTCOME":"W","PtF":119,"PA":94,"MIN":27,"PTS":21,"REB":8,"AST":17,"FGM":8,"FGA":12,"FGP":0.667,"m3p":2,"a3p":3,"P3P":0.667,"FTM":3,"FTA":5,"FTP":0.6,"BLK":0,"STL":0,"PF":1,"TO":2,"player":"Westbrook","first":"Russ"},{"DATE":"2/24/2015","OPP":"vsIND","OUTCOME":"W","PtF":105,"PA":92,"MIN":28,"PTS":20,"REB":11,"AST":10,"FGM":8,"FGA":19,"FGP":0.421,"m3p":1,"a3p":3,"P3P":0.333,"FTM":3,"FTA":3,"FTP":1,"BLK":0,"STL":1,"PF":1,"TO":5,"player":"Westbrook","first":"Russ"},{"DATE":"2/26/2015","OPP":"@ PHX","OUTCOME":"L","PtF":113,"PA":117,"MIN":43,"PTS":39,"REB":14,"AST":11,"FGM":12,"FGA":38,"FGP":0.316,"m3p":1,"a3p":10,"P3P":0.1,"FTM":14,"FTA":16,"FTP":0.875,"BLK":0,"STL":3,"PF":3,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"2/27/2015","OPP":"@ POR","OUTCOME":"L","PtF":112,"PA":115,"MIN":37,"PTS":40,"REB":13,"AST":11,"FGM":14,"FGA":32,"FGP":0.438,"m3p":0,"a3p":1,"P3P":0,"FTM":12,"FTA":14,"FTP":0.857,"BLK":0,"STL":1,"PF":1,"TO":6,"player":"Westbrook","first":"Russ"},{"DATE":"3/1/2015","OPP":"@ LAL","OUTCOME":"W","PtF":108,"PA":101,"MIN":0,"PTS":0,"REB":0,"AST":0,"FGM":0,"FGA":0,"FGP":0,"m3p":0,"a3p":0,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":0,"TO":0,"player":"Westbrook","first":"Russ"},{"DATE":"3/4/2015","OPP":"vsPHI","OUTCOME":"W","PtF":123,"PA":118,"MIN":42,"PTS":49,"REB":15,"AST":10,"FGM":16,"FGA":33,"FGP":0.485,"m3p":1,"a3p":4,"P3P":0.25,"FTM":16,"FTA":20,"FTP":0.8,"BLK":1,"STL":3,"PF":3,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"3/5/2015","OPP":"@ CHI","OUTCOME":"L","PtF":105,"PA":108,"MIN":38,"PTS":43,"REB":8,"AST":7,"FGM":14,"FGA":32,"FGP":0.438,"m3p":3,"a3p":5,"P3P":0.6,"FTM":12,"FTA":15,"FTP":0.8,"BLK":0,"STL":2,"PF":3,"TO":5,"player":"Westbrook","first":"Russ"},{"DATE":"3/8/2015","OPP":"vsTOR","OUTCOME":"W","PtF":108,"PA":104,"MIN":40,"PTS":30,"REB":11,"AST":17,"FGM":9,"FGA":21,"FGP":0.429,"m3p":1,"a3p":4,"P3P":0.25,"FTM":11,"FTA":13,"FTP":0.846,"BLK":1,"STL":4,"PF":1,"TO":9,"player":"Westbrook","first":"Russ"},{"DATE":"3/11/2015","OPP":"vsLAC","OUTCOME":"L","PtF":108,"PA":120,"MIN":36,"PTS":24,"REB":9,"AST":7,"FGM":5,"FGA":14,"FGP":0.357,"m3p":3,"a3p":6,"P3P":0.5,"FTM":11,"FTA":13,"FTP":0.846,"BLK":0,"STL":2,"PF":4,"TO":10,"player":"Westbrook","first":"Russ"},{"DATE":"3/13/2015","OPP":"vsMIN","OUTCOME":"W","PtF":113,"PA":99,"MIN":35,"PTS":29,"REB":10,"AST":12,"FGM":11,"FGA":24,"FGP":0.458,"m3p":3,"a3p":6,"P3P":0.5,"FTM":4,"FTA":4,"FTP":1,"BLK":0,"STL":2,"PF":1,"TO":8,"player":"Westbrook","first":"Russ"},{"DATE":"3/15/2015","OPP":"vsCHI","OUTCOME":"W","PtF":109,"PA":100,"MIN":41,"PTS":36,"REB":11,"AST":6,"FGM":12,"FGA":27,"FGP":0.444,"m3p":2,"a3p":6,"P3P":0.333,"FTM":10,"FTA":13,"FTP":0.769,"BLK":0,"STL":1,"PF":1,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"3/16/2015","OPP":"@ DAL","OUTCOME":"L","PtF":115,"PA":119,"MIN":36,"PTS":24,"REB":8,"AST":12,"FGM":7,"FGA":17,"FGP":0.412,"m3p":1,"a3p":4,"P3P":0.25,"FTM":9,"FTA":10,"FTP":0.9,"BLK":1,"STL":4,"PF":6,"TO":7,"player":"Westbrook","first":"Russ"},{"DATE":"3/18/2015","OPP":"vsBOS","OUTCOME":"W","PtF":122,"PA":118,"MIN":40,"PTS":36,"REB":5,"AST":10,"FGM":8,"FGA":26,"FGP":0.308,"m3p":1,"a3p":6,"P3P":0.167,"FTM":19,"FTA":22,"FTP":0.864,"BLK":0,"STL":5,"PF":5,"TO":7,"player":"Westbrook","first":"Russ"},{"DATE":"3/20/2015","OPP":"vsATL","OUTCOME":"W","PtF":123,"PA":115,"MIN":44,"PTS":36,"REB":10,"AST":14,"FGM":8,"FGA":24,"FGP":0.333,"m3p":3,"a3p":6,"P3P":0.5,"FTM":17,"FTA":17,"FTP":1,"BLK":0,"STL":1,"PF":3,"TO":6,"player":"Westbrook","first":"Russ"},{"DATE":"3/22/2015","OPP":"vsMIA","OUTCOME":"W","PtF":93,"PA":75,"MIN":34,"PTS":12,"REB":9,"AST":17,"FGM":5,"FGA":16,"FGP":0.313,"m3p":1,"a3p":4,"P3P":0.25,"FTM":1,"FTA":2,"FTP":0.5,"BLK":2,"STL":4,"PF":3,"TO":5,"player":"Westbrook","first":"Russ"},{"DATE":"3/24/2015","OPP":"vsLAL","OUTCOME":"W","PtF":127,"PA":117,"MIN":30,"PTS":27,"REB":2,"AST":11,"FGM":9,"FGA":17,"FGP":0.529,"m3p":2,"a3p":3,"P3P":0.667,"FTM":7,"FTA":8,"FTP":0.875,"BLK":1,"STL":2,"PF":3,"TO":2,"player":"Westbrook","first":"Russ"},{"DATE":"3/25/2015","OPP":"@ SA","OUTCOME":"L","PtF":91,"PA":130,"MIN":26,"PTS":16,"REB":4,"AST":7,"FGM":5,"FGA":16,"FGP":0.313,"m3p":0,"a3p":3,"P3P":0,"FTM":6,"FTA":6,"FTP":1,"BLK":0,"STL":2,"PF":1,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"3/28/2015","OPP":"@ UTAH","OUTCOME":"L","PtF":89,"PA":94,"MIN":41,"PTS":37,"REB":8,"AST":6,"FGM":12,"FGA":29,"FGP":0.414,"m3p":3,"a3p":9,"P3P":0.333,"FTM":10,"FTA":12,"FTP":0.833,"BLK":0,"STL":0,"PF":4,"TO":9,"player":"Westbrook","first":"Russ"},{"DATE":"3/29/2015","OPP":"@ PHX","OUTCOME":"W","PtF":109,"PA":97,"MIN":38,"PTS":33,"REB":9,"AST":7,"FGM":10,"FGA":29,"FGP":0.345,"m3p":1,"a3p":7,"P3P":0.143,"FTM":12,"FTA":14,"FTP":0.857,"BLK":1,"STL":3,"PF":3,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"4/1/2015","OPP":"vsDAL","OUTCOME":"L","PtF":131,"PA":135,"MIN":44,"PTS":31,"REB":11,"AST":11,"FGM":10,"FGA":32,"FGP":0.313,"m3p":2,"a3p":11,"P3P":0.182,"FTM":9,"FTA":11,"FTP":0.818,"BLK":0,"STL":1,"PF":4,"TO":6,"player":"Westbrook","first":"Russ"},{"DATE":"4/3/2015","OPP":"@ MEM","OUTCOME":"L","PtF":92,"PA":100,"MIN":38,"PTS":18,"REB":7,"AST":7,"FGM":5,"FGA":20,"FGP":0.25,"m3p":0,"a3p":3,"P3P":0,"FTM":8,"FTA":13,"FTP":0.615,"BLK":1,"STL":1,"PF":2,"TO":5,"player":"Westbrook","first":"Russ"},{"DATE":"4/5/2015","OPP":"vsHOU","OUTCOME":"L","PtF":112,"PA":115,"MIN":41,"PTS":40,"REB":11,"AST":13,"FGM":12,"FGA":29,"FGP":0.414,"m3p":4,"a3p":11,"P3P":0.364,"FTM":12,"FTA":15,"FTP":0.8,"BLK":0,"STL":0,"PF":5,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"4/7/2015","OPP":"vsSA","OUTCOME":"L","PtF":88,"PA":113,"MIN":26,"PTS":17,"REB":2,"AST":2,"FGM":7,"FGA":16,"FGP":0.438,"m3p":1,"a3p":2,"P3P":0.5,"FTM":2,"FTA":4,"FTP":0.5,"BLK":0,"STL":6,"PF":3,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"4/10/2015","OPP":"vsSAC","OUTCOME":"W","PtF":116,"PA":103,"MIN":38,"PTS":27,"REB":5,"AST":10,"FGM":11,"FGA":19,"FGP":0.579,"m3p":1,"a3p":4,"P3P":0.25,"FTM":4,"FTA":4,"FTP":1,"BLK":0,"STL":0,"PF":3,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"4/12/2015","OPP":"@IND","OUTCOME":"L","PtF":104,"PA":116,"MIN":40,"PTS":54,"REB":9,"AST":8,"FGM":21,"FGA":43,"FGP":0.488,"m3p":5,"a3p":15,"P3P":0.333,"FTM":7,"FTA":11,"FTP":0.636,"BLK":0,"STL":1,"PF":5,"TO":2,"player":"Westbrook","first":"Russ"},{"DATE":"4/13/2015","OPP":"vsPOR","OUTCOME":"W","PtF":101,"PA":90,"MIN":38,"PTS":36,"REB":11,"AST":7,"FGM":13,"FGA":27,"FGP":0.481,"m3p":2,"a3p":5,"P3P":0.4,"FTM":8,"FTA":8,"FTP":1,"BLK":0,"STL":2,"PF":5,"TO":5,"player":"Westbrook","first":"Russ"},{"DATE":"4/15/2015","OPP":"@MIN","OUTCOME":"W","PtF":138,"PA":113,"MIN":32,"PTS":37,"REB":8,"AST":7,"FGM":11,"FGA":20,"FGP":0.55,"m3p":2,"a3p":5,"P3P":0.4,"FTM":13,"FTA":17,"FTP":0.765,"BLK":0,"STL":2,"PF":2,"TO":4,"player":"Westbrook","first":"Russ"},{"DATE":"4/14/2015","OPP":"@ PHX","OUTCOME":"W","PtF":112,"PA":29,"MIN":22,"PTS":1,"REB":6,"AST":8,"FGM":0.471,"FGA":6,"FGP":0.545,"m3p":0,"a3p":0,"P3P":1,"FTM":2,"FTA":3,"FTP":1,"BLK":null,"STL":null,"PF":null,"TO":null,"player":"Paul","first":"Chris"},{"DATE":"4/13/2015","OPP":"vsDEN","OUTCOME":"W","PtF":110,"PA":103,"MIN":37,"PTS":17,"REB":7,"AST":9,"FGM":6,"FGA":11,"FGP":0.545,"m3p":1,"a3p":2,"P3P":0.5,"FTM":4,"FTA":4,"FTP":1,"BLK":0,"STL":2,"PF":2,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"4/11/2015","OPP":"vsMEM","OUTCOME":"W","PtF":94,"PA":86,"MIN":35,"PTS":15,"REB":5,"AST":14,"FGM":6,"FGA":13,"FGP":0.462,"m3p":1,"a3p":4,"P3P":0.25,"FTM":2,"FTA":2,"FTP":1,"BLK":1,"STL":1,"PF":2,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"4/7/2015","OPP":"vsLAL","OUTCOME":"W","PtF":105,"PA":100,"MIN":35,"PTS":19,"REB":5,"AST":10,"FGM":5,"FGA":11,"FGP":0.455,"m3p":0,"a3p":2,"P3P":0,"FTM":9,"FTA":9,"FTP":1,"BLK":0,"STL":2,"PF":2,"TO":0,"player":"Paul","first":"Chris"},{"DATE":"4/5/2015","OPP":"@ LAL","OUTCOME":"W","PtF":106,"PA":78,"MIN":30,"PTS":7,"REB":4,"AST":15,"FGM":3,"FGA":7,"FGP":0.429,"m3p":1,"a3p":3,"P3P":0.333,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":2,"PF":1,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"4/4/2015","OPP":"@ DEN","OUTCOME":"W","PtF":107,"PA":92,"MIN":32,"PTS":23,"REB":5,"AST":9,"FGM":6,"FGA":10,"FGP":0.6,"m3p":4,"a3p":4,"P3P":1,"FTM":7,"FTA":8,"FTP":0.875,"BLK":0,"STL":2,"PF":4,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"4/1/2015","OPP":"@ POR","OUTCOME":"W","PtF":126,"PA":122,"MIN":39,"PTS":41,"REB":5,"AST":17,"FGM":13,"FGA":21,"FGP":0.619,"m3p":5,"a3p":9,"P3P":0.556,"FTM":10,"FTA":10,"FTP":1,"BLK":0,"STL":4,"PF":2,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"3/31/2015","OPP":"vsGS","OUTCOME":"L","PtF":106,"PA":110,"MIN":39,"PTS":27,"REB":2,"AST":9,"FGM":7,"FGA":17,"FGP":0.412,"m3p":1,"a3p":6,"P3P":0.167,"FTM":12,"FTA":12,"FTP":1,"BLK":0,"STL":1,"PF":4,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"3/29/2015","OPP":"@ BOS","OUTCOME":"W","PtF":119,"PA":106,"MIN":35,"PTS":21,"REB":3,"AST":10,"FGM":5,"FGA":9,"FGP":0.556,"m3p":3,"a3p":4,"P3P":0.75,"FTM":8,"FTA":8,"FTP":1,"BLK":1,"STL":2,"PF":1,"TO":4,"player":"Paul","first":"Chris"},{"DATE":"3/27/2015","OPP":"@ PHI","OUTCOME":"W","PtF":119,"PA":98,"MIN":30,"PTS":25,"REB":3,"AST":7,"FGM":9,"FGA":15,"FGP":0.6,"m3p":3,"a3p":5,"P3P":0.6,"FTM":4,"FTA":4,"FTP":1,"BLK":0,"STL":1,"PF":3,"TO":3,"player":"Paul","first":"Chris"},{"DATE":"3/25/2015","OPP":"@ NY","OUTCOME":"W","PtF":111,"PA":80,"MIN":28,"PTS":11,"REB":3,"AST":16,"FGM":5,"FGA":8,"FGP":0.625,"m3p":1,"a3p":2,"P3P":0.5,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":4,"PF":1,"TO":5,"player":"Paul","first":"Chris"},{"DATE":"3/22/2015","OPP":"vsNO","OUTCOME":"W","PtF":107,"PA":100,"MIN":38,"PTS":23,"REB":7,"AST":11,"FGM":8,"FGA":19,"FGP":0.421,"m3p":0,"a3p":4,"P3P":0,"FTM":7,"FTA":8,"FTP":0.875,"BLK":0,"STL":4,"PF":1,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"3/20/2015","OPP":"vsWSH","OUTCOME":"W","PtF":113,"PA":99,"MIN":37,"PTS":30,"REB":6,"AST":15,"FGM":12,"FGA":21,"FGP":0.571,"m3p":1,"a3p":3,"P3P":0.333,"FTM":5,"FTA":6,"FTP":0.833,"BLK":0,"STL":1,"PF":3,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"3/18/2015","OPP":"@ SAC","OUTCOME":"W","PtF":116,"PA":105,"MIN":39,"PTS":30,"REB":3,"AST":11,"FGM":13,"FGA":20,"FGP":0.65,"m3p":4,"a3p":10,"P3P":0.4,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":1,"PF":4,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"3/17/2015","OPP":"vsCHA","OUTCOME":"W","PtF":99,"PA":92,"MIN":35,"PTS":21,"REB":3,"AST":8,"FGM":6,"FGA":16,"FGP":0.375,"m3p":4,"a3p":7,"P3P":0.571,"FTM":5,"FTA":6,"FTP":0.833,"BLK":0,"STL":2,"PF":2,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"3/15/2015","OPP":"vsHOU","OUTCOME":"L","PtF":98,"PA":100,"MIN":37,"PTS":23,"REB":3,"AST":5,"FGM":7,"FGA":21,"FGP":0.333,"m3p":1,"a3p":3,"P3P":0.333,"FTM":8,"FTA":8,"FTP":1,"BLK":0,"STL":2,"PF":4,"TO":3,"player":"Paul","first":"Chris"},{"DATE":"3/13/2015","OPP":"@ DAL","OUTCOME":"L","PtF":99,"PA":129,"MIN":33,"PTS":11,"REB":1,"AST":7,"FGM":5,"FGA":8,"FGP":0.625,"m3p":1,"a3p":2,"P3P":0.5,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":5,"PF":2,"TO":4,"player":"Paul","first":"Chris"},{"DATE":"3/11/2015","OPP":"@ OKC","OUTCOME":"W","PtF":120,"PA":108,"MIN":37,"PTS":33,"REB":4,"AST":9,"FGM":11,"FGA":19,"FGP":0.579,"m3p":5,"a3p":8,"P3P":0.625,"FTM":6,"FTA":7,"FTP":0.857,"BLK":0,"STL":2,"PF":3,"TO":5,"player":"Paul","first":"Chris"},{"DATE":"3/9/2015","OPP":"vsMIN","OUTCOME":"W","PtF":89,"PA":76,"MIN":30,"PTS":2,"REB":4,"AST":15,"FGM":1,"FGA":6,"FGP":0.167,"m3p":0,"a3p":1,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":1,"PF":3,"TO":4,"player":"Paul","first":"Chris"},{"DATE":"3/8/2015","OPP":"@ GS","OUTCOME":"L","PtF":98,"PA":106,"MIN":31,"PTS":14,"REB":5,"AST":11,"FGM":7,"FGA":17,"FGP":0.412,"m3p":0,"a3p":4,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":2,"PF":4,"TO":5,"player":"Paul","first":"Chris"},{"DATE":"3/4/2015","OPP":"vsPOR","OUTCOME":"L","PtF":93,"PA":98,"MIN":42,"PTS":36,"REB":6,"AST":12,"FGM":14,"FGA":29,"FGP":0.483,"m3p":2,"a3p":6,"P3P":0.333,"FTM":6,"FTA":6,"FTP":1,"BLK":0,"STL":1,"PF":5,"TO":5,"player":"Paul","first":"Chris"},{"DATE":"3/2/2015","OPP":"@ MIN","OUTCOME":"W","PtF":110,"PA":105,"MIN":37,"PTS":26,"REB":4,"AST":14,"FGM":12,"FGA":20,"FGP":0.6,"m3p":2,"a3p":4,"P3P":0.5,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":3,"PF":1,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"3/1/2015","OPP":"@ CHI","OUTCOME":"W","PtF":96,"PA":86,"MIN":38,"PTS":28,"REB":5,"AST":12,"FGM":12,"FGA":19,"FGP":0.632,"m3p":0,"a3p":4,"P3P":0,"FTM":4,"FTA":6,"FTP":0.667,"BLK":1,"STL":0,"PF":2,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"2/27/2015","OPP":"@ MEM","OUTCOME":"W","PtF":97,"PA":79,"MIN":40,"PTS":19,"REB":4,"AST":13,"FGM":7,"FGA":14,"FGP":0.5,"m3p":1,"a3p":4,"P3P":0.25,"FTM":4,"FTA":4,"FTP":1,"BLK":0,"STL":2,"PF":4,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"2/25/2015","OPP":"@ HOU","OUTCOME":"L","PtF":105,"PA":110,"MIN":39,"PTS":22,"REB":3,"AST":14,"FGM":8,"FGA":18,"FGP":0.444,"m3p":2,"a3p":4,"P3P":0.5,"FTM":4,"FTA":4,"FTP":1,"BLK":0,"STL":0,"PF":4,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"2/23/2015","OPP":"vsMEM","OUTCOME":"L","PtF":87,"PA":90,"MIN":39,"PTS":30,"REB":4,"AST":10,"FGM":13,"FGA":22,"FGP":0.591,"m3p":2,"a3p":3,"P3P":0.667,"FTM":2,"FTA":2,"FTP":1,"BLK":2,"STL":2,"PF":0,"TO":4,"player":"Paul","first":"Chris"},{"DATE":"2/21/2015","OPP":"vsSAC","OUTCOME":"W","PtF":126,"PA":99,"MIN":27,"PTS":10,"REB":3,"AST":9,"FGM":5,"FGA":10,"FGP":0.5,"m3p":0,"a3p":1,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":3,"TO":3,"player":"Paul","first":"Chris"},{"DATE":"2/19/2015","OPP":"vsSA","OUTCOME":"W","PtF":119,"PA":115,"MIN":40,"PTS":22,"REB":5,"AST":16,"FGM":9,"FGA":16,"FGP":0.563,"m3p":2,"a3p":4,"P3P":0.5,"FTM":2,"FTA":3,"FTP":0.667,"BLK":0,"STL":1,"PF":5,"TO":4,"player":"Paul","first":"Chris"},{"DATE":"2/11/2015","OPP":"vsHOU","OUTCOME":"W","PtF":110,"PA":95,"MIN":37,"PTS":12,"REB":6,"AST":12,"FGM":4,"FGA":14,"FGP":0.286,"m3p":1,"a3p":5,"P3P":0.2,"FTM":3,"FTA":4,"FTP":0.75,"BLK":0,"STL":2,"PF":4,"TO":5,"player":"Paul","first":"Chris"},{"DATE":"2/9/2015","OPP":"@ DAL","OUTCOME":"W","PtF":115,"PA":98,"MIN":40,"PTS":25,"REB":6,"AST":13,"FGM":7,"FGA":15,"FGP":0.467,"m3p":1,"a3p":3,"P3P":0.333,"FTM":10,"FTA":10,"FTP":1,"BLK":0,"STL":2,"PF":3,"TO":3,"player":"Paul","first":"Chris"},{"DATE":"2/8/2015","OPP":"@ OKC","OUTCOME":"L","PtF":108,"PA":131,"MIN":36,"PTS":18,"REB":6,"AST":13,"FGM":7,"FGA":13,"FGP":0.538,"m3p":0,"a3p":4,"P3P":0,"FTM":4,"FTA":4,"FTP":1,"BLK":0,"STL":3,"PF":3,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"2/6/2015","OPP":"@ TOR","OUTCOME":"L","PtF":107,"PA":123,"MIN":40,"PTS":22,"REB":6,"AST":9,"FGM":8,"FGA":16,"FGP":0.5,"m3p":2,"a3p":5,"P3P":0.4,"FTM":4,"FTA":4,"FTP":1,"BLK":0,"STL":2,"PF":2,"TO":3,"player":"Paul","first":"Chris"},{"DATE":"2/5/2015","OPP":"@ CLE","OUTCOME":"L","PtF":94,"PA":105,"MIN":27,"PTS":10,"REB":2,"AST":9,"FGM":4,"FGA":14,"FGP":0.286,"m3p":1,"a3p":4,"P3P":0.25,"FTM":1,"FTA":2,"FTP":0.5,"BLK":0,"STL":1,"PF":3,"TO":0,"player":"Paul","first":"Chris"},{"DATE":"2/2/2015","OPP":"@ BKN","OUTCOME":"L","PtF":100,"PA":102,"MIN":38,"PTS":20,"REB":8,"AST":8,"FGM":8,"FGA":18,"FGP":0.444,"m3p":2,"a3p":5,"P3P":0.4,"FTM":2,"FTA":4,"FTP":0.5,"BLK":0,"STL":3,"PF":4,"TO":3,"player":"Paul","first":"Chris"},{"DATE":"1/31/2015","OPP":"@ SA","OUTCOME":"W","PtF":105,"PA":85,"MIN":31,"PTS":20,"REB":3,"AST":6,"FGM":9,"FGA":16,"FGP":0.563,"m3p":2,"a3p":3,"P3P":0.667,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":1,"PF":3,"TO":5,"player":"Paul","first":"Chris"},{"DATE":"1/30/2015","OPP":"@ NO","OUTCOME":"L","PtF":103,"PA":108,"MIN":38,"PTS":24,"REB":8,"AST":7,"FGM":10,"FGA":20,"FGP":0.5,"m3p":3,"a3p":9,"P3P":0.333,"FTM":1,"FTA":2,"FTP":0.5,"BLK":0,"STL":3,"PF":2,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"1/28/2015","OPP":"@ UTAH","OUTCOME":"W","PtF":94,"PA":89,"MIN":35,"PTS":21,"REB":5,"AST":6,"FGM":8,"FGA":16,"FGP":0.5,"m3p":1,"a3p":6,"P3P":0.167,"FTM":4,"FTA":5,"FTP":0.8,"BLK":0,"STL":1,"PF":2,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"1/26/2015","OPP":"vsDEN","OUTCOME":"W","PtF":102,"PA":98,"MIN":35,"PTS":15,"REB":5,"AST":8,"FGM":7,"FGA":11,"FGP":0.636,"m3p":1,"a3p":3,"P3P":0.333,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":1,"PF":1,"TO":3,"player":"Paul","first":"Chris"},{"DATE":"1/25/2015","OPP":"@ PHX","OUTCOME":"W","PtF":120,"PA":100,"MIN":36,"PTS":23,"REB":8,"AST":12,"FGM":7,"FGA":11,"FGP":0.636,"m3p":3,"a3p":5,"P3P":0.6,"FTM":6,"FTA":7,"FTP":0.857,"BLK":0,"STL":2,"PF":2,"TO":4,"player":"Paul","first":"Chris"},{"DATE":"1/22/2015","OPP":"vsBKN","OUTCOME":"W","PtF":123,"PA":84,"MIN":24,"PTS":6,"REB":5,"AST":17,"FGM":2,"FGA":5,"FGP":0.4,"m3p":1,"a3p":3,"P3P":0.333,"FTM":1,"FTA":1,"FTP":1,"BLK":0,"STL":3,"PF":0,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"1/19/2015","OPP":"vsBOS","OUTCOME":"W","PtF":102,"PA":93,"MIN":34,"PTS":9,"REB":6,"AST":6,"FGM":3,"FGA":9,"FGP":0.333,"m3p":2,"a3p":2,"P3P":1,"FTM":1,"FTA":2,"FTP":0.5,"BLK":0,"STL":1,"PF":1,"TO":3,"player":"Paul","first":"Chris"},{"DATE":"1/17/2015","OPP":"@ SAC","OUTCOME":"W","PtF":117,"PA":108,"MIN":33,"PTS":12,"REB":4,"AST":9,"FGM":4,"FGA":13,"FGP":0.308,"m3p":0,"a3p":2,"P3P":0,"FTM":4,"FTA":4,"FTP":1,"BLK":0,"STL":1,"PF":1,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"1/16/2015","OPP":"vsCLE","OUTCOME":"L","PtF":121,"PA":126,"MIN":41,"PTS":15,"REB":8,"AST":14,"FGM":4,"FGA":15,"FGP":0.267,"m3p":1,"a3p":7,"P3P":0.143,"FTM":6,"FTA":7,"FTP":0.857,"BLK":0,"STL":3,"PF":3,"TO":5,"player":"Paul","first":"Chris"},{"DATE":"1/14/2015","OPP":"@ POR","OUTCOME":"W","PtF":100,"PA":94,"MIN":41,"PTS":23,"REB":6,"AST":10,"FGM":6,"FGA":17,"FGP":0.353,"m3p":1,"a3p":3,"P3P":0.333,"FTM":10,"FTA":10,"FTP":1,"BLK":0,"STL":2,"PF":4,"TO":3,"player":"Paul","first":"Chris"},{"DATE":"1/11/2015","OPP":"vsMIA","OUTCOME":"L","PtF":90,"PA":104,"MIN":36,"PTS":23,"REB":3,"AST":9,"FGM":9,"FGA":17,"FGP":0.529,"m3p":3,"a3p":8,"P3P":0.375,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":0,"PF":4,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"1/10/2015","OPP":"vsDAL","OUTCOME":"W","PtF":120,"PA":100,"MIN":30,"PTS":17,"REB":2,"AST":13,"FGM":5,"FGA":11,"FGP":0.455,"m3p":2,"a3p":4,"P3P":0.5,"FTM":5,"FTA":5,"FTP":1,"BLK":0,"STL":1,"PF":0,"TO":0,"player":"Paul","first":"Chris"},{"DATE":"1/7/2015","OPP":"vsLAL","OUTCOME":"W","PtF":114,"PA":89,"MIN":31,"PTS":24,"REB":2,"AST":11,"FGM":9,"FGA":15,"FGP":0.6,"m3p":4,"a3p":6,"P3P":0.667,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":3,"PF":2,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"1/5/2015","OPP":"vsATL","OUTCOME":"L","PtF":98,"PA":107,"MIN":33,"PTS":10,"REB":6,"AST":10,"FGM":4,"FGA":9,"FGP":0.444,"m3p":0,"a3p":2,"P3P":0,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":0,"PF":6,"TO":3,"player":"Paul","first":"Chris"},{"DATE":"1/3/2015","OPP":"vsPHI","OUTCOME":"W","PtF":127,"PA":91,"MIN":30,"PTS":24,"REB":8,"AST":12,"FGM":7,"FGA":11,"FGP":0.636,"m3p":3,"a3p":5,"P3P":0.6,"FTM":7,"FTA":7,"FTP":1,"BLK":0,"STL":2,"PF":4,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"12/31/2014","OPP":"vsNY","OUTCOME":"W","PtF":99,"PA":78,"MIN":28,"PTS":5,"REB":3,"AST":8,"FGM":2,"FGA":11,"FGP":0.182,"m3p":1,"a3p":4,"P3P":0.25,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":1,"TO":3,"player":"Paul","first":"Chris"},{"DATE":"12/29/2014","OPP":"vsUTAH","OUTCOME":"W","PtF":101,"PA":97,"MIN":36,"PTS":20,"REB":5,"AST":8,"FGM":7,"FGA":14,"FGP":0.5,"m3p":3,"a3p":5,"P3P":0.6,"FTM":3,"FTA":4,"FTP":0.75,"BLK":0,"STL":4,"PF":2,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"12/27/2014","OPP":"vsTOR","OUTCOME":"L","PtF":98,"PA":110,"MIN":34,"PTS":10,"REB":3,"AST":8,"FGM":3,"FGA":12,"FGP":0.25,"m3p":1,"a3p":3,"P3P":0.333,"FTM":3,"FTA":4,"FTP":0.75,"BLK":0,"STL":1,"PF":3,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"12/25/2014","OPP":"vsGS","OUTCOME":"W","PtF":100,"PA":86,"MIN":38,"PTS":22,"REB":7,"AST":4,"FGM":7,"FGA":18,"FGP":0.389,"m3p":1,"a3p":4,"P3P":0.25,"FTM":7,"FTA":8,"FTP":0.875,"BLK":0,"STL":3,"PF":2,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"12/23/2014","OPP":"@ ATL","OUTCOME":"L","PtF":104,"PA":107,"MIN":39,"PTS":19,"REB":6,"AST":7,"FGM":7,"FGA":18,"FGP":0.389,"m3p":3,"a3p":7,"P3P":0.429,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":5,"PF":2,"TO":3,"player":"Paul","first":"Chris"},{"DATE":"12/22/2014","OPP":"@ SA","OUTCOME":"L","PtF":118,"PA":125,"MIN":34,"PTS":25,"REB":4,"AST":9,"FGM":8,"FGA":17,"FGP":0.471,"m3p":3,"a3p":6,"P3P":0.5,"FTM":6,"FTA":7,"FTP":0.857,"BLK":2,"STL":2,"PF":1,"TO":3,"player":"Paul","first":"Chris"},{"DATE":"12/20/2014","OPP":"vsMIL","OUTCOME":"W","PtF":106,"PA":102,"MIN":36,"PTS":27,"REB":3,"AST":9,"FGM":6,"FGA":16,"FGP":0.375,"m3p":4,"a3p":6,"P3P":0.667,"FTM":11,"FTA":12,"FTP":0.917,"BLK":1,"STL":2,"PF":3,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"12/19/2014","OPP":"@ DEN","OUTCOME":"L","PtF":106,"PA":109,"MIN":39,"PTS":17,"REB":5,"AST":15,"FGM":8,"FGA":17,"FGP":0.471,"m3p":1,"a3p":4,"P3P":0.25,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":3,"PF":2,"TO":4,"player":"Paul","first":"Chris"},{"DATE":"12/17/2014","OPP":"vsIND","OUTCOME":"W","PtF":102,"PA":100,"MIN":38,"PTS":20,"REB":6,"AST":9,"FGM":7,"FGA":14,"FGP":0.5,"m3p":1,"a3p":3,"P3P":0.333,"FTM":5,"FTA":6,"FTP":0.833,"BLK":0,"STL":5,"PF":2,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"12/15/2014","OPP":"vsDET","OUTCOME":"W","PtF":113,"PA":91,"MIN":24,"PTS":11,"REB":2,"AST":8,"FGM":4,"FGA":7,"FGP":0.571,"m3p":2,"a3p":3,"P3P":0.667,"FTM":1,"FTA":1,"FTP":1,"BLK":0,"STL":0,"PF":0,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"12/13/2014","OPP":"@ MIL","OUTCOME":"L","PtF":106,"PA":111,"MIN":36,"PTS":10,"REB":5,"AST":7,"FGM":5,"FGA":15,"FGP":0.333,"m3p":0,"a3p":4,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":1,"PF":6,"TO":6,"player":"Paul","first":"Chris"},{"DATE":"12/12/2014","OPP":"@ WSH","OUTCOME":"L","PtF":96,"PA":104,"MIN":35,"PTS":19,"REB":7,"AST":6,"FGM":7,"FGA":14,"FGP":0.5,"m3p":1,"a3p":4,"P3P":0.25,"FTM":4,"FTA":4,"FTP":1,"BLK":0,"STL":0,"PF":1,"TO":6,"player":"Paul","first":"Chris"},{"DATE":"12/10/2014","OPP":"@ IND","OUTCOME":"W","PtF":103,"PA":96,"MIN":37,"PTS":17,"REB":8,"AST":15,"FGM":7,"FGA":16,"FGP":0.438,"m3p":0,"a3p":2,"P3P":0,"FTM":3,"FTA":3,"FTP":1,"BLK":0,"STL":3,"PF":4,"TO":4,"player":"Paul","first":"Chris"},{"DATE":"12/8/2014","OPP":"vsPHX","OUTCOME":"W","PtF":121,"PA":120,"MIN":43,"PTS":20,"REB":7,"AST":10,"FGM":8,"FGA":23,"FGP":0.348,"m3p":1,"a3p":9,"P3P":0.111,"FTM":3,"FTA":4,"FTP":0.75,"BLK":0,"STL":1,"PF":4,"TO":3,"player":"Paul","first":"Chris"},{"DATE":"12/6/2014","OPP":"vsNO","OUTCOME":"W","PtF":120,"PA":100,"MIN":30,"PTS":18,"REB":6,"AST":16,"FGM":7,"FGA":13,"FGP":0.538,"m3p":4,"a3p":6,"P3P":0.667,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":0,"PF":2,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"12/3/2014","OPP":"vsORL","OUTCOME":"W","PtF":114,"PA":86,"MIN":29,"PTS":19,"REB":5,"AST":10,"FGM":7,"FGA":10,"FGP":0.7,"m3p":3,"a3p":5,"P3P":0.6,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":1,"PF":0,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"12/1/2014","OPP":"vsMIN","OUTCOME":"W","PtF":127,"PA":101,"MIN":25,"PTS":12,"REB":3,"AST":8,"FGM":3,"FGA":7,"FGP":0.429,"m3p":0,"a3p":2,"P3P":0,"FTM":6,"FTA":6,"FTP":1,"BLK":0,"STL":1,"PF":2,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"11/29/2014","OPP":"@ UTAH","OUTCOME":"W","PtF":112,"PA":96,"MIN":34,"PTS":17,"REB":3,"AST":10,"FGM":7,"FGA":13,"FGP":0.538,"m3p":1,"a3p":3,"P3P":0.333,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":2,"PF":0,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"11/28/2014","OPP":"@ HOU","OUTCOME":"W","PtF":102,"PA":85,"MIN":33,"PTS":10,"REB":1,"AST":7,"FGM":3,"FGA":7,"FGP":0.429,"m3p":1,"a3p":2,"P3P":0.5,"FTM":3,"FTA":3,"FTP":1,"BLK":0,"STL":5,"PF":2,"TO":0,"player":"Paul","first":"Chris"},{"DATE":"11/26/2014","OPP":"@ DET","OUTCOME":"W","PtF":104,"PA":98,"MIN":36,"PTS":23,"REB":2,"AST":7,"FGM":9,"FGA":12,"FGP":0.75,"m3p":3,"a3p":4,"P3P":0.75,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":1,"PF":3,"TO":3,"player":"Paul","first":"Chris"},{"DATE":"11/24/2014","OPP":"@ CHA","OUTCOME":"W","PtF":113,"PA":92,"MIN":38,"PTS":22,"REB":5,"AST":15,"FGM":10,"FGA":16,"FGP":0.625,"m3p":2,"a3p":5,"P3P":0.4,"FTM":0,"FTA":0,"FTP":0,"BLK":1,"STL":0,"PF":3,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"11/23/2014","OPP":"@ MEM","OUTCOME":"L","PtF":91,"PA":107,"MIN":34,"PTS":22,"REB":5,"AST":5,"FGM":7,"FGA":13,"FGP":0.538,"m3p":1,"a3p":2,"P3P":0.5,"FTM":7,"FTA":7,"FTP":1,"BLK":0,"STL":4,"PF":3,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"11/20/2014","OPP":"@ MIA","OUTCOME":"W","PtF":110,"PA":93,"MIN":33,"PTS":26,"REB":2,"AST":12,"FGM":8,"FGA":13,"FGP":0.615,"m3p":3,"a3p":5,"P3P":0.6,"FTM":7,"FTA":8,"FTP":0.875,"BLK":0,"STL":1,"PF":0,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"11/19/2014","OPP":"@ ORL","OUTCOME":"W","PtF":114,"PA":90,"MIN":30,"PTS":16,"REB":4,"AST":9,"FGM":8,"FGA":13,"FGP":0.615,"m3p":0,"a3p":2,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":2,"PF":1,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"11/17/2014","OPP":"vsCHI","OUTCOME":"L","PtF":89,"PA":105,"MIN":37,"PTS":12,"REB":1,"AST":7,"FGM":5,"FGA":11,"FGP":0.455,"m3p":1,"a3p":1,"P3P":1,"FTM":1,"FTA":2,"FTP":0.5,"BLK":1,"STL":1,"PF":2,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"11/15/2014","OPP":"vsPHX","OUTCOME":"W","PtF":120,"PA":107,"MIN":36,"PTS":32,"REB":5,"AST":9,"FGM":10,"FGA":13,"FGP":0.769,"m3p":5,"a3p":6,"P3P":0.833,"FTM":7,"FTA":8,"FTP":0.875,"BLK":0,"STL":2,"PF":3,"TO":3,"player":"Paul","first":"Chris"},{"DATE":"11/10/2014","OPP":"vsSA","OUTCOME":"L","PtF":85,"PA":89,"MIN":39,"PTS":12,"REB":10,"AST":9,"FGM":6,"FGA":13,"FGP":0.462,"m3p":0,"a3p":3,"P3P":0,"FTM":0,"FTA":0,"FTP":0,"BLK":0,"STL":1,"PF":4,"TO":4,"player":"Paul","first":"Chris"},{"DATE":"11/8/2014","OPP":"vsPOR","OUTCOME":"W","PtF":106,"PA":102,"MIN":36,"PTS":22,"REB":4,"AST":11,"FGM":9,"FGA":18,"FGP":0.5,"m3p":0,"a3p":3,"P3P":0,"FTM":4,"FTA":4,"FTP":1,"BLK":1,"STL":1,"PF":2,"TO":0,"player":"Paul","first":"Chris"},{"DATE":"11/5/2014","OPP":"@ GS","OUTCOME":"L","PtF":104,"PA":121,"MIN":31,"PTS":15,"REB":2,"AST":12,"FGM":6,"FGA":15,"FGP":0.4,"m3p":1,"a3p":3,"P3P":0.333,"FTM":2,"FTA":2,"FTP":1,"BLK":0,"STL":4,"PF":2,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"11/3/2014","OPP":"vsUTAH","OUTCOME":"W","PtF":107,"PA":101,"MIN":36,"PTS":13,"REB":10,"AST":12,"FGM":5,"FGA":12,"FGP":0.417,"m3p":1,"a3p":2,"P3P":0.5,"FTM":2,"FTA":2,"FTP":1,"BLK":1,"STL":2,"PF":2,"TO":2,"player":"Paul","first":"Chris"},{"DATE":"11/2/2014","OPP":"vsSAC","OUTCOME":"L","PtF":92,"PA":98,"MIN":37,"PTS":16,"REB":5,"AST":11,"FGM":6,"FGA":12,"FGP":0.5,"m3p":2,"a3p":5,"P3P":0.4,"FTM":2,"FTA":2,"FTP":1,"BLK":1,"STL":4,"PF":3,"TO":0,"player":"Paul","first":"Chris"},{"DATE":"10/31/2014","OPP":"@ LAL","OUTCOME":"W","PtF":118,"PA":111,"MIN":37,"PTS":12,"REB":2,"AST":10,"FGM":2,"FGA":9,"FGP":0.222,"m3p":1,"a3p":5,"P3P":0.2,"FTM":7,"FTA":8,"FTP":0.875,"BLK":0,"STL":2,"PF":4,"TO":1,"player":"Paul","first":"Chris"},{"DATE":"10/30/2014","OPP":"vsOKC","OUTCOME":"W","PtF":93,"PA":90,"MIN":38,"PTS":22,"REB":4,"AST":7,"FGM":9,"FGA":18,"FGP":0.5,"m3p":1,"a3p":3,"P3P":0.333,"FTM":3,"FTA":7,"FTP":0.429,"BLK":1,"STL":3,"PF":3,"TO":0,"player":"Paul","first":"Chris"}];

// function makeDataz(){
//   var data = [];
//   d3.csv("csv/anthony.csv", function(error, dataz) {
//     dataz.forEach(function(element, index){
//       element.player = "Davis";
//       element.first = "Anthony"
//       var keys = ["PF","PA","MIN","FGM","FGA","FGP","m3p","a3p","P3P","FTM","FTA","FTP","REB","AST","BLK","STL","TO","PTS","PtF"];
//       for (var i = 0; i < keys.length; i++) {
//         element[keys[i]] = parseFloat(element[keys[i]]);
//       };
//       data.push(element);
//     });
//   });
//   d3.csv("csv/curry.csv", function(error, dataz) {
//     dataz.forEach(function(element, index){
//       element.player = "Curry";
//       element.first = "Stephen"
//       var keys = ["PF","PA","MIN","FGM","FGA","FGP","m3p","a3p","P3P","FTM","FTA","FTP","REB","AST","BLK","STL","TO","PTS","PtF"];
//       for (var i = 0; i < keys.length; i++) {
//         element[keys[i]] = parseFloat(element[keys[i]]);
//       };
//       data.push(element);
//     });
//   });
//   d3.csv("csv/paul.csv", function(error, dataz) {
//     dataz.forEach(function(element, index){
//       element.player = "Paul";
//       element.first = "Chris"
//       var keys = ["PF","PA","MIN","FGM","FGA","FGP","m3p","a3p","P3P","FTM","FTA","FTP","REB","AST","BLK","STL","TO","PTS","PtF"];
//       for (var i = 0; i < keys.length; i++) {
//         element[keys[i]] = parseFloat(element[keys[i]]);
//       };
//       data.push(element);
//     });
//   });
//   d3.csv("csv/harden.csv", function(error, dataz) {
//     dataz.forEach(function(element, index){
//       element.player = "Harden";
//       element.first = "James"
//       var keys = ["PF","PA","MIN","FGM","FGA","FGP","m3p","a3p","P3P","FTM","FTA","FTP","REB","AST","BLK","STL","TO","PTS","PtF"];
//       for (var i = 0; i < keys.length; i++) {
//         element[keys[i]] = parseFloat(element[keys[i]]);
//       };
//       data.push(element);
//     });
//   });
//   d3.csv("csv/lebron.csv", function(error, dataz) {
//     dataz.forEach(function(element, index){
//       element.player = "James";
//       element.first = "Lebron"
//       var keys = ["PF","PA","MIN","FGM","FGA","FGP","m3p","a3p","P3P","FTM","FTA","FTP","REB","AST","BLK","STL","TO","PTS","PtF"];
//       for (var i = 0; i < keys.length; i++) {
//         element[keys[i]] = parseFloat(element[keys[i]]);
//       };
//       data.push(element);
//     });
//   });
//   d3.csv("csv/russell.csv", function(error, dataz) {
//     dataz.forEach(function(element, index){
//       element.player = "Westbrook";
//       element.first = "Russ"
//       var keys = ["PF","PA","MIN","FGM","FGA","FGP","m3p","a3p","P3P","FTM","FTA","FTP","REB","AST","BLK","STL","TO","PTS","PtF"];
//       for (var i = 0; i < keys.length; i++) {
//         element[keys[i]] = parseFloat(element[keys[i]]);
//       };
//       data.push(element);
//     });
//   });
//   setTimeout(function(){
//     d3.select("body").append('p').text(JSON.stringify(data));
//   }, 1000);
// }
// function makeMVPS(){
//   d3.csv("csv/mvps.csv", function(error,data){

//     var keys = ["G","MP","PTS","TRB","AST","STL","BLK","FGP","3PP","FTP"];
//     aveMvp = [0,0,0,0,0,0,0,0,0,0];
//     data.forEach(function(element, index){
//       keys.forEach(function(el, ind){
//         if(element[el]){
//           aveMvp[ind] += parseFloat(element[el]);
//         }
//       });
//     });
//     for (var i = 0; i < aveMvp.length; i++) {
//       var noobn = round(aveMvp[i] / data.length, 1);
//       aveMvp[i] = noobn;
//     };
//     topReb = d3.max(data, function(m) { return parseFloat(m.TRB);} );
//     topPts = d3.max(data, function(m) { return parseFloat(m.PTS);} );
//     topAst = d3.max(data, function(m) { return parseFloat(m.AST);} );
//     topMin = d3.max(data, function(m) { return parseFloat(m.MP);} );
//     presets.push({named : "MVP Average", sets : [0,0,Math.ceil(aveMvp[1]),Math.ceil(aveMvp[2]),0,0,aveMvp[7],0,0,aveMvp[8],0,0,aveMvp[9],Math.ceil(aveMvp[3]),Math.ceil(aveMvp[4]),Math.ceil(aveMvp[6]),Math.ceil(aveMvp[5]),0,0]});
//     data.forEach(function(p){
      
//       if(parseFloat(p["TRB"]) === topReb){
//         presets.push({named : "Most Rebounds"+p.Player+" "+p.Season, sets : [0,0,0,0,0,0,0,0,0,0,0,0,0,Math.ceil(p["TRB"]),0,0,0,0,0]});
//       }
//       if(parseFloat(p["PTS"]) === topPts){
//         presets.push({named : "Most Points"+p.Player+" "+p.Season, sets : [0,0,0,Math.ceil(p["PTS"]),0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]});
//       }
//       if(parseFloat(p["AST"]) === topAst){
//         presets.push({named : "Most Assists"+p.Player+" "+p.Season, sets : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,Math.ceil(p["AST"]),0,0,0,0]});
//       }
//       if(parseFloat(p["MP"]) === topMin){
//         presets.push({named : "Most Minutes"+p.Player+" "+p.Season, sets : [0,0,Math.ceil(p["MP"]),0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]});
//       }    

//     });
//     console.log(presets);
//   });
// }
// makeDataz();
// makeMVPS();
$(function() {
    FastClick.attach(document.body);
    d3.select('.preloader').remove();
});
function round(numz,pts){
  var rndrs = Math.pow(10, pts);
  return parseInt(numz * rndrs) / rndrs;
}
function sortNumber(a,b) {
    return b - a;
}
Array.prototype.inArray = function(comparer) { 
  for(var i=0; i < this.length; i++) { 
      if(comparer(this[i])) return true; 
  }
  return false; 
};
Array.prototype.pushIfNotExist = function(element, comparer) { 
  if (!this.inArray(comparer)) {
      this.push(element);
  }
}; 
var reset = [{named : 'Reset', sets : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], grouped: "basic", descript: "Reset All Values to 0",negs:[]}];
var playersCurrent = [{player:'Lebron',total:82},{player:'Westbrook',total:82},{player:'Davis',total:82},{player:'Curry',total:82},{player:'Harden',total:82},{player:'Paul',total:82}];
var presets = [{named : 'Triple Doubles', sets : [0,0,0,10,0,0,0,0,0,0,0,0,0,10,10,0,0,0,0], grouped: "basic", descript: "Sets values for a traditional triple double. 10pts 10ast 10reb",negs:[]},
                {named : '100% FT', sets : [0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0], grouped: "basic", descript: "Sets values for a perfect free throw shooting day. 100%",negs:[]},
                {named : 'Games Played', sets : [0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], grouped: "basic", descript: "Sets values for games played in. Playing Time > 1min.",negs:[]},
                {named : 'MVP with Most Pts/Gm - Jordan 87-88', sets : [0,0,0,35,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], grouped: "MVP", descript: "Jordan's first year with Scottie Pippen may have been his greatest. He won MVP, Defensive Player of the Year, Dunk Contest, All-Star MVP, and this was all while averaging 35PPG, the most by an MVP in the past 30 years.",negs:[]},
                {named : 'MVP with Most Ast/Gm - Johnson 88-89', sets : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,13,0,0,0,0], grouped: "MVP", descript: "The year after winning his final Championship, Magic Johnson put up high numbers in every category for his second of three MVP awards. He had the second highest assists per game average of his career, and the highest among MVPs since, 13ast/gm.",negs:[]},
                {named : 'MVP with Most Reb/Gm - Garnett 03-04', sets : [0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,0,0], grouped: "MVP", descript: "Kevin Garnett's beastly performance in 2003-2004 season gained him 120 of 123 possible first place MVP votes. Almost a unanymous decision. Along with averaging 24 points and 5 assists, he also put up the largest rebound average by an MVP since 1987 with 14 reb/gm.",negs:[]},
                {named : 'MVP with Most Min/Gm - Iverson 00-01', sets : [0,0,42,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], grouped: "MVP", descript: "After the 1999-2000 Iverson said he,'I had to look in the mirror and see things I didn't do right as a person or a player.', and said after the 2000-2001 season when he looked in the mirror ,'I'll know I did everything right.' Well, Iverson sure did do everything right, winning his first and only MVP award. He is the lightest and shortest player to win MVP, which makes it even more amazing that he holds the record for highest minutes per game in an MVP season.",negs:[]},
                // {named : 'MVP Average', sets : [0,0,39,27,0,0,0.5,0,0,0.2,0,0,0.8,9,6,2,2,0,0], grouped: "MVP", descript: "THIS IT A TEST",negs:[]},
                {named : 'Curry', sets : [100,99,0,0,0,0,0,0,8,0.44,0,0,0,0,0,0,0,0,0], grouped: "Player", descript: "Curry's shooting ability is unique. His range and consistency create wins for his team. His stat line is a complex one. Team wins, scores 100, keeps opponent below 100, He shoots at least 8 3's and makes at least 44%.",negs:['PA']},
                {named : 'Davis', sets : [0,0,0,25,0,0,0,0,0,0,0,0,0.75,10,0,2,0,0,0], grouped: "Player", descript: "A ten inch growth spurt over 5 years transitioned Anthony Davis from a guard to a big man and made him one of the most versatile players in the league. A combination of points, rebounds, blocks, and a high free throw shooting percentage show off the many talents of Davis : 25 PTS, 75% FT, 10 REB, 2 BLK",negs:['TO']},
                // {named : 'Harden', sets : [0,0,36,0,0,0,0.4,0,0,0.4,0,0,0.78,0,0,0,0,0,0], grouped: "Player", descript: "James Harden is talent, hard work, and consistency personified. Hidden behind his lucious facial hair, the man quietly anchors his team game in and game out. Harden's case for MVP can be made with shooting percentages combined with high minutes played : 36 MIN, 40% FG, 40% 3P, 78% FT.",negs:[]},
                {named : 'Harden', sets : [0,0,0,30,20,0,0,0,0,0,0,0,0,0,0,0,0,0,0], grouped: "Player", descript: "James Harden is talent, hard work, and consistency personified. Hidden behind his facial hair, the man quietly anchors his team game in and game out. Harden's case for MVP can be made with his scoring efficiency and ability : 30 PTS on fewer than 20 FGA.",negs:['FGA']},
                {named : 'James', sets : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], grouped: "Player", descript: "Lebron James is the King for a reason. He does everything. There are many people with the opinion that James should be penciled in for MVP every season, and it is hard to argue. He adds things to a team that can't be measured, but we can try. His stat line is : 20 PTS, 50% FG, 5 REB, 5 AST, 1 BLK, 1 STL.",negs:[]},
                {named : 'Paul', sets : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,0,0,1,0], grouped: "Player", descript: "Chris Paul is a floor general like no other. His consistency makes him one of the best in the league at controlling the floor. The impact of his style is immeasurable, which makes him perenial sleeper for MVP. Paul's case for MVP can be seen in games with 10 AST and 1 or fewer TOs.",negs:['TO']},
                {named : 'Westbrook', sets : [0,0,0,20,0,0,0,0,0,0,0,0,0,10,10,0,0,0,0], grouped: "Player", descript: "Russell Westbrook is possibly the most atheletic and unique player in the league. He grabbed voters attention with his truly one of a kind stat lines. Westbrook's case for MVP come with his comparison of 20pts 10ast and 10reb games.",negs:[]}];
// d3.json("data.json", function(error, data) {
  var introTime = 5000;
  var introSteps = ['sliders'];
  var keys = ["PtF","PA","MIN","PTS","FGM","FGA","FGP","m3p","a3p","P3P","FTM","FTA","FTP","REB","AST","BLK","STL","TO","PF"];
  var keysLabels = ["PF","PA","MIN","PTS","FGM","FGA","FG%","3PM","3PA","3P%","FTM","FTA","FT%","REB","AST","BLK","STL","TO","Fouls"];
  var statDivs = ["game", "outcome", "mainstats", "defstats", "shootingstats"];
  var allKeys = _.keys(data[0]);
  var names = _.uniq(_.pluck(data, 'player'));
  var maxes = [];
  var indyData = [];
  var ppYS = [];
  for (var i = 0; i < names.length; i++) {
    var usage = [];
    data.forEach(function(element, index){
      if(element.player === names[i]){
        usage.push(element);
      }
    });
    indyData.push(usage);
  };
  for (var i = 0; i < keys.length; i++) {
    var maxObj = {};
    maxObj.val = d3.max(data, function(d) { return d[keys[i]];});
    maxObj.named = keys[i];
    maxObj.labeled = keysLabels[i];
    maxes.push(maxObj);
  };

  var navved = d3.select('header').append('nav');                
  var navvedPRE = navved.append('div').attr('class', 'presets-cont');
  var npHead = navvedPRE.append('h3')
                .on('click', function(){
                  $('.presets-cont').toggleClass('open');
                  if(! $('.presets-cont').hasClass('open')){
                    $(this).text('presets');
                    $('.presets').addClass('closing');
                    setTimeout(function(){
                      $('.presets').removeClass('closing');
                    }, 1000);
                  }else{
                    $(this).text('close');
                  }
                }).text('presets');
  var navPresets = navvedPRE.append('ul').attr('class', 'presets');
  
  var npbHead = navPresets.append('h4').attr('class', 'head basic')
                          .on('click', function(){
                            var sbmen = $(this).next('.sub-menu');
                            sbmen.toggleClass('open');
                            $(this).toggleClass('open');
                            $('.presets').toggleClass('sub-open')
                            if($(this).hasClass('open')){
                              $(this).text('close basics');
                            }else{
                              sbmen.addClass('closing');
                              setTimeout(function(){
                                sbmen.removeClass('closing');
                              }, 1000);
                              $(this).text('basics');
                            }
                          })
                          .text("Basics");
  var npBasic = navPresets.append('ul').attr('class', 'sub-menu basic');
  var npbList = npBasic.selectAll('li').data(jQuery.grep(presets, function(q){ return q.grouped === "basic"; })).enter().append('li');
  var npbLT =  npbList.append('div').attr('class', 'text');
  var npbLTg =  npbList.append('div').attr('class', 'toggle');
  var npbLTd =  npbList.append('div').attr('class', 'descript');

  var npmHead = navPresets.append('h4').attr('class', 'head mvp')
                          .on('click', function(){
                            var sbmen = $(this).next('.sub-menu');
                            sbmen.toggleClass('open');
                            $(this).toggleClass('open');
                            $('.presets').toggleClass('sub-open')
                            if($(this).hasClass('open')){
                              $(this).text('close mvps');
                            }else{
                              sbmen.addClass('closing');
                              setTimeout(function(){
                                sbmen.removeClass('closing');
                              }, 1000);
                              $(this).text("MVPs Since '87");
                            }
                          })
                          .text("MVPs Since '87");
  var npMVP = navPresets.append('ul').attr('class', 'sub-menu mvp');
  var npList = npMVP.selectAll('li').data(jQuery.grep(presets, function(q){ return q.grouped === "MVP"; })).enter().append('li');
  var npLT =  npList.append('div').attr('class', 'text');
  var npLTg =  npList.append('div').attr('class', 'toggle');
  var npLTd =  npList.append('div').attr('class', 'descript');

  var nppHead = navPresets.append('h4').attr('class', 'head player')
                          .on('click', function(){
                            var sbmen = $(this).next('.sub-menu');
                            sbmen.toggleClass('open');
                            $(this).toggleClass('open');
                            $('.presets').toggleClass('sub-open')
                            if($(this).hasClass('open')){
                              $(this).text('close player');
                            }else{
                              sbmen.addClass('closing');
                              setTimeout(function(){
                                sbmen.removeClass('closing');
                              }, 1000);
                              $(this).text('Player');
                            }
                          })
                          .text("Player's Case");
  var npPlayer = navPresets.append('ul').attr('class', 'sub-menu player');
  var nppList = npPlayer.selectAll('li').data(jQuery.grep(presets, function(q){ return q.grouped === "Player"; })).enter().append('li');
  var nppLT =  nppList.append('div').attr('class', 'text');
  var nppLTg =  nppList.append('div').attr('class', 'toggle');
  var nppLTd =  nppList.append('div').attr('class', 'descript');

  navPresets.selectAll('li').attr('class', function(d,i){return d.named;}).on('click', function(d,i){
                            $('.sliders li').removeClass('lessThan');
                            if($(this).hasClass('active')){
                              $('.presets li').removeClass('active');
                            }else{
                              $('.presets li').removeClass('active');
                              $(this).addClass('active');
                            }
                            if($(this).hasClass('active')){
                              for (var i = 0; i < d.negs.length; i++) {
                                d3.select('.sliders .'+d.negs[i]).classed('lessThan', true);
                              };                              
                              for (var i = 0; i < d.sets.length; i++) {
                                if(i === d.sets.length - 1){
                                  update(d.sets[i],"slider"+keys[i],false);
                                }else{
                                  update(d.sets[i],"slider"+keys[i],true);
                                }
                              };
                            }else{
                              for (var i = 0; i < d.sets.length; i++) {
                                if(i === d.sets.length - 1){
                                  update(0,"slider"+keys[i],false);
                                }else{
                                  update(0,"slider"+keys[i],true);
                                }
                              };
                            }
                          });
  navPresets.selectAll('li .descript').text(function(d,i){return d.descript;});
  navPresets.selectAll('li .text').text(function(d,i){return d.named;});
  navPresets.selectAll('li .toggle').text("details")
                          .on('click', function(d,i){
                            d3.event.stopPropagation()
                            var dett = $(this).next('.descript');
                            $(this).parent('li').toggleClass('open');
                            $(this).parent('li').parent('.sub-menu').toggleClass('sub-open');
                            dett.toggleClass('open');

                            if($(this).parent('li').hasClass('open')){
                              $(this).text('hide');
                            }else{
                              dett.addClass('closing');
                              setTimeout(function(){
                                dett.removeClass('closing');
                              }, 1000);
                              $(this).text('details');
                            }

                          });
  var ngMT = navved.append('div').attr('class', 'mobile-toggle')
                        .on('click',function(){
                          $('.sliders').slideToggle();
                          $(this).text($(this).text() == "close menu" ? "open menu" : "open menu");
                        })
                        .text('close menu');
  var navGroup = navved.append('ul').attr('class', 'sliders ').selectAll('li').data(maxes).enter().append('li');
  var reseter = navved.selectAll('.reset').data(reset).enter().append('div').attr('class', 'reset')
                    .on('click', function(d,i){
                        $('nav .outcome').attr('class', 'outcome both').text('All Games');
                        d3.selectAll('.sliders li').classed('lessThan', false);
                        for (var i = d.sets.length - 1; i >= 0; i--) {
                          if(i === 0){
                            update(d.sets[i],"slider"+keys[i],false);
                          }else{
                            update(d.sets[i],"slider"+keys[i],true);
                          }
                        };
                    }).text('reset');
  var winner = navved.selectAll('.outcome').data(reset).enter().append('div').attr('class', 'outcome both')
                    .on('click', function(d,i){
                      var curOUT = $(this).attr('class').replace('outcome ', '');
                      var idz = d3.select('.svgc-label').text();
                      if(curOUT === 'both'){
                        $(this).attr('class', 'outcome W').text('Wins');
                        curOUT = 'W';
                      }else if(curOUT === 'W'){
                        $(this).attr('class', 'outcome L').text('Losses');
                        curOUT = 'L';
                      }else{
                        $(this).attr('class', 'outcome both').text('All Games');
                        curOUT = 'both';
                      }
                      indyData.forEach(function(element, index){
                        var PGC = d3.select('div.'+element[0].player);
                        PGC.selectAll('.svgc-label').text(keysLabels[keys.indexOf(idz)]);
                        PGC.select('svg.player-graph').selectAll('circle')
                            .on('mouseleave', function(q,w){
                              d3.select(this).transition().attr('r', 1).attr('stroke-width', 1.2);
                            }).transition().duration(1000).attr('r', 1).attr('cy', 0);
                        var playerGroup = PGC.select('svg.player-graph').selectAll('circle');
                        var psmargin = {top: 20, right: 20, bottom: 30, left: 40},
                            pswidth = 960 - psmargin.left - psmargin.right,
                            psheight = 400 - psmargin.top - psmargin.bottom;
                        var hMAX = d3.max(element, function(w,q){ return parseFloat(w[idz]); });
                        var minMAX = d3.max(element, function(w,q){ return parseFloat(w.MIN); });
                        var psxScale = d3.scale.linear().domain([-2,element.length + 1]).range([0,pswidth]);
                        var psyScale = d3.scale.linear().domain([-(hMAX * 10)/100,(hMAX * 110)/100]).range([400, 0]);
                        var playerAveCont = d3.selectAll('.'+element[0].player+' .aves h4');
                        var newyAxis = d3.svg.axis().scale(psyScale)
                                        .tickValues([round(parseFloat(hMAX/4),1),round(parseFloat(hMAX/2),1),round(parseFloat(hMAX*0.75),1),hMAX])
                                        .orient('left').tickSize(-940);
                        d3.select('.'+element[0].player+' .player-graph .y.axis').transition().duration(1000).call(newyAxis);
                        if(curOUT !== 'both'){
                          var pgFIL = playerGroup;
                          var setupObj = element.slice(0);
                          var slicers = [];
                          pgFIL.filter(function(d,poop){
                                  if( d.OUTCOME !== curOUT ){
                                    slicers.pushIfNotExist(poop, function(e){
                                      return e === poop;
                                    });
                                  }
                                  return d.OUTCOME === curOUT;
                                })
                              .on('mouseleave', function(q,w){
                                d3.select(this).transition().attr('r', 1 + (q.MIN / minMAX) * 7).attr('stroke-width', 1.2);
                              }).transition().duration(1000)
                              .attr('cy', function(q,w){return psyScale(q[idz]);})
                              .attr('r', function(q,w){
                                return 1 + (q.MIN / minMAX) * 7;
                              });
                          slicers.sort(sortNumber);
                          for (var i = 0; i < slicers.length; i++) {
                            setupObj.splice(slicers[i],1); 
                          };      
                          playerAveCont.text(function(d){
                            var moo = 0;
                            setupObj.forEach(function(m,j){
                              moo += m[d];
                            });
                            if(moo === 0){
                              return " - ";
                            }else{
                              if(d === "FGP" || d === "P3P" || d === "FTP"){
                                return round(moo / setupObj.length, 2);
                              }else{
                                return round(moo / setupObj.length, 1);
                              }
                            }
                          });
                          PGC.select('h4').text(element.length - slicers.length); 
                        }else{
                          playerGroup.on('mouseleave', function(q,w){
                            d3.select(this).transition().attr('r', 1 + (q.MIN / minMAX) * 7).attr('stroke-width', 1.2);
                          }).transition().duration(1000)
                          .attr('cy', function(q,w){return psyScale(q[idz]);})
                          .attr('r', function(q,w){
                            return 1 + (q.MIN / minMAX) * 7;
                          });
                          playerAveCont.text(function(d){
                            var moo = 0;
                            element.forEach(function(m,j){
                              moo += m[d];
                            });
                            if(moo === 0){
                              return " - ";
                            }else{
                              if(d === "FGP" || d === "P3P" || d === "FTP"){
                                return round(moo / element.length, 2);
                              }else{
                                return round(moo / element.length, 1);
                              }
                            }
                          });        
                          PGC.select('h4').text("82"); 
                        }
                      });

                      var actARR = $('.sliders .active input');
                      if(actARR.length > 0){
                        for (var i = 0; i < actARR.length; i++) {
                            update($(actARR[i]).val(), $(actARR[i]).attr('id'));
                        };
                      }else{
                        update(0, 'sliderPTS');
                      }
                    }).text('All Games');      

  navGroup.attr('class', function(d,i){return d.named;}).on('click', function(){ updateStat(this); }, false);
  navGroup.append('span').text(function(d){return d.labeled;});
  var lbl = navGroup.append('label').attr('id', function(d){return 'slider' + d.named + 'label';}).attr('for', function(d){return 'slider' + d.named;});
  lbl.text(0);
  var inpt = navGroup.append('input').attr('type', 'range').attr('orient', '270deg').attr('class', 'range')
          .attr('id', function(d){return 'slider' + d.named;}).attr('min', 0).attr('max', function(d){return d.val;})
          .attr('step', function(d){if(d.named === 'FGP' || d.named === 'P3P' || d.named === 'FTP'){return 0.01;}else{return 1;}})
          .attr('value', 0);
  inpt.on('click',  function() { d3.event.stopPropagation() }).on('input', function(){ circtip.hide(); update(+this.value, this.id); });
  var circtip = d3.tip()
    .attr('class', 'd3-tip')
    .offset([-10, 0])
    .html(function(d) {
      var team = d.OPP;
      var teamVS = team.indexOf('vs');
      var teamAT = team.indexOf('@');
      if(teamVS > -1){
        team = team.slice(0, teamVS) + team.slice(teamVS+2);
      }else{
        team = team.slice(0, teamAT) + team.slice(teamAT+1);
      }
      return "<div class='tool-tip circ team"+team.replace(/ /g, '')+"'><span>" + d.OPP + "</span><span>" + d.DATE + "</span></div>";
    });  
  init();
  function init(){
    indyData.forEach(function(element, index){
      var allStar, setupObj, pgFIL;
      var theCURpl = element[0].player;
      var PGC = d3.select('article').append('div').attr('class', theCURpl + " player");
      var PL = PGC.append('div').attr('class', 'left');
      var PR = PGC.append('div').attr('class', 'right');
      var PLhead = PL.append('div').attr('class', 'player-card')
      var plh3 = PLhead.append('h3');
      plh3.append('span').attr('class','first').text(element[0].first);
      plh3.append('span').attr('class','last').text(theCURpl);
      var p4 = PLhead.append('h4').text(theCURpl);
      var dd = PL.append('div').attr('class', 'slideCTRL')
                  .on('click', function(){ 
                    $(this).closest('.player').toggleClass('open');
                    $(this).parent('.left').next('.right').find('.svg-cont').slideToggle();
                    $(this).next('.game-props').slideToggle();
                    circtip.hide();
                  });
      var playerAves = PR.append('ul').attr('class', 'aves ').selectAll('li').data(keys).enter().append('li').attr('class', function(d,i){return d;});
      playerAves.append('h2').text(function(d,i){return keysLabels[i];});
      var playerAveCont = playerAves.append('h4');
      var usedivs = [];
      var gmbb = PL.append('aside').attr('class', 'game-props');

      getAllStar();
      makeGameInd();
      makePlayerDots();
      makePlayerAves();
      makePlayerBar();
      function getAllStar(){
        if(theCURpl === 'James'){
          allStar = 55 - 0.35;
        }else if(theCURpl === 'Paul'){
          allStar = 54 - 0.35;
        }else if(theCURpl === 'Westbrook'){
          allStar = 54 - 0.35;
        }else if(theCURpl === 'Harden'){
          allStar = 53 - 0.35;
        }else if(theCURpl === 'Davis'){
          allStar = 53 - 0.35;
        }else if(theCURpl === 'Curry'){
          allStar = 51 - 0.35;
        }
      }
      function makeGameInd(){
        for (var i = 0; i < statDivs.length; i++) {
          var usediv = gmbb.append('div').attr('class', statDivs[i]);
          usedivs.push(usediv);
        };     
        for (var i = 0; i < allKeys.length; i++) {
          var q;
          if(i <= 1){
            q = 0;
          }else if(i <= 4){
            q = 1;
          }else if(i <= 8){
            q = 2;          
          }else if(i <= 17){
            q = 4;
          }else{
            q = 3;
          }
          var greif = usedivs[q].append('div').attr('class', allKeys[i]);
          greif.append('h6').text(allKeys[i]);
          greif.append('span');
        };
        gmbb.insert("div",".shootingstats").attr('class', 'head shooting')
            .on('click', function(){ 
              $(this).next('.shootingstats').slideToggle(); 
              $(this).siblings('.sg-svg').slideToggle(); 
              $(this).toggleClass('open'); 
            })
            .text('shooting stats');
        gmbb.select('.shootingstats').insert("div",".FGM").attr('class', 'head').text('field goal stats');
        gmbb.select('.shootingstats').insert("div",".m3p").attr('class', 'head').text('three point stats');
        gmbb.select('.shootingstats').insert("div",".FTM").attr('class', 'head').text('free throw stats');
      }
      function makePlayerDots(){
        var psmargin = {top: 20, right: 20, bottom: 30, left: 40},
            pswidth = 1000 - psmargin.left - psmargin.right,
            psheight = 400 - psmargin.top - psmargin.bottom;
        var svgCont = PR.append('div').attr('class','svg-cont');
        svgCont.append('h2').attr('class', 'svgc-label').text('PTS');                
        var playerSVG = svgCont.append('div').attr('class','player-graph-cont').append('svg')
                          .attr('class', 'player-graph')
                          .attr("width", "100%")
                          .attr("height", "100%")
                          .attr("viewBox", "0 0 "+(pswidth + psmargin.left + psmargin.right)+" "+(psheight + psmargin.top + psmargin.bottom))
                          .attr("preserveAspectRatio", "xMinYMin meet")
                          .append("g")
                          .attr("transform", "translate(" + psmargin.left + "," + psmargin.top + ")");  
        svgCont.append('h2').attr('class', 'svgc-label').text('PTS');                
        playerSVG.call(circtip);

        var hMAX = d3.max(element, function(w,q){ return parseFloat(w.PTS); });
        var minMAX = d3.max(element, function(w,q){ return parseFloat(w.MIN); });
        var psxScale = d3.scale.linear().domain([-2,element.length + 1]).range([0,pswidth]);
        var psyScale = d3.scale.linear().domain([-5,hMAX * 1.1]).range([psheight, 0]);
        var psyAxis = d3.svg.axis().scale(psyScale)
                .orient("left")
                .tickValues([parseInt(hMAX/4),parseInt(hMAX/2),parseInt(hMAX*0.75),hMAX])
                .tickSize(-pswidth);
        var psxAxis = d3.svg.axis().scale(psxScale).tickValues([allStar]).orient("bottom").tickSize(-psheight / 1.1,0,0).tickFormat("All-star Break");
        var psxAxisGroup = playerSVG.append("g")
                .attr("class", "x axis grid")
                .attr("transform", "translate(0,"+(psheight / 1.1)+")")
                .call(psxAxis);
        var psyAxisGroup = playerSVG.append("g")
                .attr("class", "y axis grid")
                .call(psyAxis);
        psyAxisGroup.selectAll(".domain").style("opacity", "0");
        var playerGroup = playerSVG.selectAll('circle').data(element).enter().append('circle')
                                    .on('click', function(q,w){
                                      d3.select('.'+theCURpl+' .player-graph .active').classed('active', false).on('mouseleave', function(q,w){
                                      d3.select(this).transition().attr('r', 1 + (q.MIN / minMAX) * 7).attr('stroke-width', 1.2);
                                    }).transition().attr('r', 1 + (q.MIN / minMAX) * 7).attr('stroke-width', 1.2);
                                      d3.select(this).classed('active', true).on('mouseleave', function(q,w){
                                      d3.select(this).transition().attr('r', 15).attr('stroke-width', 5);
                                    }).transition().attr('r', 15).attr('stroke-width', 5);

                                      var purd = d3.select('.'+theCURpl+' .avers .active');
                                      var avev = d3.select('.'+theCURpl+' .avvl').text();
                                      if(purd[0][0]){
                                        avev = purd.attr('valz');  
                                      }
                                      var curv = " -- ";
                                      for (var i = allKeys.length - 1; i >= 0; i--) {
                                        var thecurd;
                                        var result = $.grep(ppYS, function(e){ return e.player == theCURpl; });
                                        var turd = d3.select('.'+q.player+' .updaters .pb'+allKeys[i]);
                                        if(turd[0][0]){ 
                                          if(turd.classed("active")){
                                             curv = q[allKeys[i]];
                                           }
                                         }
                                        d3.select('.'+q.player+' .sg-graph .updaters .pb'+allKeys[i])
                                                          .transition()
                                                          .duration(750)
                                                          .attr('y1', function(y){
                                                            return result[0].scales[y](parseFloat(q[allKeys[i]]));
                                                          })
                                                          .attr('y2', function(y){
                                                            return result[0].scales[y](parseFloat(q[allKeys[i]]));
                                                          })
                                                          .attr('valz', parseFloat(q[allKeys[i]])); 
                                        if(allKeys[i] === "FGP" || allKeys[i] === "P3P" || allKeys[i] === "FTP"){
                                          thecurd = d3.select('.'+q.player+' .game-props .'+allKeys[i]+' span').text(parseInt(q[allKeys[i]] * 100) + "%");
                                        }else{
                                          thecurd = d3.select('.'+q.player+' .game-props .'+allKeys[i]+' span').text(q[allKeys[i]]);
                                        }
                                        if(allKeys[i] === "OUTCOME"){
                                          thecurd.attr('class', q[allKeys[i]]);
                                        }else if(allKeys[i] === "OPP"){
                                          thecurd.attr('class', function(){
                                            var team = q[allKeys[i]];
                                            var teamVS = team.indexOf('vs');
                                            var teamAT = team.indexOf('@');
                                            if(teamVS > -1){
                                              team = team.slice(0, teamVS) + team.slice(teamVS+2);
                                            }else{
                                              team = team.slice(0, teamAT) + team.slice(teamAT+1);
                                            }
                                            return "team"+team.replace(/ /g, '');
                                          });
                                        }
                                      };
                                      d3.select('.'+theCURpl+' .crvl').text(curv);
                                      d3.select('.'+theCURpl+' .avvl').text(avev);                                      
                                    })
                                    .on('mouseenter', function(q,w){
                                      d3.select(this).transition().attr('r', 15).attr('stroke-width', 5);
                                      circtip.show(q);
                                    })
                                    .on('mouseleave', function(q,w){
                                      d3.select(this).transition().attr('r', 1 + (q.MIN / minMAX) * 7).attr('stroke-width', 1.2);
                                      circtip.hide(q);
                                    });

        pgFIL = playerGroup;
        setupObj = element.slice(0);
        for (var i = 0; i < keys.length; i++) {
          var user = keys[i];
          pgFIL = pgFIL.filter(function(d,j){
                      if( $('#slider'+user).parent('li').hasClass('lessThan') ){
                        if( d[user] > parseFloat($('#slider'+user).val()) ){
                          setupObj.splice(j,1);
                        }
                        return d[user] <= $('#slider'+user).val();
                      }else{
                        if( d[user] < parseFloat($('#slider'+user).val()) ){
                          setupObj.splice(j,1);
                        }
                        return d[user] >= $('#slider'+user).val();
                      }
                    });
        };
        pgFIL.attr('class', function(d,i){
          var team = d.OPP;
          var teamVS = team.indexOf('vs');
          var teamAT = team.indexOf('@');
          if(teamVS > -1){
            team = team.slice(0, teamVS) + team.slice(teamVS+2);
          }else{
            team = team.slice(0, teamAT) + team.slice(teamAT+1);
          }
          if( d.OPP.indexOf('@') > -1 ){return 'away team' + team.replace(/ /g, '');}else{return 'vis team' + team.replace(/ /g, '');}})
          .attr('cy', function(q,w){return psyScale(q.PTS);})
          .attr('cx', function(q,w){return psxScale(w) + 1.5;})
          .attr('r', function(q,w){
            return 1 + (q.MIN / minMAX) * 7;
          })
          .attr('stroke-width', 1.2);        
      }
      function makePlayerAves(){
        playerAveCont.text(function(d){
          var thetext;
          var moo = 0;
          setupObj.forEach(function(m,j){
            moo += m[d];
          });
          var smerg = PGC.select('.game-props .'+d+' span');
          if(d === "FGP" || d === "P3P" || d === "FTP"){
            thetext = round(moo / setupObj.length, 2);
          }else{
            thetext = round(moo / setupObj.length, 1);
          }
          smerg.text(thetext);
          return thetext;
        });
        p4.text(pgFIL[0].length);        
      }
      function makePlayerBar(){
        var sgmargin = {top: 0, right: 20, bottom: 0, left: 40},
            sgwidth = 1000 - sgmargin.left - sgmargin.right,
            sgheight = 450 - sgmargin.top - sgmargin.bottom;
        var sgCONT = PL.select('.game-props').append('div').attr('class','sg-svg');
        var sgSVG = sgCONT.append('div').attr('class','sg-graph-cont').append('svg')
                          .attr('class', 'sg-graph')
                          .attr("width", "100%")
                          .attr("height", "100%")
                          .attr("viewBox", "0 0 "+(sgwidth + sgmargin.left + sgmargin.right)+" "+(sgheight + sgmargin.top + sgmargin.bottom))
                          .attr("preserveAspectRatio", "xMinYMin meet")
                          .append("g")
                          .attr("transform", "translate(" + sgmargin.left + "," + sgmargin.top + ")");

        var cstatLabel = sgCONT.append('div').attr('class','statlabels');
        var bartip = d3.tip()
          .attr('class', 'd3-tip')
          .offset([-10, 0])
          .html(function(d) {
            return "<div class='tool-tip bar'><span>" + d + "</span><span>" + $(this).attr('valz') + "</span></div>";
          });
        var linetip = d3.tip()
          .attr('class', 'd3-tip')
          .offset([-10, 0])
          .html(function(d) {
            return "<div class='tool-tip line'><span>" + d + "</span><span>" + $(this).attr('valz') + "</span></div>";
          });                  
        sgSVG.call(bartip).call(linetip);

        var sgxScale = d3.scale.linear().domain([0,keys.length]).range([0,sgwidth]);
        var allMaxes = {};
        var sgyScale = {};
        for (var b = 0; b < keys.length; b++) {
          var themax = d3.max(element, function(t,r){ return parseFloat(t[keys[b]]); });
          sgyScale[keys[b]] = d3.scale.linear().domain([0,(themax * 105)/100]).range([sgheight, 0]);
          allMaxes[keys[b]] = themax;
        };
        ppYS.push({ player : theCURpl , scales : sgyScale})
        var sgxAxis = d3.svg.axis().scale(sgxScale).orient("bottom");
        var sgxAxisGroup = sgSVG.append("g")
                .attr("class", "x axis grid")
                .attr("transform", "translate(0,"+sgheight+")")
                .call(sgxAxis);
        var sgGroup = sgSVG.append('g').attr('class', 'avers').selectAll('rect').data(keys).enter().append('rect')
                            .attr('class', function(q,w){return "pb"+q;})
                            .attr('width', function(){return sgxScale(0.9);})
                            .attr('valz', function(q,w){
                              return parseFloat($('.'+theCURpl+' .right .aves .'+q+' h4').text());
                            })
                            .attr('x', function(q,w){return sgxScale(0.05 + w);})
                            .attr('y', function(q,w){
                              return sgyScale[q](parseFloat($('.'+theCURpl+' .right .aves .'+q+' h4').text()));
                            })
                            .attr('height', function(q,w){
                              return sgheight - sgyScale[q](parseFloat($('.'+theCURpl+' .right .aves .'+q+' h4').text()));
                            })
                            .attr('valz', function(q,w){
                              return parseFloat($('.'+theCURpl+' .right .aves .'+q+' h4').text());
                            })
                            .on('mouseover', bartip.show)
                            .on('mouseleave', bartip.hide)
                            .on('click', function(q,w){
                              d3.select('.'+theCURpl+' .avlb').text(q);
                              d3.select('.'+theCURpl+' .avvl').text($(this).attr('valz'));
                              d3.select('.'+theCURpl+' .crvl').text($('.'+theCURpl+' .updaters .pb'+q).attr('valz'));
                              d3.selectAll('.sg-graph .active').classed('active', false);
                              d3.select(this).classed('active', true);
                              d3.select('.'+theCURpl+' .updaters .pb'+q).classed('active', true);
                            });        
        var sgGroup = sgSVG.append('g').attr('class', 'updaters').selectAll('line').data(keys).enter().append('line')
                            .attr('class', function(q,w){return "pb"+q;})
                            .attr('valz', function(q,w){
                              return parseFloat($('.'+theCURpl+' .right .aves .'+q+' h4').text());
                            })
                            .attr('x2', function(q,w){return sgxScale(0.95 + w);})
                            .attr('x1', function(q,w){return sgxScale(0.05 + w);})
                            .attr('y1', function(y){
                              return sgheight + 10;
                            })
                            .attr('y2', function(y){
                              return sgheight + 10;
                            })
                            .attr('valz', ' -- ')
                            .on('mouseover', bartip.show)
                            .on('mouseleave', bartip.hide)
                            .on('click', function(q,w){
                              d3.select('.'+theCURpl+' .avlb').text(q);
                              d3.select('.'+theCURpl+' .crvl').text($(this).attr('valz'));
                              d3.select('.'+theCURpl+' .avvl').text($('.'+theCURpl+' .avers .pb'+q).attr('valz'));
                              d3.selectAll('.sg-graph .active').classed('active', false);
                              d3.select(this).classed('active', true);
                              d3.select('.'+theCURpl+' .avers .pb'+q).classed('active', true);                              
                            });           

        cstatLabel.append('div').attr('class','avlb').text("PTS");
        cstatLabel.append('div').attr('class','avvl').text($('.'+theCURpl+' .avers .pbPTS').attr('valz'));
        cstatLabel.append('div').attr('class','crvl').text(" -- ");
        d3.selectAll('.'+theCURpl+' .sg-svg .pbPTS').classed('active', true);
      }
    });  
    if (document.cookie.replace(/(?:(?:^|.*;\s*)intro\s*\=\s*([^;]*).*$)|^.*$/, "$1") !== "true") {
      askIntro();
      document.cookie = "intro=true";
    }else{
    	returnIntro();
    }
    function askIntro(){
    	$('body').addClass('ask');
    	$('.yes').click(function(){
    		$('body').removeClass('ask');
      		runIntro();
    	});
    	$('.no').click(function(){
    		$('body').removeClass('ask');
      		returnIntro();
    	});    
    }
    function returnIntro(){
    	$('body').removeClass('fades').addClass('intrue'); 
	      setTimeout(function(){
	        $('body').removeClass('intrue'); 
	      }, 5000);
    }
    function runIntro(){
      setTimeout(function(){
        $('body').removeClass('fades');
      }, 2500);
      d3.select('body').append('img').attr('class', 'circled').attr('src', 'img/circled.png')
      var introSEL = ['',
                      '.sliders',
                      '.aves',
                      '.svg-cont',
                      '.game-props',
                      'nav',
                      '',
                      '',
                      '.sliders .PTS',
                      'butt',
                      '.presets',
                      '.social.buttons',
                      '',
                      '',
                      'body'];
      var dormants = ['.svg-cont','.game-props','.sliders','.aves'];
      var introTEXT = ['',
                        'Set stat filters, get updated averages and game totals.',
                        'View averages of those games.',
                        'View single game statistics.',
                        'Compare single games to season averages.',
                        'View wins',
                        'View losses',
                        'View All Games',
                        'less than',
                        'greater than',
                        'Use Presets to automatically Filter games.',
                        'Share your custom results',
                        'Build your case for the best regular season',
                        'Build your case for the best regular season'];
      var introTimers = [10,20,10,10,10,10,10,10,10,15,15,15,10,20];
      var tos = [];
      tosCur = 0;
      var itprev = [];
      var itt = 0;
      $.each(introTimers,function() {
          itt += this;
          var turd = itt;
          itprev.push(turd);
      });
      console.log(itprev);
      var offSet = 500;
      var toPTS = 40;
      var timeScale = 185;
      var tmr;
      var curTimed = 0;
      for (var i = 0; i < introTEXT.length; i++) {
        (function(index){
          if(index > 0){
            setTimeout(function(){
              d3.select('.overlay .cont').text(introTEXT[index]);
            }, ((introTimers[index] + curTimed) * timeScale));            
          }

          setTimeout(function(){
            if(introSEL[index] && introSEL[index] !== ''){
              d3.select(introSEL[index]).classed({'intro dormant' : false, 'rel' : true});
              if(index > 0 && introSEL[index-1] !== ''){
                var ddd = d3.select(introSEL[index-1])
                ddd.classed({'rel' : false});
                ddd.select('img.circled').remove();
              }
              if(introSEL[index] === '.sliders .PTS'){
                d3.select('nav > img.circled').remove();
              }

              if(introSEL[index] === '.aves'){
                d3.select('.sliders .PTS').classed({'turnt' : false, 'hidden' : true});
              }
               
              if(introSEL[index] === '.sliders .PTS'){
                d3.select(introSEL[index]).classed('lessThan' , true);
                update(parseInt(toPTS * (itprev[index]/itt)),"sliderPTS",false);
                cto(3);
              }
              if(introSEL[index] === 'butt'){
                d3.select('.sliders .PTS').classed('lessThan' , false);
                update(parseInt(toPTS * (itprev[index]/itt)),"sliderPTS",false);
                cto(3);
              }

              if(introSEL[index] === 'nav'){
                d3.select('nav .outcome.both').classed({'intro dormant both' : false, 'W' : true}).text('WINS');
                update(parseInt(toPTS * (itprev[index]/itt)),"sliderPTS",false);
                cto(3);
                setTimeout(function(){
                  d3.select('nav .outcome.W').classed({'W' : false, 'L' : true}).text('LOSSES');
                  update(parseInt(toPTS * ((itprev[index] + (1000/timeScale))/itt)),"sliderPTS",false);
                  cto(3);
                }, 10 * timeScale);
                setTimeout(function(){
                  d3.select('nav .outcome.L').classed({'L' : false, 'both' : true}).text('ALL GAMES');
                  update(parseInt(toPTS * ((itprev[index] + (2000/timeScale))/itt)),"sliderPTS",false);
                  cto(3);
                }, 20 * timeScale);
              }

              if( introSEL[index] === '.svg-cont' || introSEL[index] === '.presets' || introSEL[index] === '.game-props' ){
              	setTimeout(function(){
		            $('.circled').css('height', $(introSEL[index]).innerHeight()).css('width', $(introSEL[index]).innerWidth()).css('left', $(introSEL[index]).offset().left).css('top', $(introSEL[index]).offset().top);
              	}, 500)
              }else{
	            $('.circled').css('height', $(introSEL[index]).innerHeight()).css('width', $(introSEL[index]).innerWidth()).css('left', $(introSEL[index]).offset().left).css('top', $(introSEL[index]).offset().top);
              }
            }

          }, ((introTimers[index] + curTimed) * timeScale));

        })(i);
        curTimed += introTimers[i];
      }; 
      tmr = parseInt(toPTS / (curTimed / 10));   
      for (var i = 0; i < introSEL.length; i++) {
        if(introSEL[i] && introSEL[i] !== ''){
          d3.select(introSEL[i]).classed('intro', true);
        }
      };
      for (var i = 0; i < dormants.length; i++) {
        if(dormants[i] && dormants[i] !== ''){
          d3.select(dormants[i]).classed('dormant', true);
        }
      };      
      setTimeout(function(){
        $('body').removeClass('intro');              
        $('body').addClass('done');   
        d3.select('.rel').classed('rel', false);
        d3.select('.circled').remove();
        setTimeout(function(){
          $('body').removeClass('done');              
        }, 2000);
      }, curTimed * timeScale);

      for (var i = 0; i <= toPTS; i+= tmr) {
        (function(index){
          var curd = setTimeout(function(){
            update(index,"sliderPTS",false);
            tosCUR = i / 2;
          }, ((curTimed * timeScale) * (index / toPTS )) + offSet);
          tos.push(curd);
        })(i);
      };

      function cto(len){
        for (var i = 0; i <len; i++) {
          clearTimeout(tos[i]);
        };
      }
    }      
  }
  function updateStat(el){
    $(el).toggleClass('lessThan');
    updatePlayers($(el).find('label').attr('id').replace('slider', '').replace('label', ''));
  }
  function updatePlayers(idz){
    indyData.forEach(function(element, index){
      var curOUT = $('nav .outcome').attr('class').replace('outcome ', '');
      var PGC = d3.select('div.'+element[0].player);
      PGC.selectAll('.svgc-label').text(keysLabels[keys.indexOf(idz)]);
      PGC.select('svg.player-graph').selectAll('circle')
          .on('mouseleave', function(q,w){
            d3.select(this).transition().attr('r', 1).attr('stroke-width', 1.2);
          }).transition().duration(1000).attr('r', 1).attr('cy', 0);
      var playerGroup = PGC.select('svg.player-graph').selectAll('circle');
      var psmargin = {top: 20, right: 20, bottom: 30, left: 40},
          pswidth = 960 - psmargin.left - psmargin.right,
          psheight = 400 - psmargin.top - psmargin.bottom;
      var hMAX = d3.max(element, function(w,q){ return parseFloat(w[idz]); });
      var minMAX = d3.max(element, function(w,q){ return parseFloat(w.MIN); });
      var psxScale = d3.scale.linear().domain([-2,element.length + 1]).range([0,pswidth]);
      var psyScale = d3.scale.linear().domain([-(hMAX * 10)/100,(hMAX * 110)/100]).range([400, 0]);
      var playerAveCont = d3.selectAll('.'+element[0].player+' .aves h4');
      var setupObj = element.slice(0);
      var slicers = [];
      var usedivs = [];      
      var pgFIL = playerGroup.filter(function(d,poop){
                                  d.ogPOOP = poop;
                                  if(curOUT !== 'both'){
                                    if( d.OUTCOME !== curOUT ){
                                      slicers.pushIfNotExist(d.ogPOOP, function(e){
                                        return e === d.ogPOOP;
                                      });
                                    }                                    
                                    return d.OUTCOME === curOUT;
                                  }else{
                                    return d;
                                  }
                                });
      var newyAxis = d3.svg.axis().scale(psyScale)
                      .tickValues([round(parseFloat(hMAX/4),1),round(parseFloat(hMAX/2),1),round(parseFloat(hMAX*0.75),1),hMAX])
                      .orient('left').tickSize(-940);
      d3.select('.'+element[0].player+' .player-graph .y.axis').transition().duration(1000).call(newyAxis);
      for (var i = 0; i < keys.length; i++) {
        var user = keys[i];
        pgFIL = pgFIL.filter(function(d,poop){
                    if( $('#slider'+user).parent('li').hasClass('lessThan') ){
                      if( d[user] > parseFloat($('#slider'+user).val()) ){
                        slicers.pushIfNotExist(d.ogPOOP, function(e){
                          return e === d.ogPOOP;
                        });
                      }
                      return d[user] <= $('#slider'+user).val();
                    }else{
                      if( d[user] < parseFloat($('#slider'+user).val()) ){
                        slicers.pushIfNotExist(d.ogPOOP, function(e){
                          return e === d.ogPOOP;
                        });
                      }
                      return d[user] >= $('#slider'+user).val();
                    }
                  });
      };

      slicers.sort(sortNumber);
      for (var i = 0; i < slicers.length; i++) {
        setupObj.splice(slicers[i],1); 
      };      
      pgFIL
        .on('mouseleave', function(q,w){
          d3.select(this).transition().attr('r', 1 + (q.MIN / minMAX) * 7).attr('stroke-width', 1.2);
        }).transition().duration(1000)
        .attr('cy', function(q,w){return psyScale(q[idz]);})
        .attr('r', function(q,w){
          return 1 + (q.MIN / minMAX) * 7;
        });

      playerAveCont.text(function(d){
        var moo = 0;
        setupObj.forEach(function(m,j){
          moo += m[d];
        });
        if(moo === 0){
          return " - ";
        }else{
          if(d === "FGP" || d === "P3P" || d === "FTP"){
            return round(moo / setupObj.length, 2);
          }else{
            return round(moo / setupObj.length, 1);
          }
        }
      });
      PGC.select('h4').text(pgFIL[0].length);
    });   
  }
  function update(nRadius, idz, updatepl) {
    // adjust the text on the range slider
    d3.select('.presets div').classed('active', false);
    var rtit = d3.select("#"+idz+'label').text(nRadius);
    var rinp = d3.select("#"+idz).property("value", nRadius);
    var modcls = idz.replace('slider', '');
    if(nRadius > 0){
      rtit.select(function() { return this.parentNode; }).classed('active', true);
      d3.selectAll('.aves .'+modcls).classed('active', true);
    }else{
      rtit.select(function() { return this.parentNode; }).classed('active', false);
      d3.selectAll('.aves .'+modcls).classed('active', false);
    }
    //update Players takes values from Sliders to update players
    if(!updatepl){
      updatePlayers(modcls);
    }
  }
  function sharePopup(url){
      var width = 600;
      var height = 400;
      var leftPosition, topPosition;
      leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
      topPosition = (window.screen.height / 2) - ((height / 2) + 50);
      var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
      window.open(url,'Social Share', windowFeatures);
  }
  function mittens(){
    var inptz = $('.sliders').children('li');
    var playerz = $('.player .left').find('h4');

    var themaxed = d3.max(playerz, function(d,i){return parseFloat($(d).text());});
    var maxedPL = [];
    for (var i = playerz.length - 1; i >= 0; i--) {
      if(parseFloat($(playerz[i]).text()) === themaxed){
        maxedPL.push($(playerz[i]).parent().find('.last').text());        
      }
    };

    var storz = [];
    var twitz, mitz;
    for (var i = 0; i < inptz.length; i++) {
      if($(inptz[i]).hasClass('lessThan') || parseFloat($(inptz[i]).find('input').val()) > 0 ){
        var eminem = $(inptz[i]).find('input').val() + " " + $(inptz[i]).find('span').text();
        storz.push(eminem);
      }
    };
    if(storz.length > 0){
      twitz = storz.toString();
      mitz = maxedPL.toString() + " had " + themaxed + " games with at least ";
    }else{
      twitz = ""
      mitz = "";
    }
    return mitz + " " + twitz;
  }
  $('#share-facebook').on('click', function(){
      var u = location.href;
      var t = mittens();
      sharePopup('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t));
      return false;
  });
  $('#share-twitter').on('click', function(){
      var u = location.href;
      var t = mittens()+' ';
      sharePopup('http://twitter.com/share?url='+encodeURIComponent(u)+'&text='+encodeURIComponent(t)+'&via=1p21interactive');
      return false;
  });
  $('#share-google').on('click', function(){
      var u = location.href;
      var t = mittens();
      sharePopup('https://plus.google.com/share?url='+encodeURIComponent(u)+'&text='+encodeURIComponent(t));
      return false;
  });
  $('#information').on('click', function(){
    $('body').toggleClass('information');
  });
  function mgt(){
    var ht = $('nav').innerHeight();
    $('article').css('margin-top', ht);
  }
  mgt();
  $(window).resize(function(){
    mgt();
  });  
// });